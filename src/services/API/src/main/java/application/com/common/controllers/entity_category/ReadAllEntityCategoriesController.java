package application.com.common.controllers.entity_category;

import application.com.common.models.EntityCategory;
import application.com.common.services.interfaces.EntityCategoryService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/entity-categories")
public class ReadAllEntityCategoriesController {
    private final EntityCategoryService service;

    public ReadAllEntityCategoriesController(EntityCategoryService service) {
        this.service = service;
    }

    @GetMapping
    public List<EntityCategory> read() {
        return service.read();
    }

}
