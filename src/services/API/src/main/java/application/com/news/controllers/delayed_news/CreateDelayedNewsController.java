package application.com.news.controllers.delayed_news;

import application.com.news.models.DelayedNews;
import application.com.news.services.interfaces.DelayedNewService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("api/news/delayed")
public class CreateDelayedNewsController {
    private final DelayedNewService service;

    public CreateDelayedNewsController(DelayedNewService service) {
        this.service = service;
    }

    @PostMapping
    public DelayedNews create(@RequestBody DelayedNews delayedNews){
       return service.create(delayedNews);
    }
}
