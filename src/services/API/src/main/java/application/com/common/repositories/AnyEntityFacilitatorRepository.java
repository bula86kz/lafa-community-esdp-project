package application.com.common.repositories;

import application.com.common.models.AnyEntityFacilitator;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface AnyEntityFacilitatorRepository extends JpaRepository<AnyEntityFacilitator, Integer>, JpaSpecificationExecutor<AnyEntityFacilitator> {
}
