package application.com.events.controllers.event;

import application.com.events.DTO.EventDTO;
import application.com.events.mappers.EventMapper;
import application.com.events.models.Event;
import application.com.events.services.interfaces.EventService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/events")
public class CreateEventController {
    private final EventService service;
    private final EventMapper mapper;


    public CreateEventController(EventService service, EventMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @PostMapping
    public Event create(@RequestBody EventDTO eventDTO) {
        return service.create(mapper.toEntity(eventDTO));
    }
}
