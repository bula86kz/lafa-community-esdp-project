package application.com.news.controllers.delayed_news;

import application.com.news.models.DelayedNews;
import application.com.news.services.interfaces.DelayedNewService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/news/delayed")
public class UpdateDelayedNewsController {
    private final DelayedNewService service;

    public UpdateDelayedNewsController(DelayedNewService service) {
        this.service = service;
    }


    @PutMapping("/{id}")
    public void update(@PathVariable Integer id, @RequestBody DelayedNews delayedNews) {
        service.update(id, delayedNews);
    }
}
