package application.com.common.controllers.entity_type;

import application.com.common.models.EntityType;
import application.com.common.services.interfaces.EntityTypeService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/entity-types")
public class UpdateEntityTypeController {
    private final EntityTypeService service;

    public UpdateEntityTypeController(EntityTypeService service) {
        this.service = service;
    }

    @PutMapping("/{id}")
    public EntityType update (@PathVariable int id, @RequestBody EntityType entityType) {
        return service.update(id, entityType);
    }
}
