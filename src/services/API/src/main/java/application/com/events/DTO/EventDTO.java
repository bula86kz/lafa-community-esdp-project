package application.com.events.DTO;

import application.com.interfaces.AbstractDTO;
import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Builder
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
public class EventDTO extends AbstractDTO {

    @NotBlank
    private String title;

    @NotBlank
    private String description;

    @NotBlank
    private String image;

    @NotNull
    private LocalDateTime startDate;

    @NotNull
    private LocalDateTime finishDate;

    @NotNull
    private int price;

    @NotNull
    private Integer categoryId;

    @NotNull
    private Integer formatId;

    @NotNull
    private Integer languageId;

    @NotNull
    private Integer cityId;
}
