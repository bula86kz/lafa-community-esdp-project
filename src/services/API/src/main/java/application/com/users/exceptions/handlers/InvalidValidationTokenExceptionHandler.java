package application.com.users.exceptions.handlers;

import application.com.users.exceptions.InvalidValidationTokenException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

@ControllerAdvice(annotations = RestController.class)
public class InvalidValidationTokenExceptionHandler {

    @ExceptionHandler(InvalidValidationTokenException.class)
    private ResponseEntity<?> handle(InvalidValidationTokenException exception) {
        return ResponseEntity
                            .unprocessableEntity()
                            .body(exception.getMessage());
    }

}
