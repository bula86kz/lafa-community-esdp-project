package application.com.common.specification;

import application.com.common.models.Category;
import application.com.common.models.EntityCategory;
import application.com.common.models.EntityType;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;

public class CategorySpecification {

    public static Specification<Category> toPredicate(String type) {
        return (root, query, criteriaBuilder) -> {
            Join<EntityCategory, Category> entityCategory = root.join("category");
            Join<EntityType, EntityCategory> entityType = entityCategory.join("entityType");
            return criteriaBuilder.like(entityType.get("type"), type);
        };
    }

}
