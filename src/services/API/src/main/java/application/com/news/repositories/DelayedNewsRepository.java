package application.com.news.repositories;

import application.com.news.models.DelayedNews;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface DelayedNewsRepository extends JpaRepository<DelayedNews, Integer>, JpaSpecificationExecutor {
}
