package application.com.common.services.interfaces;

import application.com.common.models.EntityComment;
import application.com.interfaces.CRUDService;

public interface EntityCommentService extends CRUDService<EntityComment> {
}
