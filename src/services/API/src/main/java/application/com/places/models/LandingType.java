package application.com.places.models;

import application.com.interfaces.AbstractEntity;
import lombok.*;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Builder
@Getter @Setter
@AllArgsConstructor
@Table(name = "landing_types")
@NoArgsConstructor(force = true)
public class LandingType extends AbstractEntity {
    private String title;
    private String icon;
    private String slug;
}
