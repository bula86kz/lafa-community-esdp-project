package application.com.events.DTO;

import application.com.interfaces.AbstractDTO;
import lombok.*;

@Builder
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
public class FormatDTO extends AbstractDTO {
    private String title;
}
