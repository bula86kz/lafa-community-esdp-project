package application.com.places.services;

import application.com.places.models.Description;
import application.com.rest_template.RestTemplateService;
import org.springframework.http.*;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DescriptionServiceImpl {
    private final RestTemplateService restTemplateService;

    public DescriptionServiceImpl(RestTemplateService restTemplateService) {
        this.restTemplateService = restTemplateService;
    }

    public String read(String url) {
        return restTemplateService.restTemplateGet(url, HttpMethod.GET);
    }

    public String create(String url, String title, List<String> description, Integer placeId) {
        Description entity = new Description();
        entity.setTitle(title);
        entity.setDescription(description);
        entity.setPlaceId(placeId);
        return  restTemplateService.restTemplatePost(url, entity);
    }

    public void update(String url, Description description) {
        restTemplateService.restTemplateUpdate(url, description);
    }
    public void delete(String url) {
        restTemplateService.restTemplateGet(url, HttpMethod.DELETE);
    }
}
