package application.com.news.controllers.news;

import application.com.news.DTO.NewsDTO;
import application.com.news.mapper.NewsMapper;
import application.com.news.models.News;
import application.com.news.services.interfaces.NewsService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
@RequestMapping("api/news/nearest")
public class ReadAllNearestNewsController {
    private final NewsService service;
    private final NewsMapper mapper;

    public ReadAllNearestNewsController(NewsService service, NewsMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }


    @GetMapping
    public List<NewsDTO> readAllNearest() {
        return mapper.toDTOList(service.readAllNearest());
    }
}