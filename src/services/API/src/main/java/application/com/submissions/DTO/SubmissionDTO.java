package application.com.submissions.DTO;

import application.com.interfaces.AbstractDTO;
import lombok.*;

import java.time.LocalDateTime;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
public class SubmissionDTO extends AbstractDTO {
    private String organizationName;
    private LocalDateTime planningDate;
    private int participantCount;
    private int budget;
    private String goal;

    private String fullName;
    private String phoneNumber;
    private String email;
    private String comment;

    private Integer languageId;
    private Integer cityId;
    private Integer preferredCommunicationId;
}
