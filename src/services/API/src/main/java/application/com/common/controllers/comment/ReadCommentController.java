package application.com.common.controllers.comment;

import application.com.common.DTO.CommentDTO;
import application.com.common.mappers.CommentMapper;
import application.com.common.services.interfaces.CommentService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/comments")
public class ReadCommentController {
    private final CommentService service;
    private final CommentMapper mapper;

    public ReadCommentController(CommentService service, CommentMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @GetMapping("/{entityType}/{id}")
    public List<CommentDTO> readCommentsByTypeAndEntityId(@PathVariable String entityType, @PathVariable Integer id) {
        List<CommentDTO> commentDTOS = mapper.toDTOList(service.getFilteredCommentsByTypeAndEntityId(entityType, id));
        return commentDTOS;
    }
}
