package application.com.news.controllers.news;

import application.com.news.DTO.NewsDTO;
import application.com.news.mapper.NewsMapper;
import application.com.news.services.interfaces.NewsService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/news")
public class UpdateNewsController {
    private final NewsService service;
    private final NewsMapper mapper;

    public UpdateNewsController(NewsService service, NewsMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }


    @PutMapping("/{id}")
    public void update(@PathVariable Integer id, @RequestBody NewsDTO dto) {
        service.update(id, mapper.toEntity(dto));
    }
}
