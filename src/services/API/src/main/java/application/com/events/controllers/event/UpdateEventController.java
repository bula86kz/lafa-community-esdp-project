package application.com.events.controllers.event;

import application.com.events.DTO.EventDTO;
import application.com.events.mappers.EventMapper;
import application.com.events.models.Event;
import application.com.events.services.interfaces.EventService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/events")
public class UpdateEventController {
    private final EventService service;
    private final EventMapper mapper;


    public UpdateEventController(EventService service, EventMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @PutMapping("/{id}")
    public Event update(@PathVariable int id, @RequestBody EventDTO eventDTO) {
        return service.update(id, mapper.toEntity(eventDTO));
    }
}
