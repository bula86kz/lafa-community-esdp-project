package application.com.news.controllers.delayed_news;

import application.com.news.models.DelayedNews;
import application.com.news.services.interfaces.DelayedNewService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/news/delayed")
public class ReadDelayedNewsController {
    private final DelayedNewService service;

    public ReadDelayedNewsController(DelayedNewService service) {
        this.service = service;
    }


    @GetMapping("/{id}")
    public DelayedNews read(@PathVariable Integer id) {
        return service.read(id);
    }

}
