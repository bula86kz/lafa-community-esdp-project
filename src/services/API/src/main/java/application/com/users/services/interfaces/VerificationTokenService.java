package application.com.users.services.interfaces;

import application.com.users.models.User;

public interface VerificationTokenService {
    boolean existsByToken(String token);

    User getUser(String token);

    String updateTokenByEmail(String email);

    void create(User user, String token);
    void confirm(String token);
}
