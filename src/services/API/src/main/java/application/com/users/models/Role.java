package application.com.users.models;

import application.com.interfaces.AbstractEntity;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Builder
@Getter @Setter
@AllArgsConstructor
@Table(name = "roles")
@NoArgsConstructor(force = true)
public class Role extends AbstractEntity {
    private String title;
    private String description;
    private String slug;
}
