package application.com.submissions.services.interfaces;

import application.com.interfaces.CRUDService;
import application.com.submissions.models.Submission;

public interface SubmissionService extends CRUDService<Submission> {
}
