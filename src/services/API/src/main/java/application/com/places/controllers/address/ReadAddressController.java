package application.com.places.controllers.address;

import application.com.places.models.Address;
import application.com.places.services.interfaces.AddressService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/places/addresses")
public class ReadAddressController {
    private final AddressService service;

    public ReadAddressController(AddressService service) {
        this.service = service;
    }

    @GetMapping("/{id}")
    public Address read(@PathVariable int id){
        return service.read(id);
    }
}
