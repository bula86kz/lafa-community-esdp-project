package application.com.submissions.controllers.goal;

import application.com.submissions.DTO.GoalDTO;
import application.com.submissions.mappers.GoalMapper;
import application.com.submissions.services.interfaces.GoalService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/calculators/goals")
public class CreateGoalController {
    private final GoalService service;
    private final GoalMapper mapper;

    public CreateGoalController(GoalService service, GoalMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @PostMapping
    public GoalDTO create (@RequestBody GoalDTO goalDTO) {
        return mapper.toDTO(service.create(mapper.toEntity(goalDTO)));
    }
}
