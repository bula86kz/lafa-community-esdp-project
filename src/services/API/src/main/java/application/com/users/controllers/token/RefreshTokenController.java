package application.com.users.controllers.token;

import application.com.users.helpers.JWTTokenGenerator;
import application.com.users.models.Role;
import application.com.users.models.User;
import application.com.users.services.interfaces.UserService;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/api/")
public class RefreshTokenController {
    @Value("${spring.security.application.secret}")
    private String secret;

    private final UserService service;
    private final JWTTokenGenerator tokenGenerator;

    public RefreshTokenController(UserService service, JWTTokenGenerator tokenGenerator) {
        this.service = service;
        this.tokenGenerator = tokenGenerator;
    }

    @GetMapping("/token/refresh")
    public void refresh(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String authorization = request.getHeader(AUTHORIZATION);

        if (authorization != null && authorization.startsWith("Bearer ")) {
            try {
                String refreshToken = authorization.substring("Bearer ".length());
                Algorithm algorithm = Algorithm.HMAC256(secret.getBytes());

                JWTVerifier verifier = JWT.require(algorithm).build();
                DecodedJWT decodedJWT = verifier.verify(refreshToken);

                String email = decodedJWT.getSubject();
                User user = service.getByEmail(email);
                List<String> roles = user
                                        .getRoles()
                                        .stream()
                                        .map(Role::getTitle)
                                        .collect(Collectors.toList());

                String accessToken = tokenGenerator.generateAccessToken(email, request.getRequestURI(), roles);


                response.setHeader("access_token", accessToken);
                response.setHeader("refresh_token", refreshToken);
            } catch (Exception exception) {
                String message = exception.getMessage();

                response.setHeader("error", message);
                response.setStatus(UNAUTHORIZED.value());

                Map<String, String> errors = new HashMap<String, String>();

                errors.put("error_message", message);

                response.setContentType(APPLICATION_JSON_VALUE);
                new ObjectMapper().writeValue(response.getOutputStream(), errors);
            }

        } else {
            throw new RuntimeException("Refresh token is missing...");
        }
    }
}
