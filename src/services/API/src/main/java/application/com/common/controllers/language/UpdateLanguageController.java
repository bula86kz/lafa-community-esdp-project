package application.com.common.controllers.language;

import application.com.common.DTO.LanguageDTO;
import application.com.common.mappers.LanguageMapper;
import application.com.common.models.Language;
import application.com.common.services.interfaces.LanguageService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/languages")
public class UpdateLanguageController {
    private final LanguageService service;
    private final LanguageMapper mapper;

    public UpdateLanguageController(LanguageService service, LanguageMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @PutMapping("/{id}")
    public Language update(@PathVariable int id, @RequestBody LanguageDTO languageDTO) {
        return service.update(id, mapper.toEntity(languageDTO));
    }
}
