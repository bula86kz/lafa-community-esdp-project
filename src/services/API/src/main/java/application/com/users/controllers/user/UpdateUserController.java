package application.com.users.controllers.user;

import application.com.users.models.User;
import application.com.users.services.interfaces.UserService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/users")
public class UpdateUserController {
    private final UserService service;

    public UpdateUserController(UserService service) {
        this.service = service;
    }

    @PutMapping("/{id}")
    public User update(@PathVariable Integer id, @RequestBody User user) {
        return service.update(id, user);
    }
}
