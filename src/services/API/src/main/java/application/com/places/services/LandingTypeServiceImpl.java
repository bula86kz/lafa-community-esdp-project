package application.com.places.services;

import application.com.places.models.LandingType;
import application.com.places.repositories.LandingTypeRepository;
import application.com.places.services.interfaces.LandingTypeService;
import org.springframework.stereotype.Service;
import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Service
public class LandingTypeServiceImpl implements LandingTypeService {
    private final LandingTypeRepository repository;

    public LandingTypeServiceImpl(LandingTypeRepository repository) {
        this.repository = repository;
    }

    @Override
    public LandingType create(LandingType landingType) {
        return repository.save(landingType);
    }

    @Override
    public List<LandingType> read() {
        return repository.findAll();
    }

    @Override
    public LandingType read(Integer id) {
        Optional<LandingType> optionalLandingType = repository.findById(id);
        return getLandingTypeOrThrowException(optionalLandingType);
    }

    @Override
    public LandingType update(Integer id, LandingType landingType) {
        Optional<LandingType> optionalLandingType = repository.findById(id);
        LandingType targetLandingType = getLandingTypeOrThrowException(optionalLandingType);
        return repository.save(targetLandingType);
    }

    @Override
    public void delete(Integer id) {
        repository.deleteById(id);
    }

    private LandingType getLandingTypeOrThrowException(Optional<LandingType> landingType){
        if(landingType.isPresent()){
            return landingType.get();
        }
        throw new EntityNotFoundException("Can't find landing type with the ID");
    }
}
