package application.com.places.services;

import application.com.places.models.City;
import application.com.places.repositories.CityRepository;
import application.com.places.services.interfaces.CityService;
import org.springframework.stereotype.Service;
import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Service
public class CityServiceImpl implements CityService{
    private final CityRepository repository;

    public CityServiceImpl(CityRepository repository) {
        this.repository = repository;
    }

    @Override
    public City create(City city) {
        return repository.save(city);
    }

    @Override
    public List<City> read() {
        return repository.findAll();
    }

    @Override
    public City read(Integer id) {
        Optional<City> optionalCity = repository.findById(id);
        return getCityOrThrowException(optionalCity);
    }

    @Override
    public City update(Integer id, City city) {
        Optional<City> optionalCity = repository.findById(id);
        City targetCity = getCityOrThrowException(optionalCity);
        return repository.save(targetCity);
    }

    @Override
    public void delete(Integer id) {
        repository.deleteById(id);
    }

    private City getCityOrThrowException(Optional<City> city){
        if(city.isPresent()){
            return city.get();
        }
        throw new EntityNotFoundException("Can't find city with the ID");
    }
}
