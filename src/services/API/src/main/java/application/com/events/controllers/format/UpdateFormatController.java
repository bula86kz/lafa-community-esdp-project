package application.com.events.controllers.format;

import application.com.events.DTO.FormatDTO;
import application.com.events.mappers.FormatMapper;
import application.com.events.models.format.Format;
import application.com.events.services.interfaces.FormatService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/formats")
public class UpdateFormatController {
    private final FormatService service;
    private final FormatMapper mapper;


    public UpdateFormatController(FormatService service, FormatMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @PutMapping("/{id}")
    public Format update(@PathVariable int id, @RequestBody FormatDTO formatDTO) {
        return service.update(id, mapper.toEntity(formatDTO));
    }
}
