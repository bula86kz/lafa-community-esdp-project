package application.com.submissions.controllers.preferred_communication;

import application.com.submissions.models.PreferredCommunication;
import application.com.submissions.services.interfaces.PreferredCommunicationService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/submissions/preferred-communications")
public class CreatePreferredCommunicationController {
    private final PreferredCommunicationService service;

    public CreatePreferredCommunicationController(PreferredCommunicationService service) {
        this.service = service;
    }

    @PostMapping
    public PreferredCommunication create (@RequestBody PreferredCommunication preferredCommunication){
        return service.create(preferredCommunication);
    }
}
