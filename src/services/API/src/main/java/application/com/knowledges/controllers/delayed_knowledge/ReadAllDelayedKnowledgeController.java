package application.com.knowledges.controllers.delayed_knowledge;

import application.com.knowledges.models.DelayedKnowledge;
import application.com.knowledges.services.interfaces.DelayedKnowledgeService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api/knowledges/delayed")
public class ReadAllDelayedKnowledgeController {
    private final DelayedKnowledgeService service;

    public ReadAllDelayedKnowledgeController(DelayedKnowledgeService service) {
        this.service = service;
    }


    @GetMapping
    public List<DelayedKnowledge> readAll() {
        return service.read();
    }
}
