package application.com.common.controllers.entity_type;

import application.com.common.services.interfaces.EntityTypeService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/entity-types")
public class DeleteEntityTypeController {
    private final EntityTypeService service;

    public DeleteEntityTypeController(EntityTypeService service) {
        this.service = service;
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        service.delete(id);
    }
}
