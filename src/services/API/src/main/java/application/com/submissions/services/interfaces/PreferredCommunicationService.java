package application.com.submissions.services.interfaces;

import application.com.interfaces.CRUDService;
import application.com.submissions.models.PreferredCommunication;

public interface PreferredCommunicationService extends CRUDService<PreferredCommunication> {
}
