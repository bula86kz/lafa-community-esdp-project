package application.com.knowledges.repositories;

import application.com.knowledges.models.Knowledge;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;


@Repository
public interface KnowledgeRepository extends JpaRepository<Knowledge, Integer>, JpaSpecificationExecutor {
}
