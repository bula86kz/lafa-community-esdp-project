package application.com.common.services.interfaces;


import application.com.common.models.Category;
import application.com.common.models.EntityCategory;
import application.com.interfaces.CRUDService;

import java.util.List;

public interface EntityCategoryService extends CRUDService<EntityCategory> {
    List<EntityCategory> getEntityCategoriesByType(String slug);
    List<Category> getCategoriesByType(String type);
}
