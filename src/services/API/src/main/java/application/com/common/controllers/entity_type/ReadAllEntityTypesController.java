package application.com.common.controllers.entity_type;

import application.com.common.models.EntityType;
import application.com.common.services.interfaces.EntityTypeService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/entity-types")
public class ReadAllEntityTypesController {
    private final EntityTypeService service;

    public ReadAllEntityTypesController(EntityTypeService service) {
        this.service = service;
    }

    @GetMapping
    public List<EntityType> read() {
        return service.read();
    }

}
