package application.com.news.services.interfaces;

import application.com.interfaces.CRUDService;
import application.com.news.models.News;

import java.util.List;


public interface NewsService extends CRUDService<News> {
    List<News> readAllNearest();
    List<News> readAllPublished();
    List<News> readAllByAuthor(Integer id);
}