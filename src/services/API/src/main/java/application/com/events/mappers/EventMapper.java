package application.com.events.mappers;

import application.com.common.models.Category;
import application.com.common.models.Language;
import application.com.common.services.interfaces.CategoryService;
import application.com.common.services.interfaces.LanguageService;
import application.com.events.DTO.EventDTO;
import application.com.events.models.Event;
import application.com.events.models.format.Format;
import application.com.events.services.interfaces.FormatService;
import application.com.interfaces.AbstractMapper;
import application.com.places.models.City;
import application.com.places.services.interfaces.CityService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Objects;

@Component
public class EventMapper extends AbstractMapper<Event, EventDTO> {
    private final ModelMapper mapper;
    private final CategoryService categoryService;
    private final CityService cityService;
    private final FormatService formatService;
    private final LanguageService languageService;

    public EventMapper(ModelMapper mapper, CategoryService categoryService, CityService cityService, FormatService formatService, LanguageService languageService){
        super(Event.class, EventDTO.class);

        this.mapper = mapper;
        this.categoryService = categoryService;
        this.cityService = cityService;
        this.formatService = formatService;
        this.languageService = languageService;
    }

    @PostConstruct
    public void setupMapper(){
        mapper.createTypeMap(Event.class, EventDTO.class)
                .addMappings( mapper -> {
                    mapper.skip(EventDTO::setCategoryId);
                    mapper.skip(EventDTO::setCityId);
                    mapper.skip(EventDTO::setFormatId);
                    mapper.skip(EventDTO::setLanguageId);
                })
                .setPostConverter(toDTOConverter());

        mapper.createTypeMap(EventDTO.class, Event.class)
                .addMappings( mapper -> {
                    mapper.skip(Event::setCategory);
                    mapper.skip(Event::setCity);
                    mapper.skip(Event::setFormat);
                    mapper.skip(Event::setLanguage);
                })
                .setPostConverter(toEntityConverter());
    }

    @Override
    public void mapSpecificFields(Event source, EventDTO destination){
        destination.setCategoryId(source.getCategory().getId());
        destination.setCityId(source.getCity().getId());
        destination.setFormatId(source.getFormat().getId());
        destination.setLanguageId(source.getLanguage().getId());
    }

    @Override
    public void mapSpecificFields(EventDTO source, Event destination){
        Integer categoryId = getCategoryId(source);
        Integer cityId = getCityId(source);
        Integer formatId = getFormatId(source);
        Integer languageId = getLanguageId(source);

        Category category = categoryService.read(categoryId);
        City city = cityService.read(cityId);
        Format format = formatService.read(formatId);
        Language language = languageService.read(languageId);

        destination.setCategory(category);
        destination.setCity(city);
        destination.setFormat(format);
        destination.setLanguage(language);
    }

    private Integer getCategoryId(EventDTO eventDTO){
        return Objects.isNull(eventDTO) ? null : eventDTO.getCategoryId();
    }

    private Integer getCityId(EventDTO eventDTO){
        return Objects.isNull(eventDTO) ? null : eventDTO.getCityId();
    }

    private Integer getFormatId(EventDTO eventDTO){
        return Objects.isNull(eventDTO) ? null : eventDTO.getFormatId();
    }

    private Integer getLanguageId(EventDTO eventDTO){
        return Objects.isNull(eventDTO) ? null : eventDTO.getLanguageId();
    }
}



