package application.com.common.models;

import application.com.interfaces.AbstractEntity;
import lombok.*;

import javax.persistence.*;

@Entity
@Builder
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
@Table(name = "entity_comments")
public class EntityComment extends AbstractEntity {
    private Integer entityId;

    @ManyToOne
    @JoinColumn(name = "entity_type_id")
    private EntityType entityType;

    @ManyToOne
    @JoinColumn(name = "comment_id")
    private Comment comment;
}
