package application.com.submissions.services;

import application.com.submissions.models.Goal;
import application.com.submissions.repositories.GoalRepository;
import application.com.submissions.services.interfaces.GoalService;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Service
public class GoalServiceImpl implements GoalService {
    private final GoalRepository repository;

    public GoalServiceImpl(GoalRepository repository) {
        this.repository = repository;
    }

    @Override
    public Goal create(Goal goal) {
        return repository.save(goal);
    }

    @Override
    public List<Goal> read() {
        return repository.findAll();
    }

    @Override
    public Goal read(Integer id) {
        Optional<Goal> optionalGoal = repository.findById(id);
        return getGoalOrThrowException(optionalGoal);
    }

    @Override
    public Goal update(Integer id, Goal goal) {
        Optional<Goal> optionalGoal = repository.findById(id);
        Goal targetGoal = getGoalOrThrowException(optionalGoal);

        goal.setId(id);
        goal.setCreated(targetGoal.getCreated());
        return repository.save(goal);
    }

    @Override
    public void delete(Integer id) {
        repository.deleteById(id);
    }

    private Goal getGoalOrThrowException(Optional<Goal> goal) {
        if (goal.isPresent()){
            return goal.get();
        }
        throw new EntityNotFoundException("Can't find goal with the ID");
    }
}
