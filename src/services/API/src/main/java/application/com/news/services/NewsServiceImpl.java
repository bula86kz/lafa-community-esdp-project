package application.com.news.services;
import application.com.common.specification.AuthorSpecification;
import application.com.common.specification.delayed_posting.DelayedPostingSpecification;
import application.com.news.exceptions.NewsNotFoundException;
import application.com.news.models.News;
import application.com.news.repositories.NewsRepository;
import application.com.news.services.interfaces.NewsService;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class NewsServiceImpl implements NewsService {
    private final NewsRepository repository;
    private final DelayedPostingSpecification<News> specification;
    private final AuthorSpecification authorSpecification;

    public NewsServiceImpl(NewsRepository repository, DelayedPostingSpecification<News> specification, AuthorSpecification authorSpecification) {
        this.repository = repository;
        this.specification = specification;
        this.authorSpecification = authorSpecification;
    }


    @Override
    public News create(News news) {
        return repository.save(news);
    }

    @Override
    public List<News> read() {
        return repository.findAll();
    }

    @Override
    public News read(Integer id) {
        Optional<News> optionalNews = repository.findById(id);
        return getNewsOrThrowException(optionalNews);

    }

    @Override
    public News update(Integer id, News news) {
        Optional<News> optionalNews = repository.findById(id);
        News targetNews = getNewsOrThrowException(optionalNews);

        news.setId(id);
        news.setCreated(targetNews.getCreated());

        return repository.save(news);
    }

    @Override
    public void delete(Integer id) {
        repository.deleteById(id);
    }


    @Override
    public List<News> readAllNearest() {
        LocalDateTime currentTime = LocalDateTime.now();
        LocalDateTime currentTimePlusOneHour = LocalDateTime.now().plusHours(1);

        List<News> news = repository.findAll(Specification.where(specification.delayedPostingSpecification(currentTime, currentTimePlusOneHour)));

        news.stream().forEach(news1 -> {
            news1.setPublished(true);
            repository.save(news1);
        });

        return news;
    }

    @Override
    public List<News> readAllPublished() {
       return repository.findAll(Specification.where(specification.published()), Sort.by(Sort.Direction.DESC, "publishedDate"));
    }

    @Override
    public List<News> readAllByAuthor(Integer id) {
        return repository.findAll(Specification.where(authorSpecification.findByAuthorSpecification(id)));
    }




    private News getNewsOrThrowException(Optional<News> news) {
        if (news.isPresent()) {
            return news.get();
        }

        throw new NewsNotFoundException();
    }
}