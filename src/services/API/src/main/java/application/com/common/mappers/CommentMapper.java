package application.com.common.mappers;

import application.com.common.DTO.CommentDTO;
import application.com.common.models.Comment;
import application.com.interfaces.AbstractMapper;
import application.com.users.models.User;
import application.com.users.services.interfaces.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Objects;

@Component
public class CommentMapper extends AbstractMapper<Comment, CommentDTO> {
    private final ModelMapper mapper;
    private final UserService userService;

    public CommentMapper(ModelMapper mapper, UserService userService) {
        super(Comment.class, CommentDTO.class);
        this.mapper = mapper;
        this.userService = userService;
    }

    @PostConstruct
    public void setupMapper(){
        mapper
                .createTypeMap(Comment.class, CommentDTO.class)
                .addMappings(mapper -> {
                    mapper.skip(CommentDTO::setEmail);
                })
                .setPostConverter(toDTOConverter());
        mapper
                .createTypeMap(CommentDTO.class, Comment.class)
                .addMappings(mapper -> {
                    mapper.skip(Comment::setUser);
                })
                .setPostConverter(toEntityConverter());
    }

    @Override
    public void mapSpecificFields(Comment source, CommentDTO destination){
        destination.setEmail(source.getUser().getEmail());
    }

    @Override
    public void mapSpecificFields(CommentDTO source, Comment destination){
        String email = getUserEmail(source);
        User user = userService.getByEmail(email);

        destination.setUser(user);
    }

    private String getUserEmail (CommentDTO commentDTO){
        return Objects.isNull(commentDTO)
                ? null
                : commentDTO
                .getEmail();
    }
}
