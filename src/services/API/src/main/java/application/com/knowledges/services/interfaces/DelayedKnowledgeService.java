package application.com.knowledges.services.interfaces;

import application.com.interfaces.CRUDService;
import application.com.knowledges.models.DelayedKnowledge;
import java.util.List;


public interface DelayedKnowledgeService extends CRUDService<DelayedKnowledge> {
    List<DelayedKnowledge> readAllNearest();
}
