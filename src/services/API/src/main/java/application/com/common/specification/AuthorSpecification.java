package application.com.common.specification;


import lombok.AllArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class AuthorSpecification<T> {
    public Specification<T> findByAuthorSpecification(Integer id) {
        return (root, query, cb) -> cb.equal(root.get("author"), id);
    }
}
