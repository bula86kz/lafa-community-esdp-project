package application.com.knowledges.controllers.knowledge;

import application.com.knowledges.DTO.KnowledgeDTO;
import application.com.knowledges.mapper.KnowledgeMapper;
import application.com.knowledges.services.interfaces.KnowledgeService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api/knowledges/author")
public class ReadAllKnowledgeByAuthorController {
    private final KnowledgeService service;
    private final KnowledgeMapper mapper;

    public ReadAllKnowledgeByAuthorController(KnowledgeService service, KnowledgeMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }


    @GetMapping("/{id}")
    public List<KnowledgeDTO> readByAuthor(@PathVariable Integer id) {
        return mapper.toDTOList(service.readAllByAuthor(id));
    }

}
