package application.com.common.services.interfaces;

import application.com.common.models.AnyEntityFacilitator;
import application.com.interfaces.CRUDService;
import application.com.users.models.User;

import java.util.List;

public interface AnyEntityFacilitatorService extends CRUDService<AnyEntityFacilitator> {
    List<User> sendUsersByEntityTypeAndEntityId(String entityType, Integer entityId);
}
