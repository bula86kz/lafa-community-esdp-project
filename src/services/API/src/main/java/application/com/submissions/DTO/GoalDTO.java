package application.com.submissions.DTO;

import application.com.interfaces.AbstractDTO;
import lombok.*;

@Builder
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
public class GoalDTO extends AbstractDTO {
    private String title;
}
