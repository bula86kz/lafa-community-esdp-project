package application.com.places.controllers.address;

import application.com.places.services.interfaces.AddressService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/places/addresses")
public class DeleteAddressController {
    private final AddressService service;

    public DeleteAddressController(AddressService service) {
        this.service = service;
    }

    @DeleteMapping("/{id}")
    public void delete (@PathVariable int id){
        service.delete(id);
    }
}
