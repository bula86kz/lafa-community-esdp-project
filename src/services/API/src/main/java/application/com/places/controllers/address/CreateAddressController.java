package application.com.places.controllers.address;

import application.com.places.models.Address;
import application.com.places.services.interfaces.AddressService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/places/addresses")
public class CreateAddressController {
    private final AddressService service;

    public CreateAddressController(AddressService service) {
        this.service = service;
    }

    @PostMapping
    public Address create(@RequestBody Address address){
        return service.create(address);
    }
}
