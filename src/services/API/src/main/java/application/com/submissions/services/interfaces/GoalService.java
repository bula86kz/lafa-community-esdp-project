package application.com.submissions.services.interfaces;

import application.com.interfaces.CRUDService;
import application.com.submissions.models.Goal;

public interface GoalService extends CRUDService<Goal> {
}
