package application.com.common.models;

import application.com.interfaces.AbstractEntity;
import application.com.users.models.User;
import lombok.*;

import javax.persistence.*;

@Entity
@Builder
@Getter @Setter
@AllArgsConstructor
@Table(name = "comments")
@NoArgsConstructor(force = true)
public class Comment extends AbstractEntity {
    private String comment;

    private boolean isApprove;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
}
