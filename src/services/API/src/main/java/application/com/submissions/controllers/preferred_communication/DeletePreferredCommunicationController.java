package application.com.submissions.controllers.preferred_communication;

import application.com.submissions.services.interfaces.PreferredCommunicationService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/submissions/preferred-communications")
public class DeletePreferredCommunicationController {
    private final PreferredCommunicationService service;

    public DeletePreferredCommunicationController(PreferredCommunicationService service) {
        this.service = service;
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id){
        service.delete(id);
    }
}
