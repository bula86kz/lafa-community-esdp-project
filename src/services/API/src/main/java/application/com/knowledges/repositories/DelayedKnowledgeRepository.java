package application.com.knowledges.repositories;

import application.com.knowledges.models.DelayedKnowledge;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface DelayedKnowledgeRepository extends JpaRepository<DelayedKnowledge, Integer>, JpaSpecificationExecutor {
}
