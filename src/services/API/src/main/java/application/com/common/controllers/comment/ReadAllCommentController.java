package application.com.common.controllers.comment;

import application.com.common.DTO.CommentDTO;
import application.com.common.mappers.CommentMapper;
import application.com.common.services.interfaces.CommentService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/comments")
public class ReadAllCommentController {
    private final CommentService service;
    private final CommentMapper mapper;


    public ReadAllCommentController(CommentService service, CommentMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @GetMapping
    public List<CommentDTO> read() {
        return mapper.toDTOList(service.read());
    }
}
