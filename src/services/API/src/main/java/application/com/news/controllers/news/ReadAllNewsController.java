package application.com.news.controllers.news;

import application.com.news.DTO.NewsDTO;
import application.com.news.mapper.NewsMapper;
import application.com.news.services.interfaces.NewsService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api/news")
public class ReadAllNewsController {
    private final NewsService service;
    private final NewsMapper mapper;

    public ReadAllNewsController(NewsService service, NewsMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }


    @GetMapping
    public List<NewsDTO> readAll() {
        return mapper.toDTOList(service.read());
    }
}
