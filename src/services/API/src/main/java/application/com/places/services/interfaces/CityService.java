package application.com.places.services.interfaces;

import application.com.interfaces.CRUDService;
import application.com.places.models.City;

public interface CityService extends CRUDService<City> {
}
