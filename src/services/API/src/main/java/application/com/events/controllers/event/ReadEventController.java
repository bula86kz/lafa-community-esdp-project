package application.com.events.controllers.event;

import application.com.events.models.Event;
import application.com.events.services.interfaces.EventService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/events")
public class ReadEventController {
    private final EventService service;


    public ReadEventController(EventService service) {
        this.service = service;
    }

    @GetMapping("/{id}")
    public Event read(@PathVariable int id) {
        return service.read(id);
    }
}
