package application.com.submissions.models;

import application.com.interfaces.AbstractEntity;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Builder
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
@Table(name = "preferred_communications")
public class PreferredCommunication extends AbstractEntity {
    private String title;
}
