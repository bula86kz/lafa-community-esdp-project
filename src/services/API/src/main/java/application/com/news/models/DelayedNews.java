package application.com.news.models;

import application.com.interfaces.AbstractEntity;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Builder
@Getter @Setter
@AllArgsConstructor
@Table(name = "delayed_news")
@NoArgsConstructor(force = true)
public class DelayedNews extends AbstractEntity {
    private LocalDateTime postingDate;

    @ManyToOne
    @JoinColumn(name = "news_id")
    private News news;
}
