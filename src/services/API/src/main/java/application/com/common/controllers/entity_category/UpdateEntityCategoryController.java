package application.com.common.controllers.entity_category;

import application.com.common.models.EntityCategory;
import application.com.common.services.interfaces.EntityCategoryService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/entity-categories")
public class UpdateEntityCategoryController {
    private final EntityCategoryService service;

    public UpdateEntityCategoryController(EntityCategoryService service) {
        this.service = service;
    }

    @PutMapping("/{id}")
    public EntityCategory update (@PathVariable int id, @RequestBody EntityCategory entityCategory) {
        return service.update(id, entityCategory);
    }
}
