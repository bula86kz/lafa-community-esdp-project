package application.com.events.services.interfaces;

import application.com.events.models.format.Format;
import application.com.interfaces.CRUDService;

public interface FormatService extends CRUDService<Format> {
}
