package application.com.common.controllers.category;

import application.com.common.models.Category;
import application.com.common.services.interfaces.CategoryService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/categories")
public class UpdateCategoryController {
    private final CategoryService service;

    public UpdateCategoryController(CategoryService service) {
        this.service = service;
    }

    @PutMapping("/{id}")
    public Category update(@PathVariable int id, @RequestBody Category category) {
        return service.update(id, category);
    }
}
