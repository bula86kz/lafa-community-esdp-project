package application.com.news.DTO;

import application.com.interfaces.AbstractDTO;
import lombok.*;
import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;


@Builder
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
public class NewsDTO extends AbstractDTO {
    @NotBlank
    private String title;
    @NotBlank
    private String description;
    @NotBlank
    private String image;

    @NotBlank
    private Integer authorId;

    private String firstName;

    private String lastName;

    private LocalDateTime publishedDate;
}

