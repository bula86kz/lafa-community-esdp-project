package application.com.users.controllers.registration;

import application.com.users.models.User;
import application.com.users.services.interfaces.MailSenderService;
import application.com.users.services.interfaces.UserService;
import application.com.users.services.interfaces.VerificationTokenService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/api/users/confirm")
public class ResendEmailConfirmationController {
    //Ввынести зависимости на разные уровни, контроллер должен пользоваться только всвоим сервисом...
    private final UserService userService;
    private final MailSenderService mailSenderService;
    private final VerificationTokenService verificationTokenService;

    public ResendEmailConfirmationController(MailSenderService mailSenderService, UserService userService, VerificationTokenService verificationTokenService) {
        this.userService = userService;
        this.mailSenderService = mailSenderService;
        this.verificationTokenService = verificationTokenService;
    }

    @PostMapping("/resend")
    public ResponseEntity<?> resend(@RequestBody Map<String, String> payload) {
        String email = payload.get("email");

        User user = userService.getByEmail(email);
        String token = verificationTokenService.updateTokenByEmail(email);

        mailSenderService.sendEmailVerificationToken(token, user);

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body("Confirmation resend");
    }
}

