package application.com.knowledges.services;

import application.com.common.models.Category;
import application.com.common.specification.AuthorSpecification;
import application.com.common.specification.KnowledgeByCategorySpecification;
import application.com.common.specification.delayed_posting.DelayedPostingSpecification;
import application.com.knowledges.exceptions.KnowledgeNotFoundException;
import application.com.knowledges.models.Knowledge;
import application.com.knowledges.repositories.KnowledgeRepository;
import application.com.knowledges.services.interfaces.KnowledgeService;
import application.com.users.models.User;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class KnowledgeServiceImpl implements KnowledgeService {
    private final KnowledgeRepository repository;
    private final DelayedPostingSpecification<Knowledge> specification;
    private final KnowledgeByCategorySpecification<Category> knowledgeByCategorySpecification;
    private final AuthorSpecification<User> authorSpecification;

    public KnowledgeServiceImpl(KnowledgeRepository repository, DelayedPostingSpecification<Knowledge> specification, KnowledgeByCategorySpecification<Category> knowledgeByCategorySpecification, AuthorSpecification<User> authorSpecification) {
        this.repository = repository;
        this.specification = specification;
        this.knowledgeByCategorySpecification = knowledgeByCategorySpecification;
        this.authorSpecification = authorSpecification;
    }


    @Override
    public Knowledge create(Knowledge knowledge) {
        return repository.save(knowledge);
    }

    @Override
    public List<Knowledge> read() {
        return repository.findAll();
    }

    @Override
    public Knowledge read(Integer id) {
        Optional<Knowledge> optionalKnowledge = repository.findById(id);
        return getKnowledgeOrThrowException(optionalKnowledge);
    }

    @Override
    public Knowledge update(Integer id, Knowledge knowledge) {
        Optional<Knowledge> optionalKnowledge = repository.findById(id);
        Knowledge targetKnowledge = getKnowledgeOrThrowException(optionalKnowledge);

        knowledge.setId(id);
        knowledge.setCreated(targetKnowledge.getCreated());

        return repository.save(knowledge);
    }

    @Override
    public void delete(Integer id) {
        repository.deleteById(id);
    }


    @Override
    public List<Knowledge> readAllNearest() {
        LocalDateTime currentTime = LocalDateTime.now();
        LocalDateTime currentTimePlusOneHour = LocalDateTime.now().plusHours(1);

        List<Knowledge> knowledges = repository.findAll(Specification.where(specification.delayedPostingSpecification(currentTime, currentTimePlusOneHour)));

        knowledges.stream().forEach(knowledge -> {
            knowledge.setPublished(true);
            repository.save(knowledge);
        });

        return knowledges;
    }

    @Override
    public List<Knowledge> readAllPublished() {
        return repository.findAll(Specification.where(specification.published()), Sort.by(Sort.Direction.DESC, "publishedDate"));
    }

    @Override
    public List<Knowledge> readAllByCategory(Integer id) {
        return repository.findAll(Specification.where(knowledgeByCategorySpecification.findByCategorySpecification(id)));
    }

    @Override
    public List<Knowledge> readAllByAuthor(Integer id) {
        return repository.findAll(Specification.where(authorSpecification.findByAuthorSpecification(id)));
    }


    private Knowledge getKnowledgeOrThrowException(Optional<Knowledge> knowledge) {
        if (knowledge.isPresent()) {
            return knowledge.get();
        }

        throw new KnowledgeNotFoundException();
    }
}
