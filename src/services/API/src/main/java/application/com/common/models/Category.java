package application.com.common.models;

import application.com.interfaces.AbstractEntity;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Builder
@Getter @Setter
@AllArgsConstructor
@Table(name = "categories")
@NoArgsConstructor(force = true)
public class Category extends AbstractEntity {
    private String title;
    private String description;
    private String slug;
}
