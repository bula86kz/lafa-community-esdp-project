package application.com.interfaces;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.MappedSuperclass;

@Getter @Setter
@MappedSuperclass
@AllArgsConstructor
@NoArgsConstructor(force = true)
public abstract class AbstractDTO {
    private Integer id;
}
