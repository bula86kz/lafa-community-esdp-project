package application.com.news.controllers.news;

import application.com.news.DTO.NewsDTO;
import application.com.news.mapper.NewsMapper;
import application.com.news.services.interfaces.NewsService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api/news/author")
public class ReadAllNewsByAuthorController {
    private final NewsService service;
    private final NewsMapper mapper;

    public ReadAllNewsByAuthorController(NewsService service, NewsMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }


    @GetMapping("/{id}")
    public List<NewsDTO> readByAuthor(@PathVariable Integer id) {
        return mapper.toDTOList(service.readAllByAuthor(id));
    }

}
