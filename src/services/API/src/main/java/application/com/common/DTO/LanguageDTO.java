package application.com.common.DTO;

import application.com.interfaces.AbstractDTO;
import lombok.*;

@Builder
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
public class LanguageDTO extends AbstractDTO {
    private String title;
}
