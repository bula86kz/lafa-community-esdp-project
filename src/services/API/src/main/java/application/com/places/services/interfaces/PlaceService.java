package application.com.places.services.interfaces;

import application.com.interfaces.CRUDService;
import application.com.places.models.Place;

import java.util.List;

public interface PlaceService extends CRUDService<Place> {
    List<Place> getPlaceByLandingSlug(String slug);
}
