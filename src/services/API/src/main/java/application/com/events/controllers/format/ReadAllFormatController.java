package application.com.events.controllers.format;

import application.com.events.DTO.FormatDTO;
import application.com.events.mappers.FormatMapper;
import application.com.events.services.interfaces.FormatService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/formats")
public class ReadAllFormatController {
    private final FormatService service;
    private final FormatMapper mapper;


    public ReadAllFormatController(FormatService service, FormatMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @GetMapping
    public List<FormatDTO> read() {
        return mapper.toDTOList(service.read());
    }
}
