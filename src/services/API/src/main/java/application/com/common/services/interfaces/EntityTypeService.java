package application.com.common.services.interfaces;


import application.com.common.models.EntityType;
import application.com.interfaces.CRUDService;

public interface EntityTypeService extends CRUDService<EntityType> {
    EntityType read(String type);
}
