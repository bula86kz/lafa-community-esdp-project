package application.com.knowledges.controllers.delayed_knowledge;

import application.com.knowledges.models.DelayedKnowledge;
import application.com.knowledges.services.interfaces.DelayedKnowledgeService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("api/knowledges/delayed")
public class CreateDelayedKnowledgeController {
    private final DelayedKnowledgeService service;

    public CreateDelayedKnowledgeController(DelayedKnowledgeService service) {
        this.service = service;
    }

    @PostMapping
    public DelayedKnowledge create(@RequestBody DelayedKnowledge delayedKnowledge){
       return service.create(delayedKnowledge);
    }
}
