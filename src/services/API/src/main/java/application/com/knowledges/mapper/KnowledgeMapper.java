package application.com.knowledges.mapper;

import application.com.common.models.Category;
import application.com.common.services.interfaces.CategoryService;
import application.com.interfaces.AbstractMapper;
import application.com.knowledges.DTO.KnowledgeDTO;
import application.com.knowledges.models.Knowledge;
import application.com.users.models.User;
import application.com.users.services.interfaces.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Objects;

@Component
public class KnowledgeMapper extends AbstractMapper<Knowledge, KnowledgeDTO> {
    private final ModelMapper mapper;
    private final UserService userService;
    private final CategoryService categoryService;

    public KnowledgeMapper(ModelMapper mapper, UserService userService, CategoryService categoryService) {
        super(Knowledge.class, KnowledgeDTO.class);
        this.mapper = mapper;
        this.userService = userService;
        this.categoryService = categoryService;
    }

    @PostConstruct
    public void setupMapper(){
        mapper
                .createTypeMap(Knowledge.class, KnowledgeDTO.class)
                .addMappings(mapper -> {
                    mapper.skip(KnowledgeDTO::setAuthorId);
                    mapper.skip(KnowledgeDTO::setCategoryId);
                })
                .setPostConverter(toDTOConverter());
        mapper
                .createTypeMap(KnowledgeDTO.class, Knowledge.class)
                .addMappings(mapper -> {
                    mapper.skip(Knowledge::setAuthor);
                    mapper.skip(Knowledge::setCategory);
                })
                .setPostConverter(toEntityConverter());
    }

    @Override
    public void mapSpecificFields(Knowledge source, KnowledgeDTO destination){
        destination.setAuthorId(source.getAuthor().getId());
        destination.setCategoryId(source.getCategory().getId());
        destination.setFirstName(source.getAuthor().getFirstName());
        destination.setLastName(source.getAuthor().getLastName());
    }

    @Override
    public void mapSpecificFields(KnowledgeDTO source, Knowledge destination){
        Integer authorId = getAuthorId(source);
        User author = userService.read(authorId);

        Integer categoryId = getCategoryId(source);
        Category category = categoryService.read(categoryId);


        destination.setAuthor(author);
        destination.setCategory(category);
    }


    private Integer getAuthorId (KnowledgeDTO knowledgeDTO){
        return Objects.isNull(knowledgeDTO)
                ? null
                :knowledgeDTO
                .getAuthorId();
    }

    private Integer getCategoryId (KnowledgeDTO knowledgeDTO){
        return Objects.isNull(knowledgeDTO)
                ? null
                :knowledgeDTO
                .getCategoryId();
    }


}
