package application.com.common.controllers.comment;

import application.com.common.DTO.CommentDTO;
import application.com.common.mappers.CommentMapper;
import application.com.common.services.interfaces.CommentService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/comments")
public class UpdateCommentController {
    private final CommentService service;
    private final CommentMapper mapper;

    public UpdateCommentController(CommentService service, CommentMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @PutMapping("/{id}")
    public CommentDTO update(@PathVariable int id, @RequestBody CommentDTO commentDTO) {
        return mapper.toDTO(service.update(id, mapper.toEntity(commentDTO)));
    }
}
