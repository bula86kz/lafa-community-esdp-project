package application.com.users.controllers.certificate;

import application.com.users.DTO.CertificateDTO;
import application.com.users.mappers.CertificateMapper;
import application.com.users.services.interfaces.CertificateService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/certificates")
public class CreateCertificateController {
    private final CertificateService service;
    private final CertificateMapper mapper;

    public CreateCertificateController(CertificateService service, CertificateMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }


    @PostMapping
    public CertificateDTO create(@RequestBody CertificateDTO dto){
        return mapper.toDTO(service.create(mapper.toEntity(dto)));
    }
}
