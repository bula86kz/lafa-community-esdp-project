package application.com.knowledges.models;
import application.com.common.models.Category;
import application.com.interfaces.AbstractEntity;
import application.com.users.models.User;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Builder
@Getter @Setter
@AllArgsConstructor
@Table(name = "knowledges")
@NoArgsConstructor(force = true)
public class Knowledge extends AbstractEntity {
    private String title;
    private String description;
    private String image;

    private boolean isPublished;
    private boolean isPrivate;

    private LocalDateTime publishedDate;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;

    @ManyToOne
    @JoinColumn(name = "author_id")
    private User author;
}
