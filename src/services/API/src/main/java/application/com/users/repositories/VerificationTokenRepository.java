package application.com.users.repositories;

import application.com.users.models.User;
import application.com.users.models.VerificationToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VerificationTokenRepository extends JpaRepository<VerificationToken, Integer> {
    boolean existsByToken(String token);

    VerificationToken findByToken(String token);
    VerificationToken findByUser(User user);
}
