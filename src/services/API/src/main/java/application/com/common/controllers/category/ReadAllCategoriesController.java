package application.com.common.controllers.category;

import application.com.common.models.Category;
import application.com.common.services.interfaces.CategoryService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/categories")
public class ReadAllCategoriesController {
    private final CategoryService service;

    public ReadAllCategoriesController(CategoryService service) {
        this.service = service;
    }

    @GetMapping
    public List<Category> read() {
        return service.read();
    }

}
