package application.com.common.controllers.comment;

import application.com.common.DTO.CommentDTO;
import application.com.common.DTO.CreateCommentDTO;
import application.com.common.mappers.CommentMapper;
import application.com.common.services.interfaces.CommentService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/comments")
public class CreateCommentController {
    private final CommentService service;
    private final CommentMapper mapper;


    public CreateCommentController(CommentService service, CommentMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @PostMapping
    public CommentDTO create(@RequestBody CreateCommentDTO comment) {
        CommentDTO commentDTO = CommentDTO.builder()
                                          .comment(comment.getComment())
                                          .email(comment.getEmail())
                                          .build();

        return mapper.toDTO(service.create(mapper.toEntity(commentDTO), comment.getEntityType(), comment.getEntityId()));
    }
}
