package application.com.users.controllers.certificate;

import application.com.users.DTO.CertificateDTO;
import application.com.users.mappers.CertificateMapper;
import application.com.users.services.interfaces.CertificateService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/certificates")
public class ReadAllCertificateController {
    private final CertificateService service;
    private final CertificateMapper mapper;

    public ReadAllCertificateController(CertificateService service, CertificateMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }


    @GetMapping
    public List<CertificateDTO> readAll() {
        return mapper.toDTOList(service.read());
    }
}
