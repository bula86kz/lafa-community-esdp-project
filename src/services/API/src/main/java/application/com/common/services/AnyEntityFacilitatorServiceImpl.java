package application.com.common.services;

import application.com.common.models.AnyEntityFacilitator;
import application.com.common.repositories.AnyEntityFacilitatorRepository;
import application.com.common.services.interfaces.AnyEntityFacilitatorService;
import application.com.common.specification.AnyEntityFacilitatorSpecification;
import application.com.users.models.User;
import application.com.users.services.interfaces.UserService;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Log4j2
@Service
public class AnyEntityFacilitatorServiceImpl implements AnyEntityFacilitatorService {
    private final AnyEntityFacilitatorRepository repository;

    public AnyEntityFacilitatorServiceImpl(AnyEntityFacilitatorRepository repository) {
        this.repository = repository;
    }

    @Override
    public AnyEntityFacilitator create(AnyEntityFacilitator anyEntityFacilitator) {
        return repository.save(anyEntityFacilitator);
    }

    @Override
    public List<AnyEntityFacilitator> read() {
        return repository.findAll();
    }

    @Override
    public AnyEntityFacilitator read(Integer id) {
        Optional<AnyEntityFacilitator> optionalAnyEntityFacilitator = repository.findById(id);
        return getEntityFacilitatorOrThrowException(optionalAnyEntityFacilitator);
    }

    @Override
    public AnyEntityFacilitator update(Integer id, AnyEntityFacilitator eventFacilitator) {
        Optional<AnyEntityFacilitator> optionalAnyEntityFacilitator = repository.findById(id);
        AnyEntityFacilitator targetAnyEntityFacilitator = getEntityFacilitatorOrThrowException(optionalAnyEntityFacilitator);

        eventFacilitator.setId(id);
        eventFacilitator.setCreated(targetAnyEntityFacilitator.getCreated());
        return repository.save(eventFacilitator);
    }

    @Override
    public void delete(Integer id) {
        repository.deleteById(id);
    }

    private AnyEntityFacilitator getEntityFacilitatorOrThrowException(Optional<AnyEntityFacilitator> anyEntityFacilitator) {
        if (anyEntityFacilitator.isPresent()) {
            return anyEntityFacilitator.get();
        }
        log.error(new EntityNotFoundException("Сущность и пользователь не найдены"));
        throw new EntityNotFoundException("Сущность и пользователь не найдены");
    }

    @Override
    public List<User> sendUsersByEntityTypeAndEntityId(String entityType, Integer entityId){
        var anyEntityFacilitator =  repository.findAll(Specification.where(AnyEntityFacilitatorSpecification.toPredicate(entityType, entityId)));
        return anyEntityFacilitator.stream().map(any -> any.getFacilitator()).collect(Collectors.toList());
    }
}
