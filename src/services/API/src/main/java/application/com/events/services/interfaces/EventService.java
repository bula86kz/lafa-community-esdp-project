package application.com.events.services.interfaces;

import application.com.events.models.Event;
import application.com.interfaces.CRUDService;

import java.util.List;

public interface EventService extends CRUDService<Event> {
    List<Event> readAllByCategory(Integer id);
}
