package application.com.places.controllers.city;

import application.com.places.services.interfaces.CityService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/places/cities")
public class DeleteCityController {
    private final CityService service;

    public DeleteCityController(CityService service) {
        this.service = service;
    }

    @DeleteMapping("/{id}")
    public void delete (@PathVariable int id){
        service.delete(id);
    }
}
