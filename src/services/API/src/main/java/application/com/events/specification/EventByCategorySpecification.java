package application.com.events.specification;

import lombok.AllArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class EventByCategorySpecification<Event> {
    public Specification<Event> findByCategorySpecification(Integer id) {
        return (root, query, cb) -> cb.equal(root.get("category"), id);
    }
}
