package application.com.users.controllers.certificate;

import application.com.users.DTO.CertificateDTO;
import application.com.users.mappers.CertificateMapper;
import application.com.users.services.interfaces.CertificateService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/certificates")
public class ReadCertificateController {
    private final CertificateService service;
    private final CertificateMapper mapper;

    public ReadCertificateController(CertificateService service, CertificateMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }


    @GetMapping("/{id}")
    public CertificateDTO read(@PathVariable Integer id) {
        return mapper.toDTO(service.read(id));
    }

}
