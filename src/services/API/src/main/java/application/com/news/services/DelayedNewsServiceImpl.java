package application.com.news.services;

import application.com.news.exceptions.NewsNotFoundException;
import application.com.news.models.DelayedNews;
import application.com.news.repositories.DelayedNewsRepository;
import application.com.news.services.interfaces.DelayedNewService;
import application.com.common.specification.delayed_posting.DelayedPostingSpecification;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;


@Service
public class DelayedNewsServiceImpl implements DelayedNewService {
    private final DelayedNewsRepository repository;
    private final DelayedPostingSpecification<DelayedNews> specification;

    public DelayedNewsServiceImpl(DelayedNewsRepository repository, DelayedPostingSpecification specification) {
        this.repository = repository;
        this.specification = specification;
    }

    @Override
    public DelayedNews create(DelayedNews delayedNews) {
        return repository.save(delayedNews);
    }

    @Override
    public List<DelayedNews> read() {
        return repository.findAll();
    }


    @Override
    public DelayedNews read(Integer id) {
        Optional<DelayedNews> optionalDelayedNews = repository.findById(id);
        return getDelayedNewsOrThrowException(optionalDelayedNews);
    }

    @Override
    public DelayedNews update(Integer id, DelayedNews delayedNews) {
        Optional<DelayedNews> optionalDelayedNews = repository.findById(id);
        DelayedNews targetDelayedNews = getDelayedNewsOrThrowException(optionalDelayedNews);

        targetDelayedNews.setPostingDate(delayedNews.getPostingDate());

        repository.save(targetDelayedNews);

        return targetDelayedNews;
    }

    @Override
    public void delete(Integer id) {
        repository.deleteById(id);
    }



    private DelayedNews getDelayedNewsOrThrowException(Optional<DelayedNews> delayedNews) {
        if (delayedNews.isPresent()) {
            return delayedNews.get();
        }

        throw new NewsNotFoundException();
    }

    @Override
    public List<DelayedNews> readAllNearest() {
        LocalDateTime currentTime = LocalDateTime.now();
        LocalDateTime currentTimePlusOneHour = LocalDateTime.now().plusHours(1);

        return repository.findAll(Specification.where(specification.delayedPostingSpecification(currentTime, currentTimePlusOneHour)));
    }
}
