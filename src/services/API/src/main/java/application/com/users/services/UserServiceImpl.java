package application.com.users.services;

import application.com.common.services.interfaces.AnyEntityFacilitatorService;
import application.com.users.models.User;
import application.com.users.repositories.UserRepository;
import application.com.users.services.interfaces.UserService;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class UserServiceImpl implements UserService {
    private final UserRepository repository;
    private final PasswordEncoder encoder;
    private final AnyEntityFacilitatorService anyService;

    public UserServiceImpl(UserRepository repository, PasswordEncoder encoder, AnyEntityFacilitatorService anyService) {
        this.repository = repository;
        this.encoder = encoder;
        this.anyService = anyService;
    }

    @Override
    public User create(User user) {
        user.setPassword(encoder.encode(user.getPassword()));
        return repository.save(user);
    }

    @Override
    public List<User> read() {
        return repository.findAll();
    }

    @Override
    public User read(Integer id) {
        Optional<User> optionalUser = repository.findById(id);

        return getUserOrThrowException(optionalUser);
    }

    @Override
    public User update(Integer id, User user) {
        return user;
    }

    @Override
    public void delete(Integer id) {
        repository.deleteById(id);
    }

    @Override
    public UserDetails loadUserByUsername(String email) {
        Optional<User> optionalUser = repository.findByEmail(email);
        User user = getUserOrThrowException(optionalUser);

        String password = user.getPassword();
        Collection<SimpleGrantedAuthority> authorities = new ArrayList<SimpleGrantedAuthority>();

        user
            .getRoles()
            .forEach(role -> authorities.add(new SimpleGrantedAuthority(role.getTitle())));

        return new org.springframework.security.core.userdetails.User(email, password, authorities);
    }

    @Override
    public User getByEmail(String email) {
        return repository.getByEmail(email);
    }

    @Override
    public boolean existByEmail(String email) {
        return repository.existsByEmail(email);
    }

    @Override
    public void enableById(Integer id) {
        User user = repository.getById(id);
        user.setActive(true);

        repository.save(user);
    }

    private User getUserOrThrowException(Optional<User> user) {
        if (user.isPresent()) {
            return user.get();
        }

        throw new UsernameNotFoundException("User not found in the database!");
    }

    @Override
    public List<User> getUsersByEntityTypeAndEntityId(String type, Integer id) {
        return anyService.sendUsersByEntityTypeAndEntityId(type, id);
    }
}
