package application.com.users.controllers.certificate;

import application.com.users.services.interfaces.CertificateService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/certificates")
public class DeleteCertificateController {
    private final CertificateService service;

    public DeleteCertificateController(CertificateService service) {
        this.service = service;
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id){
        service.delete(id);
    }
}
