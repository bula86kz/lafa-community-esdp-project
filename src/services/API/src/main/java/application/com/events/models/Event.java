package application.com.events.models;

import application.com.common.models.Category;
import application.com.common.models.Language;
import application.com.events.models.format.Format;
import application.com.interfaces.AbstractEntity;
import application.com.places.models.City;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Builder
@Getter @Setter
@AllArgsConstructor
@Table(name="events")
@NoArgsConstructor(force = true)
public class Event extends AbstractEntity {
    private String title;
    private String description;
    private String image;

    private LocalDateTime startDate;
    private LocalDateTime finishDate;

    private int price;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;

    @ManyToOne
    @JoinColumn(name = "format_id")
    private Format format;

    @ManyToOne
    @JoinColumn(name = "language_id")
    private Language language;

    @ManyToOne
    @JoinColumn(name = "city_id")
    private City city;
}
