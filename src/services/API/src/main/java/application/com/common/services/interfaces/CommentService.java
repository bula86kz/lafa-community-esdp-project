package application.com.common.services.interfaces;

import application.com.common.models.Comment;
import application.com.common.models.EntityComment;
import application.com.interfaces.CRUDService;

import java.util.List;

public interface CommentService extends CRUDService<Comment> {
    List<Comment> getFilteredCommentsByTypeAndEntityId(String entityType, Integer entityId);
    Comment create(Comment comment, String entityType, Integer entityId);
}
