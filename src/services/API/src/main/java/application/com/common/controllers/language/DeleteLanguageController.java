package application.com.common.controllers.language;

import application.com.common.services.interfaces.LanguageService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/languages")
public class DeleteLanguageController {
    private final LanguageService service;

    public DeleteLanguageController(LanguageService service) {
        this.service = service;
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id){
        service.delete(id);
    }
}
