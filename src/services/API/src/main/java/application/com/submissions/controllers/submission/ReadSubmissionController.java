package application.com.submissions.controllers.submission;

import application.com.submissions.models.Submission;
import application.com.submissions.services.interfaces.SubmissionService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/submissions")
public class ReadSubmissionController {
    private final SubmissionService service;

    public ReadSubmissionController(SubmissionService service){
        this.service = service;
    }

    @GetMapping("/{id}")
    public Submission read (@PathVariable  int id){
        return service.read(id);
    }
}
