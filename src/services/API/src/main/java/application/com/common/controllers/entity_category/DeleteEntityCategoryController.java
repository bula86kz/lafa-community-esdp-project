package application.com.common.controllers.entity_category;

import application.com.common.services.interfaces.EntityCategoryService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/entity-categories")
public class DeleteEntityCategoryController {
    private final EntityCategoryService service;

    public DeleteEntityCategoryController(EntityCategoryService service) {
        this.service = service;
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        service.delete(id);
    }
}
