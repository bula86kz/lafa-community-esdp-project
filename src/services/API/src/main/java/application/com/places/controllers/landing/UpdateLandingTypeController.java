package application.com.places.controllers.landing;

import application.com.places.models.LandingType;
import application.com.places.services.interfaces.LandingTypeService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/places/landing-types")
public class UpdateLandingTypeController {

    private final LandingTypeService service;

    public UpdateLandingTypeController(LandingTypeService service) {
        this.service = service;
    }

    @PutMapping("/{id}")
    public LandingType update(@PathVariable int id, @RequestBody LandingType landingType) {
        return service.update(id, landingType);
    }
}
