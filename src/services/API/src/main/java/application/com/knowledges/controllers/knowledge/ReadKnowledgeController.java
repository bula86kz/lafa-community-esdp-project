package application.com.knowledges.controllers.knowledge;

import application.com.knowledges.DTO.KnowledgeDTO;
import application.com.knowledges.mapper.KnowledgeMapper;
import application.com.knowledges.services.interfaces.KnowledgeService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/knowledges")
public class ReadKnowledgeController {
    private final KnowledgeService service;
    private final KnowledgeMapper mapper;

    public ReadKnowledgeController(KnowledgeService service, KnowledgeMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }


    @GetMapping("/{id}")
    public KnowledgeDTO read(@PathVariable Integer id) {
        return mapper.toDTO(service.read(id));
    }

}
