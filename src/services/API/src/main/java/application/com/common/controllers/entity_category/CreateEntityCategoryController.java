package application.com.common.controllers.entity_category;

import application.com.common.models.EntityCategory;
import application.com.common.services.interfaces.EntityCategoryService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/entity-categories")
public class CreateEntityCategoryController {
    private final EntityCategoryService service;

    public CreateEntityCategoryController(EntityCategoryService service) {
        this.service = service;
    }

    @PostMapping
    public EntityCategory create(@RequestBody EntityCategory entityCategory) {
        return service.create(entityCategory);
    }

}
