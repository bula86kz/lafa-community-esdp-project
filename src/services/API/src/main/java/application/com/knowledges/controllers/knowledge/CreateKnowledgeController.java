package application.com.knowledges.controllers.knowledge;

import application.com.knowledges.DTO.KnowledgeDTO;
import application.com.knowledges.mapper.KnowledgeMapper;
import application.com.knowledges.models.Knowledge;
import application.com.knowledges.services.interfaces.KnowledgeService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("api/knowledges")
public class CreateKnowledgeController {
    private final KnowledgeService service;
    private final KnowledgeMapper mapper;

    public CreateKnowledgeController(KnowledgeService service, KnowledgeMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }


    @PostMapping
    public Knowledge create(@RequestBody KnowledgeDTO dto){
       return service.create(mapper.toEntity(dto));
    }
}
