package application.com.common.specification;

import application.com.common.models.EntityType;
import org.springframework.data.jpa.domain.Specification;

public class EntityTypeSpecification {
    public static Specification<EntityType> toPredicate(String type) {
        return Specification.where(
                (root, query, builder) -> builder.like(root.get("type"), type)
        );
    }
}
