package application.com.submissions.controllers.submission;

import application.com.submissions.DTO.SubmissionDTO;
import application.com.submissions.mappers.SubmissionMapper;
import application.com.submissions.services.interfaces.SubmissionService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/submissions")
public class ReadAllSubmissionController {
    private final SubmissionService service;
    private final SubmissionMapper mapper;

    public ReadAllSubmissionController(SubmissionService service, SubmissionMapper mapper){
        this.service = service;
        this.mapper = mapper;
    }

    @GetMapping
    public List<SubmissionDTO> read () {
        return mapper.toDTOList(service.read());
    }
}
