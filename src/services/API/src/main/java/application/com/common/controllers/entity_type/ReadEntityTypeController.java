package application.com.common.controllers.entity_type;

import application.com.common.models.EntityType;
import application.com.common.services.interfaces.EntityTypeService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/entity-types")
public class ReadEntityTypeController {
    private final EntityTypeService service;

    public ReadEntityTypeController(EntityTypeService service) {
        this.service = service;
    }

    @GetMapping("/{id}")
    public EntityType read(@PathVariable int id) {
        return service.read(id);
    }
}
