package application.com.common.services;

import application.com.common.models.Comment;
import application.com.common.models.EntityComment;
import application.com.common.repositories.CommentRepository;
import application.com.common.services.interfaces.CommentService;
import application.com.common.services.interfaces.EntityCommentService;
import application.com.common.services.interfaces.EntityTypeService;
import application.com.common.specification.CommentSpecification;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Log4j2
@Service
public class CommentServiceImpl implements CommentService {
    private final CommentRepository repository;
    private final EntityCommentService entityCommentService;
    private final EntityTypeService entityTypeService;

    public CommentServiceImpl(CommentRepository repository, EntityCommentService entityCommentService, EntityTypeService entityTypeService) {
        this.repository = repository;
        this.entityCommentService = entityCommentService;
        this.entityTypeService = entityTypeService;
    }

    @Override
    public Comment create(Comment comment) {
        return repository.save(comment);
    }

    @Override
    public Comment create(Comment comment, String entityType, Integer entityId) {
        Comment targetComment = create(comment);
        log.info("Create comment {}", targetComment);

        EntityComment entityComment = EntityComment.builder()
                                                   .comment(targetComment)
                                                   .entityId(entityId)
                                                   .entityType(entityTypeService.read(entityType))
                                                   .build();

        EntityComment targetEntityComment = entityCommentService.create(entityComment);
        log.info("Create entity-comment {}", targetEntityComment);
        return targetComment;
    }

    @Override
    public List<Comment> read() {
        return repository.findAll();
    }

    @Override
    public Comment read(Integer id) {
        Optional<Comment> optionalComment = repository.findById(id);
        return getCommentOrThrowException(optionalComment);
    }

    @Override
    public Comment update(Integer id, Comment comment) {
        Optional<Comment> optionalComment = repository.findById(id);
        Comment targetComment = getCommentOrThrowException(optionalComment);
        comment.setId(id);
        comment.setCreated(targetComment.getCreated());

        log.info("Update comment {}", targetComment);

        return repository.save(comment);
    }

    @Override
    public void delete(Integer id) {
        entityCommentService.delete(id);
        repository.deleteById(id);
        log.info("Delete comment {}", id);
    }

    @Override
    public List<Comment> getFilteredCommentsByTypeAndEntityId(String entityType, Integer id) {
        return repository.findAll(Specification.where(CommentSpecification.toPredicate(entityType, id)));
    }

    private Comment getCommentOrThrowException(Optional<Comment> comment) {
        if (comment.isPresent()) {
            return comment.get();
        }
        log.error(new EntityNotFoundException("Comment not found"));
        throw new EntityNotFoundException("Comment not found");
    }
}
