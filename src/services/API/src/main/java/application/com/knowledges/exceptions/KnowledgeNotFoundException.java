package application.com.knowledges.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND)
public class KnowledgeNotFoundException extends RuntimeException{
    public KnowledgeNotFoundException(String message) {
        super(message);
    }

    public KnowledgeNotFoundException() {
        super("Knowledge not found in database");
    }

}
