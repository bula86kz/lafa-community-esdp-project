package application.com.users.controllers.user;

import application.com.users.models.User;
import application.com.users.services.interfaces.UserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/users")
public class ReadAllUserController {
    private final UserService service;

    public ReadAllUserController(UserService service) {
        this.service = service;
    }

    @GetMapping
    public List<User> read() {
        return service.read();
    }
}
