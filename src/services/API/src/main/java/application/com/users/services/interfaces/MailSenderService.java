package application.com.users.services.interfaces;

import application.com.users.models.User;
import org.springframework.mail.SimpleMailMessage;

public interface MailSenderService
{
    void sendEmailVerificationToken(String token, User user);
    void sendChangePasswordVerificationToken(String token, User user);

    void sendChangeEmailVerificationToken(String token, User user);
    SimpleMailMessage constructEmail(String subject, String message, User user);
}
