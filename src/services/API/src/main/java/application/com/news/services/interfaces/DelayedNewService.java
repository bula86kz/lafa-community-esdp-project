package application.com.news.services.interfaces;

import application.com.interfaces.CRUDService;
import application.com.news.models.DelayedNews;

import java.util.List;


public interface DelayedNewService extends CRUDService<DelayedNews> {
    List<DelayedNews> readAllNearest();
}
