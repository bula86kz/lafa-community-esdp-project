package application.com.common.controllers.comment;

import application.com.common.services.interfaces.CommentService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/comments")
public class DeleteCommentController {
    private final CommentService service;

    public DeleteCommentController(CommentService service) {
        this.service = service;
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id){
        service.delete(id);
    }
}
