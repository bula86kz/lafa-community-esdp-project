package application.com.events.controllers.event;

import application.com.events.DTO.EventDTO;
import application.com.events.mappers.EventMapper;
import application.com.events.services.interfaces.EventService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/events")
public class ReadEventByCategoryController {
    private final EventService service;
    private final EventMapper mapper;

    public ReadEventByCategoryController(EventService service, EventMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @GetMapping("/category/{id}")
    public List<EventDTO> readByCategory(@PathVariable Integer id) {
        return mapper.toDTOList(service.readAllByCategory(id));
    }
}
