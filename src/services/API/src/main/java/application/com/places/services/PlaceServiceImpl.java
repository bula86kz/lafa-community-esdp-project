package application.com.places.services;

import application.com.places.models.Place;
import application.com.places.repositories.PlaceRepository;
import application.com.places.services.interfaces.PlaceService;
import application.com.places.specification.PlaceSpecification;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Service
public class PlaceServiceImpl implements PlaceService {

    private final PlaceRepository repository;

    public PlaceServiceImpl(PlaceRepository repository) {
        this.repository = repository;
    }

    @Override
    public Place create(Place place) {
        return repository.save(place);
    }

    @Override
    public List<Place> read() {
        return repository.findAll();
    }

    @Override
    public Place read(Integer id) {
        Optional<Place> optionalPlace = repository.findById(id);
        return getPlaceOrThrowException(optionalPlace);
    }

    @Override
    public Place update(Integer id, Place place) {
        Optional<Place> optionalPlace = repository.findById(id);
        Place targetPlace = getPlaceOrThrowException(optionalPlace);
        return repository.save(targetPlace);
    }

    @Override
    public void delete(Integer id) {
        repository.deleteById(id);
    }

    private Place getPlaceOrThrowException(Optional<Place> place){
        if(place.isPresent()){
            return place.get();
        }
        throw new EntityNotFoundException("Can't find places with the ID");
    }

    @Override
    public List<Place> getPlaceByLandingSlug(String slug) {
        return  repository.findAll(Specification.where(PlaceSpecification.placeByLandingSlug(slug)));
    }
}

