package application.com.submissions.controllers.goal;

import application.com.submissions.DTO.GoalDTO;
import application.com.submissions.mappers.GoalMapper;
import application.com.submissions.services.interfaces.GoalService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/calculators/goals")
public class ReadGoalController {
    private final GoalService service;
    private final GoalMapper mapper;

    public ReadGoalController(GoalService service, GoalMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @GetMapping("/{id}")
    public GoalDTO read (@PathVariable int id) {
        return mapper.toDTO(service.read(id));
    }
}
