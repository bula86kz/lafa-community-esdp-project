package application.com.places.controllers.landing;

import application.com.places.models.LandingType;
import application.com.places.services.interfaces.LandingTypeService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/places/landing-types")
public class ReadLandingTypeController {
    private final LandingTypeService service;

    public ReadLandingTypeController(LandingTypeService service) {
        this.service = service;
    }

    @GetMapping("/{id}")
    public LandingType read(@PathVariable Integer id) {
        return service.read(id);
    }
}
