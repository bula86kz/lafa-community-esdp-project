package application.com.events.controllers.format;

import application.com.events.DTO.FormatDTO;
import application.com.events.mappers.FormatMapper;
import application.com.events.models.format.Format;
import application.com.events.services.interfaces.FormatService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/formats")
public class CreateFormatController {
    private final FormatService service;
    private final FormatMapper mapper;

    public CreateFormatController(FormatService service, FormatMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @PostMapping
    public Format create(@RequestBody FormatDTO formatDTO) {
        return service.create(mapper.toEntity(formatDTO));
    }
}
