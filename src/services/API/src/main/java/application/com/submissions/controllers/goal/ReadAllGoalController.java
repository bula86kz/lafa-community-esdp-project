package application.com.submissions.controllers.goal;

import application.com.submissions.DTO.GoalDTO;
import application.com.submissions.mappers.GoalMapper;
import application.com.submissions.services.interfaces.GoalService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/calculators/goals")
public class ReadAllGoalController {
    private final GoalService service;
    private final GoalMapper mapper;

    public ReadAllGoalController(GoalService service, GoalMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @GetMapping
    public List<GoalDTO> read () {
        return mapper.toDTOList(service.read());
    }
}
