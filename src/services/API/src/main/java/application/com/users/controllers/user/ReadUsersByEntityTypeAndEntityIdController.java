package application.com.users.controllers.user;

import application.com.users.DTO.UserDTO;
import application.com.users.mappers.UserMapper;
import application.com.users.models.User;
import application.com.users.services.interfaces.UserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/users")
public class ReadUsersByEntityTypeAndEntityIdController {
    private final UserService service;
    private final UserMapper mapper;

    public ReadUsersByEntityTypeAndEntityIdController(UserService service, UserMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @GetMapping("/{entityType}/{id}")
    public List<UserDTO> readUsersByTypeAndEntityId(@PathVariable String entityType, @PathVariable Integer id) {
        List<User> users = service.getUsersByEntityTypeAndEntityId(entityType, id);
        var userDTOS = mapper.toDTOList(users);
        return userDTOS;
    }
}
