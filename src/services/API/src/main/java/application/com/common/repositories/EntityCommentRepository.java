package application.com.common.repositories;

import application.com.common.models.EntityComment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface EntityCommentRepository extends JpaRepository<EntityComment, Integer>, JpaSpecificationExecutor<EntityComment> {
}
