package application.com.users.controllers.registration;

import application.com.common.exceptions.EntityAlreadyExistsException;
import application.com.users.DTO.UserDTO;
import application.com.users.events.RegistrationCompleteEvent;
import application.com.users.mappers.UserMapper;
import application.com.users.models.User;
import application.com.users.services.interfaces.UserService;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/users")
public class RegistrationUserController {
    private final UserService service;
    private final UserMapper mapper;

    private final ApplicationEventPublisher publisher;

    public RegistrationUserController(UserService service, UserMapper mapper, ApplicationEventPublisher publisher) {
        this.service = service;
        this.mapper = mapper;

        this.publisher = publisher;
    }

    @PostMapping("/registration")
    public ResponseEntity<?> registration(@RequestBody UserDTO userDTO, HttpServletRequest request) {
        String email = userDTO.getEmail();

        if (service.existByEmail(email)) {
            throw new EntityAlreadyExistsException("User with email - " + email + " already exists!");
        }

        User user = service.create(mapper.toEntity(userDTO));

        publisher.publishEvent(new RegistrationCompleteEvent(user, request.getLocale()));

        return ResponseEntity
                            .status(HttpStatus.CREATED)
                            .body(user);
    }
}
