package application.com.places.controllers.city;

import application.com.places.models.City;
import application.com.places.services.interfaces.CityService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/places/cities")
public class UpdateCityController {
    private final CityService service;

    public UpdateCityController(CityService service) {
        this.service = service;
    }

    @PutMapping("/{id}")
    public City update(@PathVariable int id, @RequestBody City city){
        return service.update(id,city);
    }
}
