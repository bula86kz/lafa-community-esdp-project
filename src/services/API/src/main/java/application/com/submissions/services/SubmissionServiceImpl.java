package application.com.submissions.services;

import application.com.submissions.models.Submission;
import application.com.submissions.repositories.SubmissionRepository;
import application.com.submissions.services.interfaces.SubmissionService;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Log4j2
@Service
public class SubmissionServiceImpl implements SubmissionService {
    private final SubmissionRepository repository;

    public SubmissionServiceImpl(SubmissionRepository repository){
        this.repository = repository;
    }

    @Override
    public Submission create(Submission submission) {
        return repository.save(submission);
    }

    @Override
    public List<Submission> read() {
        return repository.findAll();
    }

    @Override
    public Submission read(Integer id) {
        Optional<Submission> optionalSubmission = repository.findById(id);
        return getSubmissionOrThrowException(optionalSubmission);
    }

    @Override
    public Submission update(Integer id, Submission submission) {
        Optional<Submission> optionalSubmission = repository.findById(id);
        Submission targetSubmission = getSubmissionOrThrowException(optionalSubmission);

        submission.setId(id);
        submission.setCreated(targetSubmission.getCreated());

        return repository.save(submission);
    }

    @Override
    public void delete(Integer id) {
        repository.deleteById(id);
    }

    private Submission getSubmissionOrThrowException(Optional<Submission> submission) {
        if (submission.isPresent()){
            return submission.get();
        }
        log.error(new EntityNotFoundException("Can't find submission with the ID"));
        throw new EntityNotFoundException("Can't find submission with the ID");
    }
}
