package application.com.common.services;

import application.com.common.models.Category;
import application.com.common.models.EntityCategory;
import application.com.common.repositories.EntityCategoryRepository;
import application.com.common.services.interfaces.CategoryService;
import application.com.common.services.interfaces.EntityCategoryService;
import application.com.common.specification.EntityCategorySpecification;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Log4j2
@Service
public class EntityCategoryServiceImpl implements EntityCategoryService {
    private final EntityCategoryRepository repository;
    private final CategoryService categoryService;

    public EntityCategoryServiceImpl(EntityCategoryRepository repository, CategoryService categoryService) {
        this.repository = repository;
        this.categoryService = categoryService;
    }

    @Override
    public EntityCategory create(EntityCategory entityCategory) {
        return repository.save(entityCategory);
    }

    @Override
    public List<EntityCategory> read() {
        return repository.findAll();
    }

    @Override
    public EntityCategory read(Integer id) {
        Optional<EntityCategory> optionalEntityCategory = repository.findById(id);
        return getEntityCategoryOrThrowException(optionalEntityCategory);
    }

    @Override
    public EntityCategory update(Integer id, EntityCategory entityCategory) {
        Optional<EntityCategory> optionalEntityCategory = repository.findById(id);
        EntityCategory targetEntityCategory = getEntityCategoryOrThrowException(optionalEntityCategory);
        targetEntityCategory.setCategory(entityCategory.getCategory());
        targetEntityCategory.setEntityType(entityCategory.getEntityType());
        targetEntityCategory.setUpdated(LocalDateTime.now());
        return repository.save(targetEntityCategory);
    }

    @Override
    public void delete(Integer id) {
        repository.deleteById(id);
    }

    private EntityCategory getEntityCategoryOrThrowException(Optional<EntityCategory> entityCategory) {
        if (entityCategory.isPresent()) {
            return entityCategory.get();
        }
        log.error(new EntityNotFoundException("Категория и Типы не найдены"));
        throw new EntityNotFoundException("Категория и Типы не найдены");
    }

    public List<EntityCategory> getEntityCategoriesByType(String type){
        List<EntityCategory> entityCategories = repository.findAll(Specification.where(EntityCategorySpecification.toPredicate(type)));
        return entityCategories;
    }

    public List<Category> getCategoriesByType(String type){
        return categoryService.getFilteredCategoriesByType(type);
    }
}
