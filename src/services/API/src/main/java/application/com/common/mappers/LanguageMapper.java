package application.com.common.mappers;

import application.com.common.DTO.LanguageDTO;
import application.com.common.models.Language;
import application.com.interfaces.AbstractMapper;
import org.springframework.stereotype.Component;

@Component
public class LanguageMapper extends AbstractMapper<Language, LanguageDTO> {

    public LanguageMapper() {
        super(Language.class, LanguageDTO.class);
    }
}
