package application.com.submissions.mappers;

import application.com.common.models.Language;
import application.com.common.services.interfaces.LanguageService;
import application.com.interfaces.AbstractMapper;
import application.com.places.models.City;
import application.com.places.services.interfaces.CityService;
import application.com.submissions.DTO.SubmissionDTO;
import application.com.submissions.models.PreferredCommunication;
import application.com.submissions.models.Submission;
import application.com.submissions.services.interfaces.PreferredCommunicationService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Objects;

@Component
public class SubmissionMapper extends AbstractMapper<Submission, SubmissionDTO> {
    private final ModelMapper mapper;
    private final CityService cityService;
    private final LanguageService languageService;
    private final PreferredCommunicationService preferredCommunicationService;

    public SubmissionMapper(ModelMapper mapper, CityService cityService, LanguageService languageService, PreferredCommunicationService preferredCommunicationService) {
        super(Submission.class, SubmissionDTO.class);
        this.mapper = mapper;
        this.cityService = cityService;
        this.languageService = languageService;
        this.preferredCommunicationService = preferredCommunicationService;
    }

    @PostConstruct
    public void setupMapper(){
        mapper
                .createTypeMap(Submission.class, SubmissionDTO.class)
                .addMappings(mapper -> {
                    mapper.skip(SubmissionDTO::setCityId);
                    mapper.skip(SubmissionDTO::setLanguageId);
                    mapper.skip(SubmissionDTO::setPreferredCommunicationId);
                })
                .setPostConverter(toDTOConverter());
        mapper
                .createTypeMap(SubmissionDTO.class, Submission.class)
                .addMappings(mapper -> {
                    mapper.skip(Submission::setCity);
                    mapper.skip(Submission::setLanguage);
                    mapper.skip(Submission::setPreferredCommunication);
                })
                .setPostConverter(toEntityConverter());
    }

    @Override
    public void mapSpecificFields(Submission source, SubmissionDTO destination){
        destination.setCityId(source.getCity().getId());
        destination.setLanguageId(source.getLanguage().getId());
        destination.setPreferredCommunicationId(source.getPreferredCommunication().getId());
    }

    @Override
    public void mapSpecificFields(SubmissionDTO source, Submission destination){
        Integer cityId = getCityId(source);
        City city = cityService.read(cityId);

        Integer languageId = getLanguageId(source);
        Language language = languageService.read(languageId);

        Integer preferredCommunicationId = getPreferredCommunicationId(source);
        PreferredCommunication preferredCommunication = preferredCommunicationService.read(preferredCommunicationId);

        destination.setCity(city);
        destination.setLanguage(language);
        destination.setPreferredCommunication(preferredCommunication);
    }

    private Integer getCityId (SubmissionDTO submissionDTO){
        return Objects.isNull(submissionDTO)
                ? null
                : submissionDTO
                .getCityId();
    }

    private Integer getLanguageId (SubmissionDTO submissionDTO){
        return Objects.isNull(submissionDTO)
                ? null
                : submissionDTO
                .getLanguageId();
    }

    private Integer getPreferredCommunicationId (SubmissionDTO submissionDTO){
        return Objects.isNull(submissionDTO)
                ? null
                : submissionDTO
                .getPreferredCommunicationId();
    }
}
