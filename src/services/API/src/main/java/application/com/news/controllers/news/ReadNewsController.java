package application.com.news.controllers.news;

import application.com.news.models.News;
import application.com.news.services.interfaces.NewsService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/news")
public class ReadNewsController {
    private final NewsService service;

    public ReadNewsController(NewsService service) {
        this.service = service;
    }


    @GetMapping("/{id}")
    public News read(@PathVariable Integer id) {
        return service.read(id);
    }

}
