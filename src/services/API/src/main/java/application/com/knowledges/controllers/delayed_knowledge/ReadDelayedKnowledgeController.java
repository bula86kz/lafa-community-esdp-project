package application.com.knowledges.controllers.delayed_knowledge;

import application.com.knowledges.models.DelayedKnowledge;
import application.com.knowledges.services.interfaces.DelayedKnowledgeService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/knowledges/delayed")
public class ReadDelayedKnowledgeController {
    private final DelayedKnowledgeService service;

    public ReadDelayedKnowledgeController(DelayedKnowledgeService service) {
        this.service = service;
    }


    @GetMapping("/{id}")
    public DelayedKnowledge read(@PathVariable Integer id) {
        return service.read(id);
    }

}
