package application.com.common.controllers.language;

import application.com.common.DTO.LanguageDTO;
import application.com.common.mappers.LanguageMapper;
import application.com.common.services.interfaces.LanguageService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/languages")
public class ReadAllLanguageController {
    private final LanguageService service;
    private final LanguageMapper mapper;


    public ReadAllLanguageController(LanguageService service, LanguageMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @GetMapping
    public List<LanguageDTO> read() {
        return mapper.toDTOList(service.read());
    }
}
