package application.com.users.controllers.role;

import application.com.users.models.Role;
import application.com.users.services.interfaces.RoleService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
@RequestMapping("/api/roles")
public class ReadAllRoleController {
    private final RoleService service;

    public ReadAllRoleController(RoleService service) {
        this.service = service;
    }

    @GetMapping
    public List<Role> read(){
        return service.read();
    }
}
