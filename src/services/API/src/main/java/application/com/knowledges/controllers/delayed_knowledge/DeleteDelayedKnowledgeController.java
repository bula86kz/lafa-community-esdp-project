package application.com.knowledges.controllers.delayed_knowledge;

import application.com.knowledges.services.interfaces.DelayedKnowledgeService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/knowledges/delayed")
public class DeleteDelayedKnowledgeController {
    private final DelayedKnowledgeService service;

    public DeleteDelayedKnowledgeController(DelayedKnowledgeService service) {
        this.service = service;
    }


    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id) {
        service.delete(id);
    }
}
