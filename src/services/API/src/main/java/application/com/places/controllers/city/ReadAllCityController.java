package application.com.places.controllers.city;

import application.com.places.models.City;
import application.com.places.services.interfaces.CityService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/places/cities")
public class ReadAllCityController {
    private final CityService service;

    public ReadAllCityController(CityService service) {
        this.service = service;
    }

    @GetMapping
    public List<City> read(){
        return service.read();
    }
}
