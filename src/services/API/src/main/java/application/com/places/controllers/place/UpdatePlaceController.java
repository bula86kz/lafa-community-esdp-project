package application.com.places.controllers.place;

import application.com.places.models.Place;
import application.com.places.services.interfaces.PlaceService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/places")
public class UpdatePlaceController {
    private final PlaceService service;

    public UpdatePlaceController(PlaceService service) {
        this.service = service;
    }

    @PutMapping("/{id}")
    public Place update(@PathVariable int id, @RequestBody Place place){
        return service.update(id,place);
    }
}
