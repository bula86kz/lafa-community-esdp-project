package application.com.submissions.controllers.preferred_communication;

import application.com.submissions.models.PreferredCommunication;
import application.com.submissions.services.interfaces.PreferredCommunicationService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/submissions/preferred-communications")
public class UpdatePreferredCommunicationController {
    private final PreferredCommunicationService service;

    public UpdatePreferredCommunicationController(PreferredCommunicationService service) {
        this.service = service;
    }

    @PutMapping("/{id}")
    public PreferredCommunication update(@PathVariable int id, @RequestBody PreferredCommunication preferredCommunication){
        return service.update(id, preferredCommunication);
    }
}
