package application.com.submissions.controllers.submission;

import application.com.submissions.services.interfaces.SubmissionService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/submissions")
public class DeleteSubmissionController {
    private final SubmissionService service;

    public DeleteSubmissionController(SubmissionService service){
        this.service = service;
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id){
        service.delete(id);
    }

}
