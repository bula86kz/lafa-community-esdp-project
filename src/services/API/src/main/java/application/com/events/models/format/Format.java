package application.com.events.models.format;

import application.com.interfaces.AbstractEntity;
import com.sun.istack.NotNull;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Builder
@Getter @Setter
@AllArgsConstructor
@Table(name = "formats")
@NoArgsConstructor(force = true)
public class Format extends AbstractEntity {
    private String title;
}
