package application.com.news.controllers.news;
import application.com.news.DTO.NewsDTO;
import application.com.news.mapper.NewsMapper;
import application.com.news.models.News;
import application.com.news.services.interfaces.NewsService;
import org.springframework.web.bind.annotation.*;



@RestController
@RequestMapping("api/news")
public class CreateNewsController {
    private final NewsService service;
    private final NewsMapper mapper;

    public CreateNewsController(NewsService service, NewsMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @PostMapping
    public News create(@RequestBody NewsDTO dto){
       return service.create(mapper.toEntity(dto));
    }
}
