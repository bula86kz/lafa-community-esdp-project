package application.com.common.controllers.language;

import application.com.common.models.Language;
import application.com.common.services.interfaces.LanguageService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/languages")
public class ReadLanguageController {
    private final LanguageService service;

    public ReadLanguageController(LanguageService service) {
        this.service = service;
    }

    @GetMapping("/{id}")
    public Language read(@PathVariable int id) {
        return service.read(id);
    }
}
