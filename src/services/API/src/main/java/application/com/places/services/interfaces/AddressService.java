package application.com.places.services.interfaces;

import application.com.interfaces.CRUDService;
import application.com.places.models.Address;

public interface AddressService extends CRUDService<Address> {
}
