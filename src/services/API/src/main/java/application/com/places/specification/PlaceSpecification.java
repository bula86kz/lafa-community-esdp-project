package application.com.places.specification;

import application.com.places.models.LandingType;
import application.com.places.models.Place;
import org.springframework.data.jpa.domain.Specification;
import javax.persistence.criteria.Join;

public class PlaceSpecification {

    public static Specification<Place> placeByLandingSlug(String slug) {
        return (root, query, criteriaBuilder) -> {
            Join< LandingType, Place > landingType = root.join("landingType");
            return criteriaBuilder.like(landingType.get("slug"), slug);
        };
    }
}
