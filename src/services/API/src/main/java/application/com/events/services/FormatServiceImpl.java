package application.com.events.services;

import application.com.events.models.format.Format;
import application.com.events.repositories.format.FormatRepository;
import application.com.events.services.interfaces.FormatService;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Log4j2
@Service
public class FormatServiceImpl implements FormatService {
    private final FormatRepository repository;

    public FormatServiceImpl(FormatRepository repository) {
        this.repository = repository;
    }

    @Override
    public Format create(Format format) {
        return repository.save(format);
    }

    @Override
    public List<Format> read() {
        return repository.findAll();
    }

    @Override
    public Format read(Integer id) {
        Optional<Format> optionalFormat = repository.findById(id);
        return getFormatOrThrowException(optionalFormat);
    }

    @Override
    public Format update(Integer id, Format format) {
        Optional<Format> optionalFormat = repository.findById(id);
        Format targetFormat = getFormatOrThrowException(optionalFormat);

        format.setId(id);
        format.setCreated(targetFormat.getCreated());
        return repository.save(format);
    }

    @Override
    public void delete(Integer id) {
        repository.deleteById(id);
    }

    private Format getFormatOrThrowException(Optional<Format> format) {
        if (format.isPresent()) {
            return format.get();
        }
        log.error(new EntityNotFoundException("Can't find format with the ID"));
        throw new EntityNotFoundException("Can't find format with the ID");
    }
}
