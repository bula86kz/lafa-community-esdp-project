package application.com.submissions.controllers.submission;

import application.com.submissions.DTO.SubmissionDTO;
import application.com.submissions.mappers.SubmissionMapper;
import application.com.submissions.models.Submission;
import application.com.submissions.services.interfaces.SubmissionService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/submissions")
public class UpdateSubmissionController {
    private final SubmissionService service;
    private final SubmissionMapper mapper;

    public UpdateSubmissionController(SubmissionService service, SubmissionMapper mapper){
        this.service = service;
        this.mapper = mapper;
    }

    @PutMapping("/{id}")
    public Submission update(@PathVariable int id, @RequestBody SubmissionDTO submissionDTO){
        return service.update(id, mapper.toEntity(submissionDTO));
    }
}
