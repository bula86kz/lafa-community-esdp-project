package application.com.submissions.controllers.preferred_communication;

import application.com.submissions.models.PreferredCommunication;
import application.com.submissions.services.interfaces.PreferredCommunicationService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/submissions/preferred-communications")
public class ReadAllPreferredCommunicationController {
    private final PreferredCommunicationService service;

    public ReadAllPreferredCommunicationController(PreferredCommunicationService service) {
        this.service = service;
    }

    @GetMapping
    List<PreferredCommunication> read (){
        return service.read();
    }
}
