package application.com.users.DTO;

import application.com.interfaces.AbstractDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
public class CertificateDTO extends AbstractDTO {
    @NotBlank
    private String image;
    @NotBlank
    private String title;
    @NotBlank
    private String description;

    @NotBlank
    private LocalDateTime startDate;
    @NotBlank
    private LocalDateTime finishDate;

    @NotBlank
    private String competence;
    @NotBlank
    private String addressOfCourse;
    @NotBlank
    private String organizerFullname;
    @NotBlank
    private String organizerPhoneNumber;

    @NotBlank
    private Integer formatId;

    @NotBlank
    private Integer userId;
}
