package application.com.common.specification;


import lombok.AllArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class KnowledgeByCategorySpecification<T> {
    public Specification<T> findByCategorySpecification(Integer id) {
        return (root, query, cb) -> cb.equal(root.get("category"), id);
    }
}
