package application.com.users.models;

import application.com.interfaces.AbstractEntity;
import lombok.*;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Date;

@Entity
@Builder
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
@Table(name = "verification_tokens")
public class VerificationToken extends AbstractEntity {
    private static final int EXPIRATION = 60 * 24;

    @OneToOne(targetEntity = User.class, fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "user_id")
    private User user;

    private String token;

    private Date expiryDate;

    public VerificationToken(User user, String token) {
        this.user = user;
        this.token = token;

        this.expiryDate = calculateExpiryDate();
    }

    public void updateToken(String token) {
        this.token = token;
        this.expiryDate = calculateExpiryDate();
    }

    private Date calculateExpiryDate() {
        Calendar calendar = Calendar.getInstance();

        calendar.setTimeInMillis(new Date().getTime());
        calendar.add(Calendar.MINUTE, VerificationToken.EXPIRATION);

        long time = calendar
                        .getTime()
                        .getTime();

        return new Date(time);
    }
}
