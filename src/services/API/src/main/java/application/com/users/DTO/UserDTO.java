package application.com.users.DTO;

import application.com.interfaces.AbstractDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
public class UserDTO extends AbstractDTO
{
    @NotBlank
    private String firstName;
    @NotBlank
    private String lastName;

    @Email
    @NotBlank
    @Size(min = 3, max = 256)
    private String email;
    @NotBlank
    @Size(min = 8, max = 8)
    private String phoneNumber;

    @NotBlank
    @Size(min = 8, max = 25)
    private String password;

    private String aboutMe;

}
