package application.com.places.models;

import application.com.interfaces.AbstractEntity;
import lombok.*;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Builder
@Getter @Setter
@AllArgsConstructor
@Table(name = "places")
@NoArgsConstructor(force = true)
public class Place extends AbstractEntity {
    private String title;
    private String photo;

    @ManyToOne
    private Address address;

    @ManyToOne
    private LandingType landingType;
}