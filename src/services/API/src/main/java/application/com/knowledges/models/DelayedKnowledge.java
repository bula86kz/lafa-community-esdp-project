package application.com.knowledges.models;

import application.com.interfaces.AbstractEntity;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Builder
@Getter @Setter
@AllArgsConstructor
@Table(name = "delayed_knowledges")
@NoArgsConstructor(force = true)
public class DelayedKnowledge extends AbstractEntity {
    private LocalDateTime postingDate;

    @ManyToOne
    @JoinColumn(name = "knowledge_id")
    private Knowledge knowledge;
}
