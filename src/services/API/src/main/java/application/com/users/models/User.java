package application.com.users.models;

import application.com.interfaces.AbstractEntity;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.Collection;

@Entity
@Builder
@Getter @Setter
@AllArgsConstructor
@Table(name = "users")
@NoArgsConstructor(force = true)
public class User extends AbstractEntity {
    private String firstName;
    private String lastName;

    private String email;
    private String phoneNumber;

    private String password;

    private String photo;
    private String gender;

    private String aboutMe;

    private boolean isActive;

    @Builder.Default
    @ManyToMany(fetch = FetchType.EAGER)
    private Collection<Role> roles = new ArrayList<Role>();
}
