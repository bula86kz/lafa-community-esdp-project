package application.com.news.controllers.delayed_news;

import application.com.news.services.interfaces.DelayedNewService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/news/delayed")
public class DeleteDelayedNewsController {
    private final DelayedNewService service;

    public DeleteDelayedNewsController(DelayedNewService service) {
        this.service = service;
    }


    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id) {
        service.delete(id);
    }
}
