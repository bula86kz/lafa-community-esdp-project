package application.com.places.models;

import application.com.interfaces.AbstractEntity;
import lombok.*;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Builder
@Getter @Setter
@AllArgsConstructor
@Table(name = "addresses")
@NoArgsConstructor(force = true)
public class Address extends AbstractEntity {
    private String street;
    private String buildingNumber;
    private String apartamentNumber;

    @ManyToOne
    private City city;
}
