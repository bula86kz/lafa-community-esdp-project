package application.com.places.services;

import application.com.places.models.Address;
import application.com.places.repositories.AddressRepository;
import application.com.places.services.interfaces.AddressService;
import org.springframework.stereotype.Service;
import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Service
public class AddressServiceImpl implements AddressService {

    private final AddressRepository repository;

    public AddressServiceImpl(AddressRepository repository) {
        this.repository = repository;
    }

    @Override
    public Address create(Address address) {
        return repository.save(address);
    }

    @Override
    public List<Address> read() {
        return repository.findAll();
    }

    @Override
    public Address read(Integer id) {
        Optional<Address> optionalAddress = repository.findById(id);
        return getAddressOrThrowException(optionalAddress);
    }

    @Override
    public Address update(Integer id, Address address) {
        Optional<Address> optionalAddress = repository.findById(id);
        Address targetAddress = getAddressOrThrowException(optionalAddress);
        return repository.save(targetAddress);
    }

    @Override
    public void delete(Integer id) {
        repository.deleteById(id);
    }

    private Address getAddressOrThrowException(Optional<Address> address){
        if(address.isPresent()){
            return address.get();
        }
        throw new EntityNotFoundException("Can't find address with the ID");
    }
}
