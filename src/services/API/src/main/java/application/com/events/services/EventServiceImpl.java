package application.com.events.services;

import application.com.events.models.Event;
import application.com.events.repositories.EventRepository;
import application.com.events.services.interfaces.EventService;
import application.com.events.specification.EventByCategorySpecification;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Log4j2
@Service
public class EventServiceImpl implements EventService {
    private final EventRepository repository;
    private final EventByCategorySpecification<Event> eventByCategorySpecification;

    public EventServiceImpl(EventRepository repository, EventByCategorySpecification<Event> eventByCategorySpecification) {
        this.repository = repository;
        this.eventByCategorySpecification = eventByCategorySpecification;
    }

    @Override
    public Event create(Event event) {
        return repository.save(event);
    }

    @Override
    public List<Event> read() {
        return repository.findAll();
    }

    @Override
    public Event read(Integer id) {
        Optional<Event> optionalEvent = repository.findById(id);
        return getEventOrThrowException(optionalEvent);
    }

    @Override
    public Event update(Integer id, Event event) {
        Optional<Event> optionalEvent = repository.findById(id);
        Event targetEvent = getEventOrThrowException(optionalEvent);

        event.setId(id);
        event.setCreated(targetEvent.getCreated());
        return repository.save(event);
    }

    @Override
    public void delete(Integer id) {
        repository.deleteById(id);
    }

    private Event getEventOrThrowException(Optional<Event> event) {
        if (event.isPresent()) {
            return event.get();
        }
        log.error(new EntityNotFoundException("Мероприятие не найдено"));
        throw new EntityNotFoundException("Мероприятие не найдено");
    }

    @Override
    public List<Event> readAllByCategory(Integer id) {
        return repository.findAll(Specification.where(eventByCategorySpecification.findByCategorySpecification(id)));
    }
}
