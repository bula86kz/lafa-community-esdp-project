package application.com.users.controllers.role;

import application.com.users.models.Role;
import application.com.users.services.interfaces.RoleService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/roles")
public class CreateRoleController {
    private final RoleService service;

    public CreateRoleController(RoleService service) {
        this.service = service;
    }

    @PostMapping
    public Role create(@RequestBody Role role){
        return service.create(role);
    }
}
