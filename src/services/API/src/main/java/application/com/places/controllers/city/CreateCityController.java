package application.com.places.controllers.city;

import application.com.places.models.City;
import application.com.places.services.interfaces.CityService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/places/cities")
public class CreateCityController {
    private final CityService service;

    public CreateCityController(CityService service) {
        this.service = service;
    }

    @PostMapping
    public City create(@RequestBody City city){
        return service.create(city);
    }
}
