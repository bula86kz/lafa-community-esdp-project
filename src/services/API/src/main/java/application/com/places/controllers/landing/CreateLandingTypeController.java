package application.com.places.controllers.landing;

import application.com.places.models.LandingType;
import application.com.places.services.interfaces.LandingTypeService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/places/landing-types")
public class CreateLandingTypeController {
    private final LandingTypeService service;

    public CreateLandingTypeController(LandingTypeService service) {
        this.service = service;
    }

    @PostMapping
    public LandingType create(@RequestBody LandingType landingType) {
        return service.create(landingType);
    }
}
