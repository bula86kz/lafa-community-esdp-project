package application.com.submissions.mappers;

import application.com.interfaces.AbstractMapper;
import application.com.submissions.DTO.GoalDTO;
import application.com.submissions.models.Goal;
import org.springframework.stereotype.Component;

@Component
public class GoalMapper extends AbstractMapper<Goal, GoalDTO> {

    public GoalMapper() {
        super(Goal.class, GoalDTO.class);
    }
}
