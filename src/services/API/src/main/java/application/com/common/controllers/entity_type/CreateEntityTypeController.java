package application.com.common.controllers.entity_type;

import application.com.common.models.EntityType;
import application.com.common.services.interfaces.EntityTypeService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/entity-types")
public class CreateEntityTypeController {
    private final EntityTypeService service;

    public CreateEntityTypeController(EntityTypeService service) {
        this.service = service;
    }

    @PostMapping
    public EntityType create(@RequestBody EntityType entityType) {
        return service.create(entityType);
    }

}
