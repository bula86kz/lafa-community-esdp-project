package application.com.submissions.repositories;

import application.com.submissions.models.PreferredCommunication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PreferredCommunicationRepository extends JpaRepository<PreferredCommunication, Integer> {
}
