package application.com.users.services;

import application.com.users.exceptions.CertificateNotFoundException;
import application.com.users.models.Certificate;
import application.com.users.repositories.CertificateRepository;
import application.com.users.services.interfaces.CertificateService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CertificateServiceImpl implements CertificateService {
    private final CertificateRepository repository;

    public CertificateServiceImpl(CertificateRepository repository) {
        this.repository = repository;
    }


    @Override
    public Certificate create(Certificate certificate) {
        return repository.save(certificate);
    }

    @Override
    public List<Certificate> read() {
        return repository.findAll();
    }

    @Override
    public Certificate read(Integer id) {
        Optional<Certificate> optionalCertificate = repository.findById(id);
        return getCertificateOrThrowException(optionalCertificate);
    }

    @Override
    public Certificate update(Integer id, Certificate certificate) {
        Optional<Certificate> optionalCertificate = repository.findById(id);
        Certificate targetCertificate = getCertificateOrThrowException(optionalCertificate);

        certificate.setId(id);
        certificate.setCreated(targetCertificate.getCreated());

        return repository.save(certificate);
    }

    @Override
    public void delete(Integer id) {
        repository.deleteById(id);
    }




    private Certificate getCertificateOrThrowException(Optional<Certificate> certificate) {
        if (certificate.isPresent()) {
            return certificate.get();
        }

        throw new CertificateNotFoundException();
    }
}
