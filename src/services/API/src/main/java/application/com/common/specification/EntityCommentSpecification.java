package application.com.common.specification;

import application.com.common.models.EntityComment;
import org.springframework.data.jpa.domain.Specification;

public class EntityCommentSpecification {
    public static Specification<EntityComment> toPredicate(Integer commentId) {
        return Specification.where(
                (root, query, builder) -> builder.equal(root.get("comment"), commentId)
        );
    }
}
