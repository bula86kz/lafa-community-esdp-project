package application.com.common.services.interfaces;

import application.com.common.models.Language;
import application.com.interfaces.CRUDService;

public interface LanguageService extends CRUDService<Language> {
}
