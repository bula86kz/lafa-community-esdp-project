package application.com.knowledges.controllers.delayed_knowledge;

import application.com.knowledges.models.DelayedKnowledge;
import application.com.knowledges.services.interfaces.DelayedKnowledgeService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/knowledges/delayed")
public class UpdateDelayedKnowledgeController {
    private final DelayedKnowledgeService service;

    public UpdateDelayedKnowledgeController(DelayedKnowledgeService service) {
        this.service = service;
    }


    @PutMapping("/{id}")
    public void update(@PathVariable Integer id, @RequestBody DelayedKnowledge delayedKnowledge) {
        service.update(id, delayedKnowledge);
    }
}
