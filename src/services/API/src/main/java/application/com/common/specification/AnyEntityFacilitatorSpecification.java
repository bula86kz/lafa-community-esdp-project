package application.com.common.specification;

import application.com.common.models.AnyEntityFacilitator;
import application.com.common.models.EntityType;
import application.com.users.models.User;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Join;

public class AnyEntityFacilitatorSpecification {

    public static Specification<AnyEntityFacilitator> toPredicate(String type, Integer entityId){
        return Specification.where(
                (root, query, criteriaBuilder) -> {

                    Join<AnyEntityFacilitator, EntityType> entityType = root.join("entityType");
                    Join<AnyEntityFacilitator, User> facilitator = root.join("facilitator");

                    query.distinct(true);

                    return criteriaBuilder.and(
                            criteriaBuilder.like(entityType.get("type"), type),
                            criteriaBuilder.equal(root.get("entityId"), entityId));
                }
        );
    }
}
