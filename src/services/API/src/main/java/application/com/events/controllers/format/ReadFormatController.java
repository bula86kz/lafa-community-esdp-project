package application.com.events.controllers.format;

import application.com.events.models.format.Format;
import application.com.events.services.interfaces.FormatService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/formats")
public class ReadFormatController {
    private final FormatService service;


    public ReadFormatController(FormatService service) {
        this.service = service;
    }

    @GetMapping("/{id}")
    public Format read(@PathVariable int id) {
        return service.read(id);
    }
}
