package application.com.news.controllers.news;
import application.com.news.services.interfaces.NewsService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/news")
public class DeleteNewsController {
    private final NewsService service;

    public DeleteNewsController(NewsService service) {
        this.service = service;
    }


    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id) {
        service.delete(id);
    }
}
