package application.com.knowledges.controllers.knowledge;

import application.com.knowledges.services.interfaces.KnowledgeService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/knowledges")
public class DeleteKnowledgeController {
    private final KnowledgeService service;

    public DeleteKnowledgeController(KnowledgeService service) {
        this.service = service;
    }


    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id) {
        service.delete(id);
    }
}
