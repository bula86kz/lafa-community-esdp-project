package application.com.users.services.interfaces;

import application.com.users.models.User;
import application.com.interfaces.CRUDService;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface UserService extends CRUDService<User>, UserDetailsService {
    User getByEmail(String email);

    boolean existByEmail(String email);

    void enableById(Integer id);

    List<User> getUsersByEntityTypeAndEntityId(String type, Integer id);
}
