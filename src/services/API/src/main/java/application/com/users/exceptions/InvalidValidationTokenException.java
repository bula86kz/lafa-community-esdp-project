package application.com.users.exceptions;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@Getter @Setter
@ResponseStatus(HttpStatus.UNAUTHORIZED)
public class InvalidValidationTokenException extends RuntimeException {
    private String message;

    public InvalidValidationTokenException(String message) {
        this.message = message;
    }
}
