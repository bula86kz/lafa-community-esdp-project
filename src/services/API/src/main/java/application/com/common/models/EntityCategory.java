package application.com.common.models;


import application.com.interfaces.AbstractEntity;
import lombok.*;

import javax.persistence.*;

@Entity
@Builder
@Getter @Setter
@AllArgsConstructor
@Table(name = "entity_categories")
@NoArgsConstructor(force = true)
public class EntityCategory extends AbstractEntity {

    @ManyToOne
    @JoinColumn(name = "entity_id")
    private EntityType entityType;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;
}
