package application.com.common.exceptions;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.persistence.EntityNotFoundException;

@Log4j2
@Getter @Setter
@ResponseStatus(HttpStatus.CONFLICT)
public class EntityAlreadyExistsException extends RuntimeException {
    private String entity;

    private String field;
    private Object value;

    private String message;

    public EntityAlreadyExistsException(String entity, String field, Object value) {
        this.entity = entity;

        this.field = field;
        this.value = value;

        this.message = createMessage();
    }

    public EntityAlreadyExistsException(String message) {
        this.message = message;
    }

    private String createMessage() {
        log.error("ERROR: %s field %s with value %s already exists!", entity, field, value);
        return String.format("ERROR: %s field %s with value %s already exists!", entity, field, value);
    }
}
