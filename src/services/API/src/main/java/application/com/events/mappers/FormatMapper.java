package application.com.events.mappers;

import application.com.events.DTO.FormatDTO;
import application.com.events.models.format.Format;
import application.com.interfaces.AbstractMapper;
import org.springframework.stereotype.Component;

@Component
public class FormatMapper extends AbstractMapper<Format, FormatDTO> {

    public FormatMapper() {
        super(Format.class, FormatDTO.class);
    }
}
