package application.com.news.controllers.delayed_news;

import application.com.news.models.DelayedNews;
import application.com.news.services.interfaces.DelayedNewService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api/news/delayed")
public class ReadAllDelayedNewsController {
    private final DelayedNewService service;

    public ReadAllDelayedNewsController(DelayedNewService service) {
        this.service = service;
    }


    @GetMapping
    public List<DelayedNews> readAll() {
        return service.read();
    }
}
