package application.com.submissions.services;

import application.com.submissions.models.PreferredCommunication;
import application.com.submissions.repositories.PreferredCommunicationRepository;
import application.com.submissions.services.interfaces.PreferredCommunicationService;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Log4j2
@Service
public class PreferredCommunicationServiceImpl implements PreferredCommunicationService {
    private final PreferredCommunicationRepository repository;

    public PreferredCommunicationServiceImpl(PreferredCommunicationRepository repository) {
        this.repository = repository;
    }

    @Override
    public PreferredCommunication create(PreferredCommunication preferredCommunication) {
        return repository.save(preferredCommunication);
    }

    @Override
    public List<PreferredCommunication> read() {
        return repository.findAll();
    }

    @Override
    public PreferredCommunication read(Integer id) {
        Optional<PreferredCommunication> optionalPreferredCommunication = repository.findById(id);
        return getPreferredCommunicationOrThrowException(optionalPreferredCommunication);
    }

    @Override
    public PreferredCommunication update(Integer id, PreferredCommunication preferredCommunication) {
        Optional<PreferredCommunication> optionalPreferredCommunication = repository.findById(id);
        PreferredCommunication targetPreferredCommunication = getPreferredCommunicationOrThrowException(optionalPreferredCommunication);
        targetPreferredCommunication.setTitle(preferredCommunication.getTitle());
        return repository.save(targetPreferredCommunication);
    }

    @Override
    public void delete(Integer id) {
        repository.deleteById(id);
    }

    private PreferredCommunication getPreferredCommunicationOrThrowException(Optional<PreferredCommunication> preferredCommunication){
        if(preferredCommunication.isPresent()){
            return preferredCommunication.get();
        }
        log.error(new EntityNotFoundException("Can't find preferred communication with the ID"));
        throw new EntityNotFoundException("Can't find preferred communication with the ID");
    }
}
