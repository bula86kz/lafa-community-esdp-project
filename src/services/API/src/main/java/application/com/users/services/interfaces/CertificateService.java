package application.com.users.services.interfaces;

import application.com.interfaces.CRUDService;
import application.com.users.models.Certificate;

public interface CertificateService extends CRUDService<Certificate> {
}
