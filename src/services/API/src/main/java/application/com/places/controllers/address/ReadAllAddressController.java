package application.com.places.controllers.address;

import application.com.places.models.Address;
import application.com.places.services.interfaces.AddressService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/places/addresses")
public class ReadAllAddressController {
    private final AddressService service;

    public ReadAllAddressController(AddressService service) {
        this.service = service;
    }

    @GetMapping
    public List<Address> read(){
        return service.read();
    }
}
