package application.com.common.specification;

import application.com.common.models.*;
import org.springframework.data.jpa.domain.Specification;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

public class CommentSpecification {
    public static Specification<Comment> toPredicate(String type, Integer id) {
        return Specification.where(
                (root, query, builder) -> {
                    Root<EntityComment> entityCommentRoot = query.from(EntityComment.class);
                    Join<EntityComment, EntityType> entityType = entityCommentRoot.join("entityType");
                    Join<EntityComment, Comment> comment = entityCommentRoot.join("comment");
                    query.distinct(true);
                    builder.asc(comment.get("created"));
                    return builder.and(
                            builder.like(entityType.get("type"), type),
                            builder.equal(entityCommentRoot.get("entityId"), id)
                    );
                }
        );
    }
}
