package application.com.news.models;
import application.com.interfaces.AbstractEntity;
import application.com.users.models.User;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;

@Entity
@Builder
@Getter @Setter
@AllArgsConstructor
@Table(name = "news")
@NoArgsConstructor(force = true)
public class News extends AbstractEntity {
    private String title;
    private String description;
    private String image;

    private boolean isPublished;

    private LocalDateTime publishedDate;

    @ManyToOne
    @JoinColumn(name = "author_id")
    private User author;

    @Builder.Default
    @ManyToMany(fetch = FetchType.LAZY)
    private Collection<User> participants = new ArrayList<User>();
}
