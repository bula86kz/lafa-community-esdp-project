package application.com.common.services;

import application.com.common.models.EntityComment;
import application.com.common.repositories.EntityCommentRepository;
import application.com.common.services.interfaces.EntityCommentService;
import application.com.common.specification.EntityCommentSpecification;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Log4j2
@Service
public class EntityCommentServiceImpl implements EntityCommentService {
    private final EntityCommentRepository repository;

    public EntityCommentServiceImpl(EntityCommentRepository repository) {
        this.repository = repository;
    }

    @Override
    public EntityComment create(EntityComment entityComment) {
        log.info("Create entity-comment {}", entityComment);

        return repository.save(entityComment);
    }

    @Override
    public List<EntityComment> read() {
        return repository.findAll();
    }

    @Override
    public EntityComment read(Integer id) {
        return getEntityCommentOrThrowException(repository.findById(id));
    }

    @Override
    public EntityComment update(Integer id, EntityComment entityComment) {
        EntityComment targetEntityComment = getEntityCommentOrThrowException(repository.findById(id));
        targetEntityComment.setComment(entityComment.getComment());
        entityComment.setId(id);
        entityComment.setCreated(targetEntityComment.getCreated());

        log.info("Update entity-comment {}", entityComment);

        return repository.save(entityComment);
    }

    @Override
    public void delete(Integer commentId) {
        Optional<EntityComment> optionalEntityComment = repository.findOne(Specification.where(EntityCommentSpecification.toPredicate(commentId)));
        EntityComment targetEntityComment = getEntityCommentOrThrowException(optionalEntityComment);
        repository.deleteById(targetEntityComment.getId());
        log.info("Delete entity-comment {}", targetEntityComment);
    }

    private EntityComment getEntityCommentOrThrowException(Optional<EntityComment> entityComment) {
        if (entityComment.isPresent()) {
            return entityComment.get();
        }
        log.error(new EntityNotFoundException("Entity-Comment not found"));
        throw new EntityNotFoundException("Entity-Comment not found");
    }
}
