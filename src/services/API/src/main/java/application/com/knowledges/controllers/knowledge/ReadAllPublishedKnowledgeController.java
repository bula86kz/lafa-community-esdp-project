package application.com.knowledges.controllers.knowledge;

import application.com.knowledges.DTO.KnowledgeDTO;
import application.com.knowledges.mapper.KnowledgeMapper;
import application.com.knowledges.services.interfaces.KnowledgeService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api/knowledges/published")
public class ReadAllPublishedKnowledgeController {
    private final KnowledgeService service;
    private final KnowledgeMapper mapper;

    public ReadAllPublishedKnowledgeController(KnowledgeService service, KnowledgeMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }


    @GetMapping
    public List<KnowledgeDTO> readAllPublished() {
        return mapper.toDTOList(service.readAllPublished());
    }
}
