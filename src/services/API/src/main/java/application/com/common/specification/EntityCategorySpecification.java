package application.com.common.specification;

import application.com.common.models.Category;
import application.com.common.models.EntityCategory;
import application.com.common.models.EntityType;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Join;

public class EntityCategorySpecification {
    public static Specification<EntityCategory> toPredicate(String type) {
        return (root, query, criteriaBuilder) -> {
            Join<EntityType, EntityCategory> entityType = root.join("entityType");
            Join<Category, EntityCategory> category = root.join("category");
            return criteriaBuilder.like(entityType.get("type"), type);
        };
    }

}
