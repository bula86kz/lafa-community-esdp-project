package application.com.submissions.controllers.goal;

import application.com.submissions.services.interfaces.GoalService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/calculators/goals")
public class DeleteGoalController {
    private final GoalService service;

    public DeleteGoalController(GoalService service) {
        this.service = service;
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        service.delete(id);
    }
}
