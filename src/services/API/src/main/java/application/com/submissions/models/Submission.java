package application.com.submissions.models;

import application.com.common.models.Language;
import application.com.interfaces.AbstractEntity;
import application.com.places.models.City;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Builder
@Getter @Setter
@AllArgsConstructor
@Table(name = "submissions")
@NoArgsConstructor(force = true)
public class Submission extends AbstractEntity {
    private String organizationName;
    private LocalDateTime planningDate;
    private int participantCount;
    private int budget;
    private String goal;

    private String fullName;
    private String phoneNumber;
    private String email;
    private String comment;

    @ManyToOne
    @JoinColumn(name = "language_id")
    private Language language;

    @ManyToOne
    @JoinColumn(name = "city_id")
    private City city;

    @ManyToOne
    @JoinColumn(name = "preferred_communication_id")
    private PreferredCommunication preferredCommunication;
}
