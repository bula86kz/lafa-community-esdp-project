package application.com.submissions.controllers.preferred_communication;

import application.com.submissions.models.PreferredCommunication;
import application.com.submissions.services.interfaces.PreferredCommunicationService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/submissions/preferred-communications")
public class ReadPreferredCommunicationController {
    private final PreferredCommunicationService service;

    public ReadPreferredCommunicationController(PreferredCommunicationService service) {
        this.service = service;
    }

    @GetMapping("/{id}")
    public PreferredCommunication read (@PathVariable int id){
        return service.read(id);
    }
}
