package application.com.places.controllers.place;

import application.com.places.models.Place;
import application.com.places.services.interfaces.PlaceService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/places")
public class ReadPlaceByLandingTypeSlugController {
    private final PlaceService service;

    public ReadPlaceByLandingTypeSlugController(PlaceService service) {
        this.service = service;
    }

    @GetMapping("/{slug}/landing-types")
    public List<Place> readPlaceByLandingTypeSlug(@PathVariable String slug){
        return service.getPlaceByLandingSlug(slug);
    }
}
