package application.com.places.models;

import application.com.interfaces.AbstractEntity;
import lombok.*;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Builder
@Getter @Setter
@AllArgsConstructor
@Table(name = "cities")
@NoArgsConstructor(force = true)
public class City extends AbstractEntity {
    private String title;
    private String slug;
}
