package application.com.places.controllers.landing;

import application.com.places.models.LandingType;
import application.com.places.services.interfaces.LandingTypeService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
@RequestMapping("/api/places/landing-types")
public class ReadAllLandingTypeController {
    private final LandingTypeService service;

    public ReadAllLandingTypeController(LandingTypeService service) {
        this.service = service;
    }

    @GetMapping
    public List<LandingType> read() {
      return  service.read();
    }
}
