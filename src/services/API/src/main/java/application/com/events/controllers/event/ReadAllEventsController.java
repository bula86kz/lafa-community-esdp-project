package application.com.events.controllers.event;

import application.com.events.DTO.EventDTO;
import application.com.events.mappers.EventMapper;
import application.com.events.services.interfaces.EventService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/events")
public class ReadAllEventsController {
    private final EventService service;
    private final EventMapper mapper;


    public ReadAllEventsController(EventService service, EventMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @GetMapping
    public List<EventDTO> read() {
        return mapper.toDTOList(service.read());
    }
}
