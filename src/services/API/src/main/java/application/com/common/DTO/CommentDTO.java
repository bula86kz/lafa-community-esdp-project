package application.com.common.DTO;

import application.com.interfaces.AbstractDTO;
import lombok.*;

import java.time.LocalDateTime;

@Builder
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
public class CommentDTO extends AbstractDTO {
    private String comment;
    private boolean isApprove;
    private String email;
    private LocalDateTime created;
}
