package application.com.common.DTO;

import lombok.*;

@Builder
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
public class CreateCommentDTO {
    private String comment;

    private String email;

    private Integer entityId;
    private String entityType;
}
