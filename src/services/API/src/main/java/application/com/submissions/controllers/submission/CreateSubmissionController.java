package application.com.submissions.controllers.submission;

import application.com.submissions.DTO.SubmissionDTO;
import application.com.submissions.mappers.SubmissionMapper;
import application.com.submissions.models.Submission;
import application.com.submissions.services.interfaces.SubmissionService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/submissions")
public class CreateSubmissionController {
    private final SubmissionService service;
    private final SubmissionMapper mapper;

    public CreateSubmissionController(SubmissionService service, SubmissionMapper mapper){
        this.service = service;
        this.mapper = mapper;
    }

    @PostMapping
    public Submission create (@RequestBody SubmissionDTO submissionDTO){
        return service.create(mapper.toEntity(submissionDTO));
    }
}
