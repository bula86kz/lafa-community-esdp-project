package application.com.users.controllers.role;

import application.com.users.models.Role;
import application.com.users.services.RoleServiceImpl;
import application.com.users.services.interfaces.RoleService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/roles")
public class ReadRoleController {
    private final RoleService service;

    public ReadRoleController(RoleService service) {
        this.service = service;
    }

    @GetMapping("/{id}")
    public Role read(@PathVariable Integer id){
        return service.read(id);
    }
}
