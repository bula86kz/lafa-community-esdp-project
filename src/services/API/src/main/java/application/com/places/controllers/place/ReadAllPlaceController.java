package application.com.places.controllers.place;

import application.com.places.models.Place;
import application.com.places.services.interfaces.PlaceService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/places")
public class ReadAllPlaceController {
    private final PlaceService service;

    public ReadAllPlaceController(PlaceService service) {
        this.service = service;
    }

    @GetMapping
    public List<Place> read(){
        return service.read();
    }
}
