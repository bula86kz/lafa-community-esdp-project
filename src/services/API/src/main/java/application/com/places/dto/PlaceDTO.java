package application.com.places.dto;

import application.com.places.models.City;
import application.com.places.models.Description;
import lombok.*;
import java.util.List;

@Builder
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
public class PlaceDTO {
    private int id;
    private String title;
    private String photo;
    private int addressId;
    private int placeDetailsId;
    private City city;
    private List<Description> description;
}
