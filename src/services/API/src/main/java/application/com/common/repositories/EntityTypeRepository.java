package application.com.common.repositories;

import application.com.common.models.EntityComment;
import application.com.common.models.EntityType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface EntityTypeRepository extends JpaRepository<EntityType, Integer>, JpaSpecificationExecutor<EntityType> {
}
