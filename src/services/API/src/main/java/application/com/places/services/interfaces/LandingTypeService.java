package application.com.places.services.interfaces;

import application.com.interfaces.CRUDService;
import application.com.places.models.LandingType;

public interface LandingTypeService extends CRUDService<LandingType> {
}
