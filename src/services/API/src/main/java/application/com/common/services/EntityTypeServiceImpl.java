package application.com.common.services;

import application.com.common.models.EntityType;
import application.com.common.repositories.EntityTypeRepository;
import application.com.common.services.interfaces.EntityTypeService;
import application.com.common.specification.EntityTypeSpecification;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Log4j2
@Service
public class EntityTypeServiceImpl implements EntityTypeService {
    private final EntityTypeRepository repository;

    public EntityTypeServiceImpl(EntityTypeRepository repository) {
        this.repository = repository;
    }

    @Override
    public EntityType create(EntityType entityType) {
        return repository.save(entityType);
    }

    @Override
    public List<EntityType> read() {
        return repository.findAll();
    }

    @Override
    public EntityType read(Integer id) {
        Optional<EntityType> optionalEntityType = repository.findById(id);
        return getTypeOrThrowException(optionalEntityType);
    }

    @Override
    public EntityType read(String type) {
        Optional<EntityType> optionalEntityType = repository.findOne(Specification.where(EntityTypeSpecification.toPredicate(type)));
        return getTypeOrThrowException(optionalEntityType);
    }

    @Override
    public EntityType update(Integer id, EntityType entityType) {
        Optional<EntityType> optionalEntityType = repository.findById(id);
        EntityType targetType = getTypeOrThrowException(optionalEntityType);
        targetType.setType(entityType.getType());
        targetType.setUpdated(LocalDateTime.now());
        return repository.save(targetType);
    }

    @Override
    public void delete(Integer id) {
        repository.deleteById(id);
    }

    private EntityType getTypeOrThrowException(Optional<EntityType> entityType) {
        if (entityType.isPresent()) {
            return entityType.get();
        }
        log.error(new EntityNotFoundException("Тип не найден"));
        throw new EntityNotFoundException("Тип не найден");
    }
}
