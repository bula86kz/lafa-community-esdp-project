package application.com.submissions.controllers.goal;

import application.com.submissions.DTO.GoalDTO;
import application.com.submissions.mappers.GoalMapper;
import application.com.submissions.services.interfaces.GoalService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/calculators/goals")
public class UpdateGoalController {
    private final GoalService service;
    private final GoalMapper mapper;

    public UpdateGoalController(GoalService service, GoalMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @PutMapping("/{id}")
    public GoalDTO update (@PathVariable int id, @RequestBody GoalDTO goalDTO) {
        return mapper.toDTO(service.update(id, mapper.toEntity(goalDTO)));
    }
}
