package application.com.common.models;

import application.com.interfaces.AbstractEntity;
import lombok.*;

import javax.persistence.*;

@Entity
@Builder
@Getter @Setter
@AllArgsConstructor
@Table(name = "languages")
@NoArgsConstructor(force = true)
public class Language extends AbstractEntity {
    private String title;
}
