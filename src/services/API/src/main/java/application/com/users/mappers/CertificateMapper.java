package application.com.users.mappers;

import application.com.events.models.format.Format;
import application.com.events.services.interfaces.FormatService;
import application.com.interfaces.AbstractMapper;
import application.com.users.DTO.CertificateDTO;
import application.com.users.models.Certificate;
import application.com.users.models.User;
import application.com.users.services.interfaces.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Objects;

@Component
public class CertificateMapper extends AbstractMapper<Certificate, CertificateDTO> {
    private final ModelMapper mapper;
    private final UserService userService;
    private final FormatService formatService;

    public CertificateMapper(ModelMapper mapper, UserService userService, FormatService formatService) {
        super(Certificate.class, CertificateDTO.class);
        this.mapper = mapper;
        this.userService = userService;
        this.formatService = formatService;
    }


    @PostConstruct
    public void setupMapper(){
        mapper
                .createTypeMap(Certificate.class, CertificateDTO.class)
                .addMappings(mapper -> {
                    mapper.skip(CertificateDTO::setUserId);
                    mapper.skip(CertificateDTO::setFormatId);
                })
                .setPostConverter(toDTOConverter());


        mapper
                .createTypeMap(CertificateDTO.class, Certificate.class)
                .addMappings(mapper -> {
                    mapper.skip(Certificate::setUser);
                    mapper.skip(Certificate::setFormat);
                })
                .setPostConverter(toEntityConverter());
    }

    @Override
    public void mapSpecificFields(Certificate source, CertificateDTO destination){
        destination.setUserId(source.getUser().getId());
        destination.setFormatId(source.getFormat().getId());
    }

    @Override
    public void mapSpecificFields(CertificateDTO source, Certificate destination){
        Integer userId = getUserId(source);
        User user = userService.read(userId);

        destination.setUser(user);

        Integer formatId = getFormatId(source);
        Format format = formatService.read(formatId);

        destination.setFormat(format);
    }


    private Integer getUserId (CertificateDTO certificateDTO){
        return Objects.isNull(certificateDTO)
                ? null
                :certificateDTO
                .getUserId();
    }


    private Integer getFormatId (CertificateDTO certificateDTO){
        return Objects.isNull(certificateDTO)
                ? null
                :certificateDTO
                .getUserId();
    }

}
