package application.com.users.controllers.role;

import application.com.users.models.Role;
import application.com.users.services.interfaces.RoleService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/roles")
public class UpdateRoleController {
    private final RoleService service;

    public UpdateRoleController(RoleService service) {
        this.service = service;
    }

    @PutMapping("/{id}")
    public Role update(@PathVariable Integer id,
                       @RequestBody Role role){
        return service.update(id, role);
    }
}
