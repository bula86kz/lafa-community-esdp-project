package application.com.common.controllers.entity_category;

import application.com.common.models.Category;
import application.com.common.models.EntityCategory;
import application.com.common.services.interfaces.EntityCategoryService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/entity-categories")
public class ReadEntityCategoryController {
    private final EntityCategoryService service;

    public ReadEntityCategoryController(EntityCategoryService service) {
        this.service = service;
    }

    @GetMapping("/{entityType}")
    public List<Category> readEventCategoriesByType(@PathVariable String entityType){
        return service.getCategoriesByType(entityType);
    }
}
