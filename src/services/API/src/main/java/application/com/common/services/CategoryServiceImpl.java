package application.com.common.services;

import application.com.common.models.Category;
import application.com.common.models.EntityCategory;
import application.com.common.repositories.CategoryRepository;
import application.com.common.repositories.EntityCategoryRepository;
import application.com.common.services.interfaces.CategoryService;
import application.com.common.specification.EntityCategorySpecification;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Log4j2
@Service
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository repository;
    private final EntityCategoryRepository entityCategoryRepository;

    public CategoryServiceImpl(CategoryRepository repository, EntityCategoryRepository entityCategoryRepository) {
        this.repository = repository;
        this.entityCategoryRepository = entityCategoryRepository;
    }

    @Override
    public Category create(Category category) {
        return repository.save(category);
    }

    @Override
    public List<Category> read() {
        return repository.findAll();
    }

    @Override
    public Category read(Integer id) {
        Optional<Category> optionalCategory = repository.findById(id);
        return getCategoryOrThrowException(optionalCategory);
    }

    @Override
    public Category update(Integer id, Category category) {
        Optional<Category> optionalCategory = repository.findById(id);
        Category targetCategory = getCategoryOrThrowException(optionalCategory);
        targetCategory.setTitle(category.getTitle());
        targetCategory.setDescription(category.getDescription());
        targetCategory.setSlug(category.getSlug());
        targetCategory.setUpdated(LocalDateTime.now());
        return repository.save(targetCategory);
    }

    @Override
    public void delete(Integer id) {
        repository.deleteById(id);
    }

    private Category getCategoryOrThrowException(Optional<Category> category) {
        if (category.isPresent()) {
            return category.get();
        }
        log.error(new EntityNotFoundException("Категория не найдена"));
        throw new EntityNotFoundException("Категория не найдена");
    }

    @Override
    public List<Category> getFilteredCategories(String categoryName, String type) {
        return null;
    }

    @Override
    public List<Category> getFilteredCategoriesByType(String type){
        List<EntityCategory> entityCategories = entityCategoryRepository.findAll(Specification.where(EntityCategorySpecification.toPredicate(type)));
        List<Category> categories = entityCategories.stream()
                .map(entities -> entities.getCategory())
                .collect(Collectors.toList());
        return categories;
    }
}
