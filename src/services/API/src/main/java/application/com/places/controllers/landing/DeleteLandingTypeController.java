package application.com.places.controllers.landing;

import application.com.places.services.interfaces.LandingTypeService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/places/landing-types")
public class DeleteLandingTypeController {
    private final LandingTypeService service;

    public DeleteLandingTypeController(LandingTypeService service) {
        this.service = service;
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        service.delete(id);
    }
}
