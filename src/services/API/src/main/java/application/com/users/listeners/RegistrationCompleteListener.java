package application.com.users.listeners;

import application.com.users.events.RegistrationCompleteEvent;
import application.com.users.models.User;
import application.com.users.services.interfaces.MailSenderService;
import application.com.users.services.interfaces.VerificationTokenService;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class RegistrationCompleteListener implements ApplicationListener<RegistrationCompleteEvent> {
    private final MailSenderService mailSenderService;
    private final VerificationTokenService verificationTokenService;

    public RegistrationCompleteListener(MailSenderService mailSenderService, VerificationTokenService verificationTokenService) {
        this.mailSenderService = mailSenderService;
        this.verificationTokenService = verificationTokenService;
    }

    @Override
    public void onApplicationEvent(RegistrationCompleteEvent event) {
        this.confirmRegistration(event);
    }

    private void confirmRegistration(RegistrationCompleteEvent event) {
        User user = event.getUser();
        String token = UUID.randomUUID().toString();

        verificationTokenService.create(user, token);

        mailSenderService.sendEmailVerificationToken(token, user);
    }
}
