package application.com.users.services;

import application.com.users.models.User;
import application.com.users.services.interfaces.MailSenderService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class MailSenderServiceImpl implements MailSenderService
{
    @Value("${spring.mail-sender.username}")
    private String sender;
    @Value("${spring.mail-sender.confirmation-url}")
    private String confirmationURL;

    @Qualifier("mailSender")
    private final JavaMailSender mailSender;

    public MailSenderServiceImpl(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    @Override
    public void sendEmailVerificationToken(String token, User user) {
        String url = confirmationURL + "/?token=" + token;
        String message = "You registered successfully. To confirm your registration, please click on the below link.";

        mailSender.send(constructEmail("Registration Confirmation", message + "\r\n" + url, user));
    }

    @Override
    public void sendChangePasswordVerificationToken(String token, User user) {
        String url = confirmationURL + "/?token=" + token;
        String message = "To confirm your account password change, please click on the below link.";

        mailSender.send(constructEmail("Change Password Confirmation", message + "\r\n" + url, user));
    }

    @Override
    public void sendChangeEmailVerificationToken(String token, User user) {
        String url = confirmationURL + "/?token=" + token;
        String message = "To confirm your account email change, please click on the below link.";

        mailSender.send(constructEmail("Change Email Confirmation", message + "\r\n" + url, user));
    }

    @Override
    public SimpleMailMessage constructEmail(String subject, String message, User user) {
        SimpleMailMessage email = new SimpleMailMessage();

        String recipient = user.getEmail();

        email.setTo(recipient);
        email.setFrom(sender);
        email.setSubject(subject);
        email.setText(message);

        return email;
    }
}
