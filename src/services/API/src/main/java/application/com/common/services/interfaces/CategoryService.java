package application.com.common.services.interfaces;


import application.com.common.models.Category;
import application.com.interfaces.CRUDService;

import java.util.List;

public interface CategoryService extends CRUDService<Category> {
    List<Category> getFilteredCategories(String categoryName, String type);
    List<Category> getFilteredCategoriesByType(String slug);
}
