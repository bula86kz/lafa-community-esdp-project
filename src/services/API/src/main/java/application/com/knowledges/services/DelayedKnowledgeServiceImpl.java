package application.com.knowledges.services;

import application.com.common.specification.delayed_posting.DelayedPostingSpecification;
import application.com.knowledges.exceptions.KnowledgeNotFoundException;
import application.com.knowledges.models.DelayedKnowledge;
import application.com.knowledges.repositories.DelayedKnowledgeRepository;
import application.com.knowledges.services.interfaces.DelayedKnowledgeService;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;


@Service
public class DelayedKnowledgeServiceImpl implements DelayedKnowledgeService {
    private final DelayedKnowledgeRepository repository;
    private final DelayedPostingSpecification<DelayedKnowledge> specification;

    public DelayedKnowledgeServiceImpl(DelayedKnowledgeRepository repository, DelayedPostingSpecification specification) {
        this.repository = repository;
        this.specification = specification;
    }

    @Override
    public DelayedKnowledge create(DelayedKnowledge delayedKnowledge) {
        return repository.save(delayedKnowledge);
    }

    @Override
    public List<DelayedKnowledge> read() {
        return repository.findAll();
    }

    @Override
    public DelayedKnowledge read(Integer id) {
        Optional<DelayedKnowledge> optionalDelayedKnowledge = repository.findById(id);
        return getDelayedKnowledgeOrThrowException(optionalDelayedKnowledge);
    }

    @Override
    public DelayedKnowledge update(Integer id, DelayedKnowledge delayedKnowledge) {
        Optional<DelayedKnowledge> optionalDelayedKnowledge = repository.findById(id);
        DelayedKnowledge targetDelayedKnowledge = getDelayedKnowledgeOrThrowException(optionalDelayedKnowledge);

        targetDelayedKnowledge.setPostingDate(delayedKnowledge.getPostingDate());

        repository.save(targetDelayedKnowledge);

        return targetDelayedKnowledge;
    }

    @Override
    public void delete(Integer id) {
        repository.deleteById(id);
    }

    @Override
    public List<DelayedKnowledge> readAllNearest() {
        LocalDateTime currentTime = LocalDateTime.now();
        LocalDateTime currentTimePlusOneHour = LocalDateTime.now().plusHours(1);

        return repository.findAll(Specification.where(specification.delayedPostingSpecification(currentTime, currentTimePlusOneHour)));
    }


    private DelayedKnowledge getDelayedKnowledgeOrThrowException(Optional<DelayedKnowledge> delayedKnowledge) {
        if (delayedKnowledge.isPresent()) {
            return delayedKnowledge.get();
        }

        throw new KnowledgeNotFoundException();
    }
}
