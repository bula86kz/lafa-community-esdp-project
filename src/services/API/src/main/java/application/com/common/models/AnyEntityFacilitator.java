package application.com.common.models;

import application.com.interfaces.AbstractEntity;
import application.com.users.models.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
@Table(name="any_entity_facilitators")
public class AnyEntityFacilitator extends AbstractEntity {
    private Integer entityId;

    @ManyToOne
    @JoinColumn(name = "entity_type_id")
    private EntityType entityType;

    @ManyToOne
    @JoinColumn(name = "facilitator_id")
    private User facilitator;
}
