package application.com.common.controllers.language;

import application.com.common.DTO.LanguageDTO;
import application.com.common.mappers.LanguageMapper;
import application.com.common.models.Language;
import application.com.common.services.interfaces.LanguageService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/languages")
public class CreateLanguageController {
    private final LanguageService service;
    private final LanguageMapper mapper;


    public CreateLanguageController(LanguageService service, LanguageMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @PostMapping
    public Language create(@RequestBody LanguageDTO languageDTO) {
        return service.create(mapper.toEntity(languageDTO));
    }
}
