package application.com.users.services.interfaces;

import application.com.interfaces.CRUDService;
import application.com.users.models.Role;

public interface RoleService extends CRUDService<Role> {
}
