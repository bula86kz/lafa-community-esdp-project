package application.com.news.mapper;

import application.com.interfaces.AbstractMapper;
import application.com.news.DTO.NewsDTO;
import application.com.news.models.News;
import application.com.users.models.User;
import application.com.users.services.interfaces.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Objects;

@Component
public class NewsMapper extends AbstractMapper<News, NewsDTO> {
    private final ModelMapper mapper;
    private final UserService userService;

    public NewsMapper(ModelMapper mapper, UserService userService) {
        super(News.class, NewsDTO.class);
        this.mapper = mapper;
        this.userService = userService;
    }


    @PostConstruct
    public void setupMapper(){
        mapper
                .createTypeMap(News.class, NewsDTO.class)
                .addMappings(mapper -> mapper.skip(NewsDTO::setAuthorId))
                .setPostConverter(toDTOConverter());
        mapper
                .createTypeMap(NewsDTO.class, News.class)
                .addMappings(mapper -> mapper.skip(News::setAuthor))
                .setPostConverter(toEntityConverter());
    }

    @Override
    public void mapSpecificFields(News source, NewsDTO destination){
        destination.setAuthorId(source.getAuthor().getId());
        destination.setFirstName(source.getAuthor().getFirstName());
        destination.setLastName(source.getAuthor().getLastName());
    }

    @Override
    public void mapSpecificFields(NewsDTO source, News destination){
        Integer authorId = getAuthorId(source);
        User author = userService.read(authorId);

        destination.setAuthor(author);
    }


    private Integer getAuthorId (NewsDTO newsDTO){
        return Objects.isNull(newsDTO)
                ? null
                :newsDTO
                .getAuthorId();
    }

}
