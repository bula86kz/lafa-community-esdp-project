package application.com.places.controllers.place;

import application.com.places.models.Place;
import application.com.places.services.interfaces.PlaceService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/places")
public class CreatePlaceController {
    private final PlaceService service;

    public CreatePlaceController(PlaceService service) {
        this.service = service;
    }

    @PostMapping
    public Place create(@RequestBody Place place){
        return service.create(place);
    }
}
