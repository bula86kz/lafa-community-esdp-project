package application.com.places.models;

import lombok.*;
import java.util.List;

@Getter @Setter
public class Description {
    private String title;
    private List<String> description;
    private int placeId;
}
