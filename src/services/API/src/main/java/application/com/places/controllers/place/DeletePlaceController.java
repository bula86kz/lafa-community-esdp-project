package application.com.places.controllers.place;

import application.com.places.services.interfaces.PlaceService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/places")
public class DeletePlaceController {
    private final PlaceService service;

    public DeletePlaceController(PlaceService service) {
        this.service = service;
    }

    @DeleteMapping("/{id}")
    public void delete (@PathVariable int id){
        service.delete(id);
    }
}