package application.com.places.repositories;

import application.com.places.models.LandingType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface LandingTypeRepository extends JpaRepository<LandingType, Integer>, JpaSpecificationExecutor<LandingType> {
}
