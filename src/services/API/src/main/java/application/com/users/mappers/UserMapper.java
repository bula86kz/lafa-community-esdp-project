package application.com.users.mappers;

import application.com.interfaces.AbstractMapper;
import application.com.users.DTO.UserDTO;
import application.com.users.models.User;
import org.springframework.stereotype.Component;

@Component
public class UserMapper extends AbstractMapper<User, UserDTO> {
    public UserMapper() {
        super(User.class, UserDTO.class);
    }
}
