package application.com.places.controllers.city;

import application.com.places.models.City;
import application.com.places.services.interfaces.CityService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/places/cities")
public class ReadCityController {
    private final CityService service;

    public ReadCityController(CityService service) {
        this.service = service;
    }

    @GetMapping("/{id}")
    public City read(@PathVariable int id){
        return service.read(id);
    }
}
