package application.com.common.exceptions.handlers;

import application.com.common.exceptions.EntityAlreadyExistsException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

@ControllerAdvice(annotations = RestController.class)
public class EntityAlreadyExistsExceptionHandler {

    @ExceptionHandler(EntityAlreadyExistsException.class)
    private ResponseEntity<?> handle(EntityAlreadyExistsException exception) {
        return ResponseEntity
                            .unprocessableEntity()
                            .body(exception.getMessage());
    }

}
