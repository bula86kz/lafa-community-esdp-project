package application.com.users.services;

import application.com.users.exceptions.RoleNotFoundException;
import application.com.users.models.Role;
import application.com.users.repositories.RoleRepository;
import application.com.users.services.interfaces.RoleService;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class RoleServiceImpl implements RoleService {
    private final RoleRepository repository;

    public RoleServiceImpl(RoleRepository repository) {
        this.repository = repository;
    }

    @Override
    public Role create(Role role) {
        return repository.save(role);
    }

    @Override
    public List<Role> read() {
        return repository.findAll();
    }

    @Override
    public Role read(Integer id) {
        Optional<Role> optionalRole = repository.findById(id);
        return getRoleOrThrowException(optionalRole);
    }

    @Override
    public Role update(Integer id, Role role) {
        Optional<Role> optionalRole = repository.findById(id);
        Role targetRole = getRoleOrThrowException(optionalRole);

        targetRole.setTitle(role.getTitle());
        targetRole.setDescription(role.getDescription());
        targetRole.setSlug(role.getSlug());

        repository.save(targetRole);

        return targetRole;
    }

    @Override
    public void delete(Integer id) {
        repository.deleteById(id);
    }

    private Role getRoleOrThrowException(Optional<Role> role) {
        if (role.isPresent()) {
            return role.get();
        }

        throw new RoleNotFoundException();
    }
}
