package application.com.common.models;

import application.com.interfaces.AbstractEntity;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Builder
@Getter @Setter
@AllArgsConstructor
@Table(name = "entity_types")
@NoArgsConstructor(force = true)
public class EntityType extends AbstractEntity {
    private String type;
}
