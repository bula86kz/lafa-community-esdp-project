package application.com.knowledges.controllers.knowledge;

import application.com.knowledges.DTO.KnowledgeDTO;
import application.com.knowledges.mapper.KnowledgeMapper;
import application.com.knowledges.models.Knowledge;
import application.com.knowledges.services.interfaces.KnowledgeService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/knowledges")
public class UpdateKnowledgeController {
    private final KnowledgeService service;
    private final KnowledgeMapper mapper;

    public UpdateKnowledgeController(KnowledgeService service, KnowledgeMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }


    @PutMapping("/{id}")
    public void update(@PathVariable Integer id, @RequestBody KnowledgeDTO dto) {
        service.update(id, mapper.toEntity(dto));
    }
}
