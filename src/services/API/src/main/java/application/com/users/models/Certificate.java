package application.com.users.models;

import application.com.events.models.format.Format;
import application.com.interfaces.AbstractEntity;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Builder
@Getter @Setter
@AllArgsConstructor
@Table(name = "certificates")
@NoArgsConstructor(force = true)
public class Certificate extends AbstractEntity {
    private String image;
    private String title;
    private String description;

    private LocalDateTime startDate;
    private LocalDateTime finishDate;

    private String competence;
    private String addressOfCourse;
    private String organizerFullname;
    private String organizerPhoneNumber;

    @ManyToOne
    @JoinColumn(name = "format_id")
    private Format format;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
}
