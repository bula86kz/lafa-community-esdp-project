package application.com.knowledges.services.interfaces;

import application.com.interfaces.CRUDService;
import application.com.knowledges.models.Knowledge;

import java.util.List;

public interface KnowledgeService extends CRUDService<Knowledge> {
    List<Knowledge> readAllNearest();
    List<Knowledge> readAllPublished();
    List<Knowledge> readAllByCategory(Integer id);
    List<Knowledge> readAllByAuthor(Integer id);
}
