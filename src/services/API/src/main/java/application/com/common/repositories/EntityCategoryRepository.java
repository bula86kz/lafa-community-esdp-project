package application.com.common.repositories;

import application.com.common.models.EntityCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface EntityCategoryRepository extends JpaRepository<EntityCategory, Integer>, JpaSpecificationExecutor<EntityCategory> {
}
