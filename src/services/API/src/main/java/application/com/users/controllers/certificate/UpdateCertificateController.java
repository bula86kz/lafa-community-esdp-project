package application.com.users.controllers.certificate;

import application.com.users.DTO.CertificateDTO;
import application.com.users.mappers.CertificateMapper;
import application.com.users.services.interfaces.CertificateService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/certificates")
public class UpdateCertificateController {
    private final CertificateService service;
    private final CertificateMapper mapper;

    public UpdateCertificateController(CertificateService service, CertificateMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }


    @PutMapping("/{id}")
    public CertificateDTO update(@PathVariable Integer id, @RequestBody CertificateDTO dto) {
        return mapper.toDTO(service.update(id, mapper.toEntity(dto)));
    }
}
