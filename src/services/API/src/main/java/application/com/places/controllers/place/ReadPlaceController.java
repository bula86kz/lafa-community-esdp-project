package application.com.places.controllers.place;

import application.com.places.models.Place;
import application.com.places.services.interfaces.PlaceService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/places")
public class ReadPlaceController {
    private final PlaceService service;

    public ReadPlaceController(PlaceService service) {
        this.service = service;
    }

    @GetMapping("/{id}")
    public Place read(@PathVariable int id){
        return service.read(id);
    }
}
