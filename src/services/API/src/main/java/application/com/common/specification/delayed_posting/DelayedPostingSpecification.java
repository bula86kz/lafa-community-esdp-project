package application.com.common.specification.delayed_posting;
import lombok.AllArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import java.time.LocalDateTime;

@Service
@AllArgsConstructor
public final class DelayedPostingSpecification<T> {

    public Specification<T> delayedPostingSpecification(LocalDateTime currentTime, LocalDateTime currentTimePlusOneHour) {

        return (root, query, cb) -> cb.between(root.get("publishedDate").as(LocalDateTime.class), currentTime, currentTimePlusOneHour);
    }

    public Specification<T> published(){
        return (root, query, cb) -> cb.isTrue(root.get("isPublished").as(Boolean.class));
    }

}