package application.com.common.controllers.category;

import application.com.common.models.Category;
import application.com.common.services.interfaces.CategoryService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/categories")
public class ReadCategoryController {
    private final CategoryService service;

    public ReadCategoryController(CategoryService service) {
        this.service = service;
    }

    @GetMapping("/{entityType}")
    public List<Category> readEventCategoriesByType(@PathVariable String entityType){
        return service.getFilteredCategoriesByType(entityType);
    }
}
