package application.com.places.controllers.address;

import application.com.places.models.Address;
import application.com.places.services.interfaces.AddressService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/places/addresses")
public class UpdateAddressController {
    private final AddressService service;

    public UpdateAddressController(AddressService service) {
        this.service = service;
    }

    @PutMapping("/{id}")
    public Address update(@PathVariable int id, @RequestBody Address address){
        return service.update(id,address);
    }
}
