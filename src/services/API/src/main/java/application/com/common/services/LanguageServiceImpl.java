package application.com.common.services;

import application.com.common.models.Language;
import application.com.common.repositories.LanguageRepository;
import application.com.common.services.interfaces.LanguageService;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Log4j2
@Service
public class LanguageServiceImpl implements LanguageService {
    private final LanguageRepository repository;

    public LanguageServiceImpl(LanguageRepository repository) {
        this.repository = repository;
    }


    @Override
    public Language create(Language language) {
        return repository.save(language);
    }

    @Override
    public List<Language> read() {
        return repository.findAll();
    }

    @Override
    public Language read(Integer id) {
        Optional<Language> optionalLanguage = repository.findById(id);
        return getLanguageOrThrowException(optionalLanguage);
    }


    @Override
    public Language update(Integer id, Language language) {
        Optional<Language> optionalLanguage = repository.findById(id);
        Language targetLanguage = getLanguageOrThrowException(optionalLanguage);

        language.setId(id);
        language.setCreated(targetLanguage.getCreated());
        return repository.save(language);
    }

    @Override
    public void delete(Integer id) {
        repository.deleteById(id);
    }

    private Language getLanguageOrThrowException(Optional<Language> language) {
        if (language.isPresent()) {
            return language.get();
        }
        log.error(new EntityNotFoundException("Can't find language with the ID"));
        throw new EntityNotFoundException("Can't find language with the ID");
    }
}
