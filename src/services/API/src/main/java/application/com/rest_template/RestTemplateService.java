package application.com.rest_template;

import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class RestTemplateService {
    private final RestTemplate restTemplate;
    HttpHeaders headers = new HttpHeaders();

    public RestTemplateService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public String restTemplateGet(String url, HttpMethod httpMethod) {
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        HttpEntity<?> entity = new HttpEntity<>(headers);
        HttpEntity<String> response = restTemplate.exchange(url, httpMethod, entity, String.class);
        return response.toString();
    }

    public String restTemplatePost(String url,  Object entity) {
        HttpEntity<?> request = new HttpEntity<>(entity, headers);
        ResponseEntity<String> result = restTemplate.postForEntity(url, request, String.class);
        return  result.toString();
    }

    public void restTemplateUpdate(String url, Object entity) {
        HttpEntity<?> request = new HttpEntity<>(entity, headers);
        restTemplate.put(url, request);
    }
}
