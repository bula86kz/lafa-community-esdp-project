CREATE TABLE delayed_knowledges
(
    "id"          	    SERIAL       NOT NULL,
    "created"     	    TIMESTAMP    NOT NULL,
    "updated"     	    TIMESTAMP    DEFAULT NULL,
    "posting_date"     	TIMESTAMP    NOT NULL,
    "knowledge_id"      INT          NOT NULL,

    CONSTRAINT delayed_knowledges_pkey
        PRIMARY KEY(id),

    CONSTRAINT fk_delayed_knowledges_knowledge
        FOREIGN KEY (knowledge_id)
            REFERENCES knowledges (id)
            ON DELETE RESTRICT
            ON UPDATE CASCADE
);