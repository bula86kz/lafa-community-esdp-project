CREATE TABLE certificates
(
    "id"          	            SERIAL       NOT NULL,
    "created"     	            TIMESTAMP    NOT NULL,
    "updated"     	            TIMESTAMP    DEFAULT NULL,
    "image"                     VARCHAR      DEFAULT NULL,
    "title"                     VARCHAR      NOT NULL,
    "description"               VARCHAR      DEFAULT NULL,
    "start_date"                TIMESTAMP    NOT NULL,
    "finish_date"               TIMESTAMP    NOT NULL,
    "competence"                VARCHAR      DEFAULT NULL,
    "address_of_course"         VARCHAR      NOT NULL,
    "organizer_fullname"        VARCHAR      NOT NULL,
    "organizer_phone_number"    VARCHAR      DEFAULT NULL,
    "format_id"   	            INT          NOT NULL,
    "user_id"   	            INT          NOT NULL,

    CONSTRAINT certificates_pkey
        PRIMARY KEY(id),

    CONSTRAINT fk_certificates_format
        FOREIGN KEY (format_id)
            REFERENCES formats (id)
            ON DELETE RESTRICT
            ON UPDATE CASCADE,

    CONSTRAINT fk_certificates_user
        FOREIGN KEY (user_id)
            REFERENCES users (id)
            ON DELETE RESTRICT
            ON UPDATE CASCADE
);
