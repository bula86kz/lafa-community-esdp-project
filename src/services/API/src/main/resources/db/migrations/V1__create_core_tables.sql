--Модуль атворизации и аутентификации, Users и Roles.

CREATE TABLE roles
(
    "id"          SERIAL        NOT NULL,
    "created"     TIMESTAMP     NOT NULL,
    "updated"     TIMESTAMP     DEFAULT NULL,
    "title"       VARCHAR(10)   NOT NULL,
    "description" VARCHAR(255)  DEFAULT NULL,
    "slug"        VARCHAR(50)   NOT NULL,

    CONSTRAINT roles_pkey
        PRIMARY KEY (id),

    CONSTRAINT roles_title_unique
        UNIQUE (title),

    CONSTRAINT roles_slug_unique
        UNIQUE (slug)
);

CREATE TABLE users
(
    "id"           SERIAL       NOT NULL,
    "created"      TIMESTAMP    NOT NULL,
    "updated"      TIMESTAMP    DEFAULT NULL,
    "first_name"   VARCHAR(50)  NOT NULL,
    "last_name"    VARCHAR(50)  NOT NULL,
    "phone_number" VARCHAR(12)  NOT NULL,
    "email"        VARCHAR(50)  NOT NULL,
    "password"     VARCHAR      NOT NULL,
    "gender"       VARCHAR(50)  DEFAULT NULL,
    "photo"        VARCHAR      DEFAULT NULL,
    "is_active"    BOOLEAN      DEFAULT FALSE,
    "role_id"      INT          DEFAULT NULL,
    "about_me"     VARCHAR      DEFAULT NULL,

    CONSTRAINT users_pkey
        PRIMARY KEY(id),

    CONSTRAINT users_phone_number_unique
        UNIQUE (phone_number),

    CONSTRAINT users_email_unique
        UNIQUE (email),

    CONSTRAINT fk_users_role
        FOREIGN KEY (role_id)
            REFERENCES roles (id)
            ON DELETE RESTRICT
            ON UPDATE CASCADE
);

CREATE TABLE verification_tokens
(
    "id"          SERIAL    NOT NULL,
    "created"   TIMESTAMP NOT NULL,
    "updated"   TIMESTAMP DEFAULT NULL,
    "user_id"     INT       NOT NULL,
    "token"       varchar   NOT NULL,
    "expiry_date" TIMESTAMP NOT NULL,

    CONSTRAINT verification_tokens_pkey
        PRIMARY KEY(id),

    CONSTRAINT token_unique
        UNIQUE (token),

    CONSTRAINT fk_users_verification_tokens
        FOREIGN KEY (user_id)
            REFERENCES users (id)
            ON DELETE RESTRICT
            ON UPDATE CASCADE
);


CREATE TABLE users_roles
(
    "id"      SERIAL NOT NULL,
    "user_id" INT    NOT NULL,
    "roles_id" INT    NOT NULL,

    CONSTRAINT users_roles_pk
        PRIMARY KEY (id),

    CONSTRAINT user_id_role_id_unique
        UNIQUE (user_id, roles_id),

    CONSTRAINT fk_m2m_users_roles_user
        FOREIGN KEY (user_id)
            REFERENCES users (id)
            ON DELETE RESTRICT
            ON UPDATE CASCADE,

    CONSTRAINT fk_m2m_users_roles_role
        FOREIGN KEY (roles_id)
            REFERENCES roles (id)
            ON DELETE RESTRICT
            ON UPDATE CASCADE
);

CREATE INDEX index_role_slug ON roles(slug);


--Словари Formats и Languages.

CREATE TABLE formats
(
    "id"      SERIAL      NOT NULL,
    "created" TIMESTAMP   NOT NULL,
    "updated" TIMESTAMP   DEFAULT NULL,
    "title"   VARCHAR(10) NOT NULL,

    CONSTRAINT formats_pkey
        PRIMARY KEY (id),

    CONSTRAINT formats_title_unique
        UNIQUE (title)
);

CREATE TABLE languages
(
    "id"      SERIAL      NOT NULL,
    "created" TIMESTAMP   NOT NULL,
    "updated" TIMESTAMP   DEFAULT NULL,
    "title"   VARCHAR(50) NOT NULL,

    CONSTRAINT languages_pkey
        PRIMARY KEY (id),

    CONSTRAINT languages_title_unique
        UNIQUE (title)
);


--Модуль категоризации, Categories, EntityTypes, EntityCategories.

CREATE TABLE categories
(
    "id"          SERIAL       NOT NULL,
    "created"     TIMESTAMP    NOT NULL,
    "updated"     TIMESTAMP    DEFAULT NULL,
    "title"       VARCHAR      NOT NULL,
    "description" VARCHAR      DEFAULT NULL,
    "slug"        VARCHAR(128) NOT NULL,

    CONSTRAINT categories_pkey
        PRIMARY KEY(id),

    CONSTRAINT categories_title_unique
        UNIQUE (title),

    CONSTRAINT categories_slug_unique
        UNIQUE (slug)
);

CREATE TABLE entity_types
(
    "id"       SERIAL       NOT NULL,
    "created"  TIMESTAMP    NOT NULL,
    "updated"  TIMESTAMP    DEFAULT NULL,
    "type"     VARCHAR(100) NOT NULL,

    CONSTRAINT entity_types_pkey
        PRIMARY KEY (id),

    CONSTRAINT entity_types_type_unique
        UNIQUE (type)
);

CREATE TABLE entity_categories
(
    "id"             SERIAL    NOT NULL,
    "created"        TIMESTAMP NOT NULL,
    "updated"        TIMESTAMP DEFAULT NULL,
    "category_id"    INT       NOT NULL,
    "entity_id" INT       NOT NULL,

    CONSTRAINT entity_types_categories_pkey
        PRIMARY KEY(id),

    CONSTRAINT category_id_entity_id_unique
        UNIQUE (category_id, entity_id),

    CONSTRAINT fk_m2m_entity_types_categories_category
        FOREIGN KEY (category_id)
            REFERENCES categories (id)
            ON DELETE RESTRICT
            ON UPDATE CASCADE,

    CONSTRAINT fk_m2m_entity_types_categories_entity_types
        FOREIGN KEY (entity_id)
            REFERENCES entity_types (id)
            ON DELETE RESTRICT
            ON UPDATE CASCADE
);


--Модуль новостей и базы знаний, News и Knowledges.

CREATE TABLE news
(
    "id"                SERIAL       NOT NULL,
    "created"           TIMESTAMP    NOT NULL,
    "updated"           TIMESTAMP    DEFAULT NULL,
    "title"             VARCHAR(128) NOT NULL,
    "description"       VARCHAR      NOT NULL,
    "image"             VARCHAR      NOT NULL,
    "is_published"      BOOLEAN      NOT NULL DEFAULT FALSE,
    "published_date"    TIMESTAMP    DEFAULT NULL,
    "author_id"         INT          NOT NULL,

    CONSTRAINT news_pkey
        PRIMARY KEY(id),

    CONSTRAINT fk_news_user
        FOREIGN KEY (author_id)
            REFERENCES users (id)
            ON DELETE RESTRICT
            ON UPDATE CASCADE
);


CREATE TABLE news_participants
(
    id      SERIAL NOT NULL,
    news_id INT    NOT NULL,
    participants_id INT    NOT NULL,

    CONSTRAINT news_participants_pk
        PRIMARY KEY (id),

    CONSTRAINT fk_m2m_news_participants_news
        FOREIGN KEY (news_id)
            REFERENCES news (id)
            ON DELETE RESTRICT
            ON UPDATE CASCADE,

    CONSTRAINT fk_m2m_participants_news_participant
        FOREIGN KEY (participants_id)
            REFERENCES users (id)
            ON DELETE RESTRICT
            ON UPDATE CASCADE
);


CREATE TABLE knowledges
(
    "id"                SERIAL       NOT NULL,
    "created"           TIMESTAMP    NOT NULL,
    "updated"           TIMESTAMP    DEFAULT NULL,
    "title"             VARCHAR(128) NOT NULL,
    "description"       VARCHAR      NOT NULL,
    "image"             VARCHAR      NOT NULL,
    "is_published"      BOOLEAN      NOT NULL DEFAULT FALSE,
    "is_private"        BOOLEAN      NOT NULL DEFAULT FALSE,
    "published_date"    TIMESTAMP    DEFAULT NULL,
    "author_id"         INT          NOT NULL,
    "category_id"       INT          NOT NULL,

    CONSTRAINT knowledges_pkey
        PRIMARY KEY(id),

    CONSTRAINT fk_knowledges_user
        FOREIGN KEY (author_id)
            REFERENCES users (id)
            ON DELETE RESTRICT
            ON UPDATE CASCADE,

    CONSTRAINT fk_knowledges_category
        FOREIGN KEY (category_id)
            REFERENCES categories (id)
            ON DELETE RESTRICT
            ON UPDATE CASCADE
);


-- Словарь Cities

CREATE TABLE cities
(
    "id"      SERIAL      NOT NULL,
    "created" TIMESTAMP   NOT NULL,
    "updated" TIMESTAMP   DEFAULT NULL,
    "title"   VARCHAR(50) NOT NULL,
    "slug"    VARCHAR(50) NOT NULL,

    CONSTRAINT cities_pkey
        PRIMARY KEY(id),

    CONSTRAINT cities_title_unique
        UNIQUE (title),

    CONSTRAINT cities_slug_unique
        UNIQUE (slug)
);



--Модуль мест, Addresses и Places.

CREATE TABLE addresses
(
    "id"                 SERIAL       NOT NULL,
    "created"            TIMESTAMP    NOT NULL,
    "updated"            TIMESTAMP    DEFAULT NULL,
    "street"             VARCHAR      NOT NULL,
    "apartament_number"  VARCHAR(10),
    "building_number"    VARCHAR(10),
    "city_id"            INT          NOT NULL,

    CONSTRAINT addresses_pkey
        PRIMARY KEY(id),

    CONSTRAINT fk_address_city
        FOREIGN KEY (city_id)
            REFERENCES cities (id)
            ON DELETE RESTRICT
            ON UPDATE CASCADE
);


CREATE TABLE landing_types
(
    "id"          SERIAL      NOT NULL,
    "created"     TIMESTAMP   NOT NULL,
    "updated"     TIMESTAMP   DEFAULT NULL,
    "title"       VARCHAR(50) NOT NULL,
    "icon"        VARCHAR     NOT NULL,
    "slug"        VARCHAR(50) NOT NULL,

    CONSTRAINT landing_types_pkey
            PRIMARY KEY(id),

    CONSTRAINT landing_title_unique
        UNIQUE (title)
);



CREATE TABLE places
(
    "id"               SERIAL      NOT NULL,
    "created"          TIMESTAMP   NOT NULL,
    "updated"          TIMESTAMP   DEFAULT NULL,
    "title"            VARCHAR(50) NOT NULL,
    "photo"            VARCHAR     NOT NULL,
    "address_id"       INT         NOT NULL,
    "landing_type_id"  INT         NOT NULL,

    CONSTRAINT places_pkey
        PRIMARY KEY(id),

    CONSTRAINT fk_places_address
        FOREIGN KEY (address_id)
            REFERENCES addresses (id)
            ON DELETE RESTRICT
            ON UPDATE CASCADE,

    CONSTRAINT fk_places_landing_types
        FOREIGN KEY (landing_type_id)
            REFERENCES landing_types (id)
            ON DELETE RESTRICT
            ON UPDATE CASCADE
);


--Модуль мерориятий, Events.

CREATE TABLE events
(
    "id"          SERIAL    NOT NULL,
    "created"     TIMESTAMP NOT NULL,
    "updated"     TIMESTAMP DEFAULT NULL,
    "title"       VARCHAR   NOT NULL,
    "description" VARCHAR   NOT NULL,
    "image"       VARCHAR   NOT NULL,
    "start_date"  TIMESTAMP DEFAULT NULL,
    "finish_date" TIMESTAMP DEFAULT NULL,
    "price"       INT       DEFAULT 0,

    "category_id" INT       NOT NULL,
    "format_id"   INT       NOT NULL,
    "language_id" INT       NOT NULL,
    "city_id"     INT       NOT NULL,

    CONSTRAINT events_pkey
        PRIMARY KEY(id),

    CONSTRAINT fk_events_category
        FOREIGN KEY (category_id)
            REFERENCES categories (id)
            ON DELETE RESTRICT
            ON UPDATE CASCADE,

    CONSTRAINT fk_events_format
        FOREIGN KEY (format_id)
            REFERENCES formats (id)
            ON DELETE RESTRICT
            ON UPDATE CASCADE,

    CONSTRAINT fk_events_language
        FOREIGN KEY (language_id)
            REFERENCES languages (id)
            ON DELETE RESTRICT
            ON UPDATE CASCADE,

    CONSTRAINT fk_events_city
        FOREIGN KEY (city_id)
            REFERENCES cities (id)
            ON DELETE RESTRICT
            ON UPDATE CASCADE
);

CREATE TABLE events_speakers
(
    "id"         SERIAL NOT NULL,
    "event_id"   INT    NOT NULL,
    "speaker_id" INT    NOT NULL,

    CONSTRAINT events_speakers_pkey
        PRIMARY KEY(id),

    CONSTRAINT event_id_speaker_id_unique
        UNIQUE (event_id, speaker_id),

    CONSTRAINT fk_m2m_events_speakers_event
        FOREIGN KEY (event_id)
            REFERENCES events (id)
            ON DELETE RESTRICT
            ON UPDATE CASCADE,

    CONSTRAINT fk_m2m_events_speakers_speaker
        FOREIGN KEY (speaker_id)
            REFERENCES users (id)
            ON DELETE RESTRICT
            ON UPDATE CASCADE
);

--Модуль заявки на фасилитатора, Submissions.

CREATE TABLE preferred_communications
(
    "id"        SERIAL NOT NULL,
    "created"   TIMESTAMP NOT NULL,
    "updated"   TIMESTAMP DEFAULT NULL,
    "title"     VARCHAR(10) NOT NULL,

    CONSTRAINT preferred_communications_pkey
        PRIMARY KEY("id")
);

CREATE TABLE submissions
(
    "id"                        SERIAL       NOT NULL,
    "created"                   TIMESTAMP    NOT NULL,
    "updated"                   TIMESTAMP    DEFAULT NULL,
    "organization_name"         VARCHAR(128) NOT NULL,
    "planning_date"             TIMESTAMP,
    "participant_count"         INT          NOT NULL,
    "budget"                    INT          DEFAULT 0,
    "goal"                      VARCHAR      NOT NULL,

    "full_name"                 VARCHAR(128) NOT NULL,
    "phone_number"              VARCHAR(12)  NOT NULL,
    "email"                     VARCHAR(256) NOT NULL,
    "comment"                   VARCHAR      NOT NULL,

    "city_id"                       INT          NOT NULL,
    "language_id"                   INT          NOT NULL,
    "preferred_communication_id"    INT NOT NULL,

    CONSTRAINT submissions_pkey
        PRIMARY KEY(id),

    CONSTRAINT fk_submission_city
        FOREIGN KEY(city_id)
            REFERENCES cities (id),

    CONSTRAINT fk_submission_preferred_communication
        FOREIGN KEY(preferred_communication_id)
            REFERENCES preferred_communications (id),

    CONSTRAINT fk_submission_language
        FOREIGN KEY (language_id)
            REFERENCES languages (id)
            ON DELETE RESTRICT
            ON UPDATE CASCADE
);
