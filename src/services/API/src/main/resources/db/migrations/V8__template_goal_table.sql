INSERT INTO goals (created, updated, title)
VALUES ('2022-03-11T19:10:25-07', '2022-03-11T20:10:25-07', 'Построение консенсуса'),
       ('2022-03-11T19:10:25-07', '2022-03-11T20:10:25-07', 'Планирование действий'),
       ('2022-03-11T19:10:25-07', '2022-03-11T20:10:25-07', 'Ретроспектива прошлого периода'),
       ('2022-03-11T19:10:25-07', '2022-03-11T20:10:25-07', 'Консенсус по одному вопросу'),
       ('2022-03-11T19:10:25-07', '2022-03-11T20:10:25-07', 'Стратегическая сессия'),
       ('2022-03-11T19:10:25-07', '2022-03-11T20:10:25-07', 'Другое');