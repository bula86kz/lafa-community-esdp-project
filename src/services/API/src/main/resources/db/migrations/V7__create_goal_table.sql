--Модуль Goal для Калькулятора

CREATE TABLE goals
(
    "id"                                SERIAL NOT NULL,
    "created"                           TIMESTAMP NOT NULL,
    "updated"                           TIMESTAMP DEFAULT NULL,
    "title"                             VARCHAR(60) NOT NULL,

    CONSTRAINT goals_pkey
        PRIMARY KEY("id")
);