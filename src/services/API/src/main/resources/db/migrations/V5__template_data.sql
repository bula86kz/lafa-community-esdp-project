INSERT INTO categories (created, updated, title, description, slug)
VALUES ('2022-03-11T19:10:25-07', '2022-03-11T20:10:25-07', 'Мастер классы','Мастер классы для фасилитаторов', 'masterclasses'),
       ('2022-03-10T16:10:25-07', '2022-03-11T19:10:25-07', 'Статьи','Обучающие статьи', 'articles'),
       ('2022-03-10T17:10:25-07', '2022-03-11T18:10:25-07', 'Вебинары','Вебинары для фасилитаторов', 'webinars'),
       ('2022-03-10T10:10:25-07', '2022-03-11T11:10:25-07', 'Видео материалы','Видео материалы для обучения', 'videos'),
       ('2022-03-10T10:10:25-07', '2022-03-11T11:10:25-07', 'Категория Базы знаний №1','Категория Базы знаний', 'knowledges-category-1'),
       ('2022-03-10T10:10:25-07', '2022-03-11T11:10:25-07', 'Категория Базы знаний №2','Категория Базы знаний', 'knowledges-category-2'),
       ('2022-03-10T10:10:25-07', '2022-03-11T11:10:25-07', 'Категория Базы знаний №3','Категория Базы знаний', 'knowledges-category-3'),
       ('2022-03-10T10:10:25-07', '2022-03-11T11:10:25-07', 'Категория Базы знаний №4','Категория Базы знаний', 'knowledges-category-4');

INSERT INTO entity_types (created, updated, type)
VALUES ('2022-03-10T20:10:25-07', '2022-03-11T20:10:25-07', 'events'),
       ('2022-03-10T20:10:25-07', '2022-03-11T20:10:25-07', 'knowledges');


INSERT INTO entity_categories (created, updated, category_id, entity_id)
VALUES ('2022-03-11T10:10:25-07', '2022-03-11T18:10:25-07', 1, 1),
       ('2022-03-11T10:10:25-07', '2022-03-11T18:10:25-07', 2, 1),
       ('2022-03-11T10:10:25-07', '2022-03-11T18:10:25-07', 3, 1),
       ('2022-03-11T10:10:25-07', '2022-03-11T18:10:25-07', 4, 1),
       ('2022-03-11T10:10:25-07', '2022-03-11T18:10:25-07', 5, 2),
       ('2022-03-11T10:10:25-07', '2022-03-11T18:10:25-07', 6, 2),
       ('2022-03-11T10:10:25-07', '2022-03-11T18:10:25-07', 7, 2),
       ('2022-03-11T10:10:25-07', '2022-03-11T18:10:25-07', 8, 2);



INSERT INTO users (created, updated, first_name, last_name, phone_number, email, password, gender, photo, about_me)
VALUES ('2022-03-11T19:10:25-07', '2022-03-11T20:10:25-07', 'Анна','Пацюк', '+77077777777', 'anna@gmail.com', '12345', 'женский', '/assets/images/avatar_facilitator_1.jpg', 'Далеко-далеко за словесными горами в стране гласных и согласных живут рыбные тексты. Вопроса своих решила текстами напоивший журчит заманивший продолжил языкового парадигматическая?'),
       ('2022-03-10T16:10:25-07', '2022-03-11T19:10:25-07', 'Ольга','Чаадаева', '+77077777778', 'olga@gmail.com', '12345', 'женский', '/assets/images/avatar_facilitator_1.jpg', 'Далеко-далеко за, словесными горами в стране гласных и согласных живут рыбные тексты. Залетают рекламных назад все текст несколько, пунктуация напоивший продолжил одна!'),
       ('2022-03-10T17:10:25-07', '2022-03-11T18:10:25-07', 'Екатерина','Иванова', '+77077777779', 'kate@gmail.com', '12345', 'женский', '/assets/images/avatar_facilitator_1.jpg', 'Далеко-далеко за словесными горами в стране гласных и согласных живут рыбные тексты. Скатился, силуэт ее имени за путь бросил. Дорогу, заголовок мир.');


INSERT INTO knowledges (created, updated, title, description, image, is_published, published_date,  author_id, category_id)
VALUES ('2022-03-11T19:10:25-07', '2022-03-11T20:10:25-07', 'База знаний #1', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '/assets/knowledges/knowledges.jpg', true, '2022-04-01T23:10:25-07', 1, 5),
       ('2022-03-10T16:10:25-07', '2022-03-11T19:10:25-07', 'База знаний #2', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '/assets/knowledges/knowledges.jpg', true, '2022-04-02T22:50:25-07', 2, 6),
       ('2022-03-10T17:10:25-07', '2022-03-11T18:10:25-07', 'База знаний #3', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '/assets/knowledges/knowledges.jpg', true, '2022-04-03T23:50:25-07', 3, 7),
       ('2022-03-10T17:10:25-07', '2022-03-11T18:10:25-07', 'База знаний #4', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '/assets/knowledges/knowledges.jpg', true, '2022-04-04T20:50:25-07', 3, 8);

INSERT INTO news (created, updated, title, description, image, is_published, published_date, author_id)
VALUES ('2022-03-11T19:10:25-07', '2022-03-11T20:10:25-07', 'Новости #1', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '/assets/news/news.jpg', true, '2022-04-01T22:50:25-07', 1),
       ('2022-03-10T16:10:25-07', '2022-03-11T19:10:25-07', 'Новости #2', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '/assets/news/news.jpg', true, '2022-04-02T23:50:25-07', 2),
       ('2022-03-10T17:10:25-07', '2022-03-11T18:10:25-07', 'Новости #3', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '/assets/news/news.jpg', true, '2022-04-03T23:10:25-07', 3),
       ('2022-03-10T16:10:25-07', '2022-03-11T19:10:25-07', 'Новости #2', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '/assets/news/news.jpg', true, '2022-04-04T23:50:25-07', 2),
       ('2022-03-10T17:10:25-07', '2022-03-11T18:10:25-07', 'Новости #3', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '/assets/news/news.jpg', true, '2022-04-05T23:10:25-07', 3);


INSERT INTO formats (created, updated, title)
VALUES 	('2022-03-11T19:10:25-07', '2022-03-11T20:10:25-07', 'онлайн'),
	    ('2022-03-10T16:10:25-07', '2022-03-11T19:10:25-07', 'офлайн'),
	    ('2022-03-10T17:10:25-07', '2022-03-11T18:10:25-07', 'гибридный');


INSERT INTO languages (created, updated, title)
VALUES 	('2022-03-11T19:10:25-07', '2022-03-11T20:10:25-07', 'русский'),
	    ('2022-03-10T16:10:25-07', '2022-03-11T19:10:25-07', 'казахский'),
	    ('2022-03-10T17:10:25-07', '2022-03-11T18:10:25-07', 'английский');


INSERT INTO preferred_communications (created, updated, title)
VALUES 	('2022-03-11T19:10:25-07', '2022-03-11T20:10:25-07', 'телефон'),
	    ('2022-03-10T16:10:25-07', '2022-03-11T19:10:25-07', 'email');


INSERT INTO cities (created, updated, title, slug)
VALUES 	('2022-03-11T19:10:25-07', '2022-03-11T20:10:25-07', 'Нур-Султан', 'nursultan'),
        ('2022-03-10T16:10:25-07', '2022-03-11T19:10:25-07', 'Алматы', 'almaty'),
        ('2022-03-10T17:10:25-07', '2022-03-11T18:10:25-07', 'Караганда', 'karaganda');


INSERT INTO submissions (created, updated, organization_name, planning_date, participant_count, budget, goal, full_name, phone_number,
email, comment, city_id, language_id, preferred_communication_id)
VALUES 	('2022-03-11T19:10:25-07', '2022-03-11T20:10:25-07', 'организация_1', '10-04-19 12:00:17', '10', '100', 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Eaque sunt,
         corporis magnam aliquid animi quod harum. Qui aspernatur itaque dolor.', 'Петр Петров', '+7 777-77-77', 'test_1@email.com',
	    'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Eaque sunt, corporis magnam aliquid animi quod harum. Qui aspernatur
	    itaque dolor. Lorem ipsum dolor sit amet consectetur adipisicing elit. Facilis quia repudiandae quos. Blanditiis deleniti quia
	    tempora? Cumque non totam minima.',(SELECT "id" FROM "cities" WHERE title='Нур-Султан'),
	    (SELECT "id" FROM "languages" WHERE title='русский'), (SELECT "id" FROM "preferred_communications" WHERE title='email')),

	    ('2022-03-10T16:10:25-07', '2022-03-11T19:10:25-07', 'организация_2', '10-04-19 12:00:17', '20', '200', 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Eaque sunt,
        corporis magnam aliquid animi quod harum. Qui aspernatur itaque dolor.', 'Непетр Непетров', '+1 111-11-11', 'test_2@email.com',
	    'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Eaque sunt, corporis magnam aliquid animi quod harum. Qui aspernatur
	    itaque dolor. Lorem ipsum dolor sit amet consectetur adipisicing elit. Facilis quia repudiandae quos. Blanditiis deleniti quia
	    tempora? Cumque non totam minima.',(SELECT "id" FROM "cities" WHERE title='Алматы'),
	    (SELECT "id" FROM "languages" WHERE title='казахский'), (SELECT "id" FROM "preferred_communications" WHERE title='телефон')),

	    ('2022-03-10T17:10:25-07', '2022-03-11T18:10:25-07', 'организация_3', '10-04-19 12:00:17', '30', '300', 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Eaque sunt,
        corporis magnam aliquid animi quod harum. Qui aspernatur itaque dolor.', 'Возможнопетр Возможнопетров', '+3 333-33-33', 'test_3@email.com',
	    'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Eaque sunt, corporis magnam aliquid animi quod harum. Qui aspernatur
	    itaque dolor. Lorem ipsum dolor sit amet consectetur adipisicing elit. Facilis quia repudiandae quos. Blanditiis deleniti quia
	    tempora? Cumque non totam minima.',(SELECT "id" FROM "cities" WHERE title='Караганда'),
	    (SELECT "id" FROM "languages" WHERE title='английский'), (SELECT "id" FROM "preferred_communications" WHERE title='email'));

