CREATE TABLE comments
(
    "id"          	    SERIAL       NOT NULL,
    "created"     	    TIMESTAMP    NOT NULL,
    "updated"     	    TIMESTAMP    DEFAULT NULL,
    "comment"           VARCHAR      NOT NULL,
    "user_id"   	    INT          DEFAULT NULL,
    "is_approve"        BOOLEAN      DEFAULT FALSE,

    CONSTRAINT comments_pkey
        PRIMARY KEY(id),

    CONSTRAINT fk_comments_user
        FOREIGN KEY (user_id)
            REFERENCES users (id)
            ON DELETE RESTRICT
            ON UPDATE CASCADE
);

CREATE TABLE entity_comments
(
    "id"             SERIAL    NOT NULL,
    "created"        TIMESTAMP NOT NULL,
    "updated"        TIMESTAMP DEFAULT NULL,
    "comment_id"     INT       NOT NULL,
    "entity_type_id" INT       NOT NULL,
    "entity_id"      INT       NOT NULL,

    CONSTRAINT entity_types_comments_pkey
        PRIMARY KEY(id),

    CONSTRAINT comment_id_entity_id_entity_type_id_unique
        UNIQUE (comment_id, entity_id, entity_type_id),

    CONSTRAINT fk_m2m_entity_types_comments_comment
        FOREIGN KEY (comment_id)
            REFERENCES comments (id)
            ON DELETE RESTRICT
            ON UPDATE CASCADE,

    CONSTRAINT fk_m2m_entity_types_comments_entity_types
        FOREIGN KEY (entity_type_id)
            REFERENCES entity_types (id)
            ON DELETE RESTRICT
            ON UPDATE CASCADE
);