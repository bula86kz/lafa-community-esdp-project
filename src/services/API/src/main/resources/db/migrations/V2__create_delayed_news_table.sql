CREATE TABLE delayed_news
(
    "id"          	    SERIAL       NOT NULL,
    "created"     	    TIMESTAMP    NOT NULL,
    "updated"     	    TIMESTAMP    DEFAULT NULL,
    "posting_date"     	TIMESTAMP    NOT NULL,
    "news_id"   	    INT          NOT NULL,

    CONSTRAINT delayed_news_pkey
        PRIMARY KEY(id),

    CONSTRAINT fk_delayed_news_news
        FOREIGN KEY (news_id)
            REFERENCES news (id)
            ON DELETE RESTRICT
            ON UPDATE CASCADE
);
