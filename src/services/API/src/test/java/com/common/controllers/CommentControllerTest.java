package com.common.controllers;

import application.Application;
import application.com.common.models.Comment;
import application.com.users.models.User;
import lombok.Getter;
import lombok.Setter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNotNull;

@Getter @Setter
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@TestPropertySource("/application-test.properties")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = Application.class)
public class CommentControllerTest {
    @Autowired
    private TestRestTemplate restTemplate;

    @LocalServerPort
    private int port;

    @Value("${spring.test.root.url}")
    private String url;

    private String getRootUrl() {
        return url + port;
    }

    @Test
    public void getAllComments() {
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(getRootUrl() + "/api/comments",
                HttpMethod.GET, entity, String.class);
        assertNotNull(response.getBody());
    }

    @Test
    public void getCommentById() {
        Comment comment = restTemplate.getForObject(getRootUrl() + "/api/comments/1", Comment.class);
        System.out.println(comment.getComment());
        assertNotNull(comment);
    }

    @Test
    public void createComment() {
        Comment comment = new Comment();
        comment.setComment("comment");
        comment.setUser(new User());
        comment.setApprove(false);
        ResponseEntity<Comment> postResponse = restTemplate.postForEntity(getRootUrl() + "/api/comments", comment, Comment.class);
        assertNotNull(postResponse);
        assertNotNull(postResponse.getBody());
    }

    @Test
    public void updateComment() {
        int id = 1;
        Comment comment = restTemplate.getForObject(getRootUrl() + "/api/comments/" + id, Comment.class);
        comment.setComment("comment-test");
        restTemplate.put(getRootUrl() + "/api/comments/" + id, comment);
        Comment updatedComment = restTemplate.getForObject(getRootUrl() + "/api/comments/" + id, Comment.class);
        assertNotNull(updatedComment);
    }

    @Test
    public void deleteComment() {
        int id = 2;
        Comment comment = restTemplate.getForObject(getRootUrl() + "/api/comments/" + id, Comment.class);
        assertNotNull(comment);
        restTemplate.delete(getRootUrl() + "/api/comments/" + id);
        try {
            comment = restTemplate.getForObject(getRootUrl() + "/api/comments/" + id, Comment.class);
        } catch (final HttpClientErrorException e) {
            assertEquals(e.getStatusCode(), HttpStatus.NOT_FOUND);
        }
    }
}
