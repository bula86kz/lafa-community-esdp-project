package com.common.controllers;

import application.Application;
import application.com.common.models.Language;
import lombok.Getter;
import lombok.Setter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNotNull;

@Getter @Setter
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@TestPropertySource("/application-test.properties")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = Application.class)
public class LanguageControllerTests {

    @Autowired
    private TestRestTemplate restTemplate;

    @LocalServerPort
    private int port;

    @Value("${spring.test.root.url}")
    private String url;

    private String getRootUrl() {
        return url + port;
    }

    @Test
    public void getAllLanguages() {
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(getRootUrl() + "/api/languages",
                HttpMethod.GET, entity, String.class);
        assertNotNull(response.getBody());
    }

    @Test
    public void getLanguageById() {
        Language language = restTemplate.getForObject(getRootUrl() + "/api/languages/1", Language.class);
        System.out.println(language.getTitle());
        assertNotNull(language);
    }

    @Test
    public void createLanguage() {
        Language language = new Language();
        language.setTitle("Украинский");
        ResponseEntity<Language> postResponse = restTemplate.postForEntity(getRootUrl() + "/api/languages", language, Language.class);
        assertNotNull(postResponse);
        assertNotNull(postResponse.getBody());
    }

    @Test
    public void updateLanguage() {
        int id = 1;
        Language language = restTemplate.getForObject(getRootUrl() + "/api/languages/" + id, Language.class);
        language.setTitle("Кавказский");
        restTemplate.put(getRootUrl() + "/api/languages/" + id, language);
        Language updatedLanguage = restTemplate.getForObject(getRootUrl() + "/api/languages/" + id, Language.class);
        assertNotNull(updatedLanguage);
    }

    @Test
    public void deleteLanguage() {
        int id = 2;
        Language language = restTemplate.getForObject(getRootUrl() + "/api/languages/" + id, Language.class);
        assertNotNull(language);
        restTemplate.delete(getRootUrl() + "/api/languages/" + id);
        try {
            language = restTemplate.getForObject(getRootUrl() + "/api/languages/" + id, Language.class);
        } catch (final HttpClientErrorException e) {
            assertEquals(e.getStatusCode(), HttpStatus.NOT_FOUND);
        }
    }
}
