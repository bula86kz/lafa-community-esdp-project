package aplication.com.repositories;

import aplication.com.models.Description;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DescriptionRepository extends MongoRepository<Description, String> {
}
