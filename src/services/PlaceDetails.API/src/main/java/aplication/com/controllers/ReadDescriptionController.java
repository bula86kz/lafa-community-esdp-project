package aplication.com.controllers;

import aplication.com.models.Description;
import aplication.com.services.DescriptionService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/descriptions")
public class ReadDescriptionController {
    private final DescriptionService service;

    public ReadDescriptionController(DescriptionService service) {
        this.service = service;
    }

    @GetMapping("/{id}")
    public Description read(@PathVariable String id) {
        return service.read(id);
    }
}
