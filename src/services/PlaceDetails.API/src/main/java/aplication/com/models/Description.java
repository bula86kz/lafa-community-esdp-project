package aplication.com.models;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import java.util.List;
import java.util.UUID;

@Getter @Setter
@Document("descriptions")
public class Description {
    @Id
    private String id;
    private String title;
    private List<String> description;

    public Description(String title, List<String> description) {
        this.id = UUID.randomUUID().toString();
        this.title = title;
        this.description = description;
    }
}
