package aplication.com.controllers;

import aplication.com.models.Description;
import aplication.com.services.DescriptionService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/descriptions")
public class CreateDescriptionController {
    private final DescriptionService service;

    public CreateDescriptionController(DescriptionService service) {
        this.service = service;
    }

    @PostMapping
    public Description create(@RequestBody Description description) {
        return service.create(description.getTitle(), description.getDescription());
    }
}
