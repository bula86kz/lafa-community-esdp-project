package aplication.com.controllers;

import aplication.com.services.DescriptionService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/descriptions")
public class DeleteDescriptionController {
    private final DescriptionService service;

    public DeleteDescriptionController(DescriptionService service) {
        this.service = service;
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable String id) {
        service.delete(id);
    }

}
