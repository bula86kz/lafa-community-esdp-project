package aplication.com.services;

import aplication.com.models.Description;
import aplication.com.repositories.DescriptionRepository;
import org.springframework.stereotype.Service;
import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Service
public class DescriptionService {
    private final DescriptionRepository repository;

    public DescriptionService(DescriptionRepository repository) {
        this.repository = repository;
    }

    public Description create(String title, List<String> placeDescription) {
        Description description = new Description(title, placeDescription);
        return repository.save(description);
    }

    public void delete(String id) {
         repository.deleteById(id);
    }

    public Description read(String id) {
        Optional<Description> optionalDescription = repository.findById(id);
        return getDescriptionOrTrowException(optionalDescription);
    }

    public List<Description> readAll() {
        return repository.findAll();
    }

    public Description update(String id, String title, List<String> placeDescription) {
        Optional<Description> optionalDescription = repository.findById(id);
        Description targetDescription =getDescriptionOrTrowException(optionalDescription);
        targetDescription.setTitle(title);
        targetDescription.setDescription(placeDescription);
        return  repository.save(targetDescription);
    }

    public Description getDescriptionOrTrowException(Optional<Description> description) {
        if(description.isPresent()){
            return description.get();
        }
        throw new EntityNotFoundException("Can't find description with the ID");
    }
}
