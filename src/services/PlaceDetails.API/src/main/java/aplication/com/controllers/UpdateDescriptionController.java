package aplication.com.controllers;

import aplication.com.models.Description;
import aplication.com.services.DescriptionService;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/descriptions")
public class UpdateDescriptionController {
    private final DescriptionService service;

    public UpdateDescriptionController(DescriptionService service) {
        this.service = service;
    }

    @PutMapping("/{id}")
    public Description update(@PathVariable String id, @RequestBody Description description) {
        return service.update(id, description.getTitle(), description.getDescription());
    }
}
