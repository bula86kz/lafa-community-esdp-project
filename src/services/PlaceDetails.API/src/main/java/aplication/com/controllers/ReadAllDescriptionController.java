package aplication.com.controllers;

import aplication.com.models.Description;
import aplication.com.services.DescriptionService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api/descriptions")
public class ReadAllDescriptionController {
    private final DescriptionService service;

    public ReadAllDescriptionController(DescriptionService service) {
        this.service = service;
    }

    @GetMapping
    public List<Description> readAll() {
        return service.readAll();
    }
}
