package application.com.tokens;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import java.util.Date;

@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
public class Configuration {
    private Integer id;

    @CreatedDate
    @JsonIgnore
    private Date created;

    @LastModifiedDate
    @JsonIgnore
    private Date modified;

    private String key;
    private String value;
}

