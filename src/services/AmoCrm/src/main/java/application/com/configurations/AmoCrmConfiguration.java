package application.com.configurations;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;


@Getter @Setter
@Configuration
@ConfigurationProperties(prefix = "amo")
public class AmoCrmConfiguration {
    private String url;
    private int sizeOfMemoryInWebflux = 16 * 1024 * 1024;

    @Bean
    public WebClient amoCrmWebClient() {
        return WebClient.builder().exchangeStrategies(ExchangeStrategies.builder()
                .codecs(configurer -> configurer
                        .defaultCodecs()
                        .maxInMemorySize(sizeOfMemoryInWebflux))
                .build()).baseUrl(getUrl()).build();
    }

}
