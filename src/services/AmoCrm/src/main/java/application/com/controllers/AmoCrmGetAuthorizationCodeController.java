package application.com.controllers;

import application.com.services.AmoCrmServiceImpl;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/amo")
public class AmoCrmGetAuthorizationCodeController {

    private final AmoCrmServiceImpl amoCrmService;

    public AmoCrmGetAuthorizationCodeController(AmoCrmServiceImpl amoCrmService) {
        this.amoCrmService = amoCrmService;
    }

    @GetMapping("/authorization-code")
    public void getAuthorizationCode(@RequestParam String code) {
        amoCrmService.getAuthorizationCode(code);
    }
}
