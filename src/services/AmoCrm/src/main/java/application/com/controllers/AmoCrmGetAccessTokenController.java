package application.com.controllers;

import application.com.services.AmoCrmServiceImpl;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/amo")
public class AmoCrmGetAccessTokenController {

    private final AmoCrmServiceImpl amoCrmService;

    public AmoCrmGetAccessTokenController(AmoCrmServiceImpl amoCrmService) {
        this.amoCrmService = amoCrmService;
    }

    @GetMapping("/access-token")
    public void getAccessToken() {
        amoCrmService.getAccessToken();
    }
}
