package application.com.services.interfaces;

public interface AmoCrmService {
    String getAccessToken();
    void getAuthorizationCode(String code);
}
