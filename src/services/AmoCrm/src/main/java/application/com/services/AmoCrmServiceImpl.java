package application.com.services;

import application.com.dto.AmoAccessTokenResponse;
import application.com.dto.AmoAuthorizationCodeRequest;
import application.com.dto.AmoRefreshTokenRequest;
import application.com.dto.*;
import application.com.tokens.Configuration;
import application.com.services.interfaces.AmoCrmService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import java.util.Date;
import java.util.Objects;

@Log4j2
@Service
public class AmoCrmServiceImpl implements AmoCrmService {
    private final String KEY_ACCESS = "accessToken";
    private final String KEY_REFRESH = "refreshToken";

    private final WebClient amoCrmWebClient;

    private final RedisTemplate<String, Configuration> redis;

    @Value("${amo.client_id}")
    private String clientId;

    @Value("${amo.client_secret}")
    private String clientSecret;

    @Value("${amo.validity-time.access-token}")
    private Long accessTokenValidityTime;

    @Value("${amo.redirect_url}")
    private String redirectUrl;

    public AmoCrmServiceImpl(WebClient amoCrmWebClient, RedisTemplate<String, Configuration> redis) {
        this.amoCrmWebClient = amoCrmWebClient;
        this.redis = redis;
    }

    @Override
    public void getAuthorizationCode(String code) {
        AmoAuthorizationCodeRequest authorizationCodeRequest = new AmoAuthorizationCodeRequest(
                clientId,
                clientSecret,
                "authorization_code",
                redirectUrl,
                code);

        AmoAccessTokenResponse accessTokenResponse = Objects.requireNonNull(amoCrmWebClient.post()
                .uri("/oauth2/access_token")
                .bodyValue(authorizationCodeRequest)
                .retrieve()
                .bodyToMono(AmoAccessTokenResponse.class)
                .doOnError(throwable -> {
                    throw new BadCredentialsException("Authorization Code not valid");
                })
                .doOnSuccess(contactResponse -> log.info("OK requesting AmoCRM: {}", contactResponse))
                .block());

        redis.opsForValue().set(KEY_REFRESH, new Configuration(2, new Date(), new Date(), KEY_REFRESH, accessTokenResponse.getRefreshToken()));
        redis.opsForValue().set(KEY_ACCESS, new Configuration(1, new Date(), new Date(), KEY_ACCESS, accessTokenResponse.getAccessToken()));
    }

    @Override
    public String getAccessToken() {
        Configuration accessToken = redis.opsForValue().get(KEY_ACCESS);
        Configuration refreshToken = redis.opsForValue().get(KEY_REFRESH);

        if ((accessToken.getModified().getTime() + accessTokenValidityTime) <= System.currentTimeMillis()) {
            AmoRefreshTokenRequest refreshTokenRequest = new AmoRefreshTokenRequest(
                    clientId,
                    clientSecret,
                    "refresh_token",
                    refreshToken.getValue(),
                    redirectUrl);

            AmoAccessTokenResponse accessTokenResponse = Objects.requireNonNull(amoCrmWebClient.post()
                    .uri("/oauth2/access_token")
                    .bodyValue(refreshTokenRequest)
                    .retrieve()
                    .bodyToMono(AmoAccessTokenResponse.class)
                    .doOnError(throwable -> {
                        throw new BadCredentialsException("Refresh token not valid");
                    })
                    .doOnSuccess(contactResponse -> log.info("OK requesting AmoCRM: {}", contactResponse))
                    .block());

            accessToken.setValue(accessTokenResponse.getAccessToken());
            refreshToken.setValue(accessTokenResponse.getRefreshToken());
            accessToken.setValue(accessTokenResponse.getAccessToken());
            refreshToken.setValue(accessTokenResponse.getRefreshToken());
            redis.opsForValue().set(KEY_REFRESH, refreshToken);
            redis.opsForValue().set(KEY_ACCESS, accessToken);
            return redis.opsForValue().get(KEY_ACCESS).getValue();
        } else {
            return accessToken.getValue();
        }
    }

}
