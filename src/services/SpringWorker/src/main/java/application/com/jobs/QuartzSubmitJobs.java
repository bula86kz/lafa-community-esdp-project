package application.com.jobs;

import application.com.configuration.QuartzConfiguration;
import org.quartz.JobDetail;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;
import org.springframework.scheduling.quartz.SimpleTriggerFactoryBean;

@Configuration
public class QuartzSubmitJobs {

    @Bean(name = "delayedNews")
    public JobDetailFactoryBean jobDelayedNews() {
        return QuartzConfiguration.createJobDetail(DelayedNewsJob.class, "Delayed News Job");
    }

    @Bean
    public SimpleTriggerFactoryBean triggerDelayedNews(@Qualifier("delayedNews") JobDetail jobDetail) {
        return QuartzConfiguration.createTrigger(jobDetail, 3600000,"Delayed News Trigger");
    }
}
