package application.com.jobs;

import application.com.services.NewsService;
import lombok.extern.slf4j.Slf4j;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@DisallowConcurrentExecution
public class DelayedNewsJob implements Job {
    private final NewsService newsService;

    public DelayedNewsJob(NewsService newsService) {
        this.newsService = newsService;
    }

    @Override
    public void execute(JobExecutionContext context) {
        log.info("Job ** {} ** starting @ {}", context.getJobDetail().getKey().getName(), context.getFireTime());
        String response = newsService.sendGet();
        log.info("Job ** {} ** starting @ {}", context.getJobDetail().getKey().getName(), context.getFireTime() + " " + response);
    }
}
