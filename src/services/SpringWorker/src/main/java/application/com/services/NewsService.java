package application.com.services;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class NewsService
{
    private final RestTemplate restTemplate;

    public NewsService(RestTemplate restTemplate)
    {
        this.restTemplate = restTemplate;
    }

    public String sendGet()
    {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        String url = "http://localhost:8080/api/news/delayed/nearest";
        HttpEntity<?> entity = new HttpEntity<>(headers);

        HttpEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);

        return response.toString();
    }
}
