package application.com.repositories;

import application.com.models.Bucket;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface BucketRepository extends MongoRepository<Bucket, String> {
    Bucket getByBindingId(String bindingId);
    Optional<Bucket> findByBindingId(String bindingId);

    boolean existsByBindingId(String bindingId);

    void deleteByBindingId(String bindingId);
}
