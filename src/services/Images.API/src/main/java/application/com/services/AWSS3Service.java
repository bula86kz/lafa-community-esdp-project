package application.com.services;

import application.com.models.Bucket;
import application.com.models.Media;
import application.com.utils.FileUtils;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import lombok.extern.java.Log;
import lombok.extern.log4j.Log4j;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.*;

@Log4j2
@Service
public class AWSS3Service {
    @Value("${aws.s3.url}")
    private String S3Url;
    @Value("${aws.s3.bucket-name}")
    private String bucket;

    private final AmazonS3 S3Client;

    public AWSS3Service(AmazonS3 s3Client) {
        S3Client = s3Client;
    }

    public List<Media> uploadFiles(List<MultipartFile> multipartFiles) {
        List<Media> mediaList = new ArrayList<Media>();

        log.info("Upload {} to S3", mediaList);

        for(MultipartFile multipartFile : multipartFiles)
        {
            Media media = uploadFile(multipartFile);
            mediaList.add(media);
        }

        return mediaList;
    }

    public void deleteFiles(Bucket bucket) {
        List<Media> mediaList = bucket.getMediaList();

        log.info("Delete {} from S3", mediaList.toString());

        for (Media media : mediaList)
        {
            deleteFileFromS3Bucket(media.getFileName());
        }
    }

    private Media uploadFile(MultipartFile multipartFile) {
        String id = UUID.randomUUID().toString();

        File file = FileUtils.convertMultipartToFile(multipartFile);
        String fileName = FileUtils.generateFilename(id, file);

        String url = S3Url + "/" + fileName;

        log.info("Upload {} to S3", fileName);

        uploadFileToS3Bucket(fileName, file);

        file.delete();

        return new Media(id, url, fileName);
    }

    public void deleteFile(String fileName) {
        log.info("Delete {} from S3", fileName);

        deleteFileFromS3Bucket(fileName);
    }

    private void uploadFileToS3Bucket(String fileName, File file) {
        S3Client.putObject(bucket, fileName, file);
        S3Client.setObjectAcl(bucket, fileName, CannedAccessControlList.PublicRead);
    }

    private void deleteFileFromS3Bucket(String fileName) {
        S3Client.deleteObject(bucket, fileName);
    }
}
