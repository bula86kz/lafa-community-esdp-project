package application.com.controllers;

import application.com.models.Bucket;
import application.com.services.BucketService;
import lombok.extern.log4j.Log4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/api/buckets")
public class UpdateBucketController {
    private final BucketService service;

    public UpdateBucketController(BucketService service) {
        this.service = service;
    }

    @PutMapping
    public ResponseEntity<?> update(@RequestPart("files") List<MultipartFile> files, @RequestPart("bindingId") String bindingId) {
        return ResponseEntity
                            .status(HttpStatus.OK)
                            .body(service.update(files, bindingId));
    }
}
