package application.com.controllers;

import application.com.services.BucketService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/buckets/media")
public class DeleteImageFromBucketController {
    private final BucketService service;

    public DeleteImageFromBucketController(BucketService service) {
        this.service = service;
    }

    @DeleteMapping
    public void delete(@RequestPart String bindingId, @RequestPart String fileName) {
        service.deleteMedia(bindingId, fileName);
    }
}
