package application.com.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.UUID;

@ToString
@Getter @Setter
@Document("media")
@AllArgsConstructor
public class Media
{
    @Id
    private String id;

    private String url;
    private String fileName;

    public Media() {
        this.id = UUID.randomUUID().toString();
    }

    public Media(String url, String fileName) {
        this.id = UUID.randomUUID().toString();

        this.url = url;
        this.fileName = fileName;
    }
}
