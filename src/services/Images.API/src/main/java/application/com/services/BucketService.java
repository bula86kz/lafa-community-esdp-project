package application.com.services;

import application.com.models.Bucket;
import application.com.models.Media;
import application.com.repositories.BucketRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Log4j2
@Service
public class BucketService {
    private final BucketRepository repository;

    private final AWSS3Service S3Service;

    public BucketService(BucketRepository repository, AWSS3Service S3Service) {
        this.repository = repository;

        this.S3Service = S3Service;
    }

    public Bucket save(List<MultipartFile> files, String bindingId) {
        if (repository.existsByBindingId(bindingId)) {
            log.info("Save {} to existing bucket", files);

            return update(files, bindingId);
        } else {
            List<Media> media = S3Service.uploadFiles(files);

            log.info("Save {} to new bucket", media.toString());

            return repository.save(new Bucket(bindingId, media));
        }
    }

    public Bucket read(String bindingId) {
        return repository.getByBindingId(bindingId);
    }

    public Bucket update(List<MultipartFile> files, String bindingId) {
        Bucket bucket = repository.getByBindingId(bindingId);
        List<Media> media = S3Service.uploadFiles(files);

        log.info("Update {} by new {}", bucket.toString(), media.toString());

        bucket.addMedia(media);

        return repository.save(bucket);
    }

    public void delete(String bindingId) {
        Bucket bucket = repository.getByBindingId(bindingId);

        log.info("Delete {} from DB", bucket.toString());

        S3Service.deleteFiles(bucket);

        repository.deleteByBindingId(bindingId);
    }

    public void deleteMedia(String bindingId, String fileName) {
        Bucket bucket = repository.getByBindingId(bindingId);

        log.info("Delete {} from {} in DB", fileName, bucket.toString());

        bucket.deleteMedia(fileName);
        S3Service.deleteFile(fileName);

        repository.save(bucket);
    }
}
