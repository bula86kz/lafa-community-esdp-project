package application.com.controllers;

import application.com.services.BucketService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/api/buckets")
public class CreateBucketController {
    private final BucketService service;

    public CreateBucketController(BucketService service) {
        this.service = service;
    }

    @PostMapping
    public ResponseEntity<?> create(@RequestPart("files") List<MultipartFile> files, @RequestPart("bindingId") String bindingId) {
        return ResponseEntity
                            .status(HttpStatus.CREATED)
                            .body(service.save(files, bindingId));
    }
}
