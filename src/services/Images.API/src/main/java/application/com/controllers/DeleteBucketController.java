package application.com.controllers;

import application.com.services.BucketService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/buckets")
public class DeleteBucketController {
    private final BucketService service;

    public DeleteBucketController(BucketService service) {
        this.service = service;
    }

    @DeleteMapping
    public void delete(@RequestPart String bindingId) {
        service.delete(bindingId);
    }
}
