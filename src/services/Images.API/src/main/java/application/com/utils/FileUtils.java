package application.com.utils;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Objects;

@Component
public class FileUtils {

    public static String getFileExtension(File file) {
        String fileName = file.getName();

        int index = fileName.indexOf('.');

        return index > 0 ? fileName.substring(index + 1) : "";
    }

    public static String getOriginalFilename(MultipartFile file) {
        return Objects.requireNonNull(file.getOriginalFilename());
    }

    public static File convertMultipartToFile(MultipartFile file) {
        File convertedFile = new File(FileUtils.getOriginalFilename(file));

        try(FileOutputStream stream = new FileOutputStream(convertedFile)) {
            stream.write(file.getBytes());
        } catch (IOException exception) {
            exception.printStackTrace();
        }

        return convertedFile;

    }

    public static String generateFilename(String UUID, File file) {
        return UUID + "." + FileUtils.getFileExtension(file);
    }
}
