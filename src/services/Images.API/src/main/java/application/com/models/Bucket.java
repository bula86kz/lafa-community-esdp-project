package application.com.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@ToString
@Getter @Setter
@AllArgsConstructor
@Document("buckets")
public class Bucket {
    @Id
    private String id;
    private String bindingId;

    private List<Media> mediaList;

    public Bucket() {
        this.id = UUID.randomUUID().toString();
    }

    public Bucket(String bindingId, List<Media> mediaList) {
        this.id = UUID.randomUUID().toString();
        this.bindingId = bindingId;

        this.mediaList = mediaList;
    }

    public void addMedia(List<Media> mediaList) {
        this.mediaList = Stream
                            .concat(this.mediaList.stream(), mediaList.stream())
                            .collect(Collectors.toList());
    }

    public void deleteMedia(String fileName) {
        this.mediaList.removeIf(media -> media.getFileName().equals(fileName));
    }
}
