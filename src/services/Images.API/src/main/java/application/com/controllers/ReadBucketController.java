package application.com.controllers;

import application.com.services.BucketService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/buckets")
public class ReadBucketController {
    private final BucketService service;

    public ReadBucketController(BucketService service) {
        this.service = service;
    }

    @GetMapping("/{bindingId}")
    public ResponseEntity<?> read(@PathVariable String bindingId) {
        return ResponseEntity
                            .status(HttpStatus.OK)
                            .body(service.read(bindingId));
    }
}
