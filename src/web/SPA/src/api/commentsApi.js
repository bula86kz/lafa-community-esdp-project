import axios from "axios";
const { isProd, API_URL } = __myapp;

const BASE_URL = API_URL + '/comments';

const commentsAPI = {

	readAll: async (entityType, entityId) => {
		console.log(entityId, entityType);
		const response = await axios.get(BASE_URL + '/' + entityType + '/' + entityId);
		console.log("Data: ", response.data);
		return response.data;
	},


	read: async (id) => {
		const response = await axios.get(BASE_URL + '/' + id);
		return response.data;
	},

	create: async (comment) => {
		const response = await axios.post(BASE_URL, comment, {
			headers: {
				'Content-Type': 'application/json;charset=utf-8'
			},
		}).then(res => {
			console.log("Data: ", res.data);
			console.log("success");
			return response.data;
	}).catch(error => {
		console.log(error.response)
	});
}
}

export default commentsAPI;