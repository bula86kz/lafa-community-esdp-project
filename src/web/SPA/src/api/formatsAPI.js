import axios from "axios";
const { isProd, API_URL } = __myapp;

const BASE_URL = API_URL + '/formats';

const formatsAPI = {

	readAll: async () => {
		const response = await axios.get(BASE_URL);
		return response.data;
	},

	read: async (id) => {
		const response = await axios.get(BASE_URL + '/' + id);
		return response.data;
	},

	create: async (formats) => {
		const response = await axios.post(BASE_URL, {
			headers: {
				'Content-Type': 'application/json;charset=utf-8'
			},
			body: JSON.stringify(formats)
		});
		return response.data;
	}
}

export default formatsAPI;





