import axios from "axios";
const { isProd, API_URL } = __myapp;

const BASE_URL = API_URL + '/categories';

const categoriesAPI = {

    readAll: async () => {
        const response = await axios.get(BASE_URL);
        return response.data;
    },


    read: async (entityType) => {
        const response = await axios.get(BASE_URL + '/' + entityType);
        return response.data;
    }

}

export default categoriesAPI;