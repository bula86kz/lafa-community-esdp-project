import axios from "axios";
const { isProd, API_URL } = __myapp;

const BASE_URL = API_URL + '/users';

const facilitatorsAPI = {
    
    readAll: async () => {
        const response = await axios.get(BASE_URL);
        return response.data;
    },
    
    
    read: async (email) => {
        const response = await axios.get(BASE_URL + '/' + email);
        return response.data;
    },
    
    create: async (user) => {
        const response = await axios.post(BASE_URL, {
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify(user)
        });
        return response.data;
    }
}

export default facilitatorsAPI;





