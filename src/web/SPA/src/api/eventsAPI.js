import axios from "axios";
const { isProd, API_URL } = __myapp;

const BASE_URL = API_URL + '/events';

const eventsAPI = {

	readAll: async () => {
		const response = await axios.get(BASE_URL);
		return response.data;
	},

	readByCategory: async (id) => {
		const response = await axios.get(BASE_URL + '/category/' + id);
		return response.data;
	},

	read: async (id) => {
		const response = await axios.get(BASE_URL + '/' + id);
		return response.data;
	},

	create: async (events) => {
		const response = await axios.post(BASE_URL, {
			headers: {
				'Content-Type': 'application/json;charset=utf-8'
			},
			body: JSON.stringify(events)
		});
		return response.data;
	}
}

export default eventsAPI;





