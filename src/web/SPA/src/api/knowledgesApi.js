import axios from "axios";
const { isProd, API_URL } = __myapp;

const BASE_URL = API_URL + '/knowledges';

const knowledgesAPI = {

	readAll: async () => {
		const response = await axios.get(BASE_URL + '/published');
		return response.data;
	},


	read: async (id) => {
		const response = await axios.get(BASE_URL + '/' + id);
		return response.data;
	},


	readByCategory: async (id) => {
		const response = await axios.get(BASE_URL + '/category/' + id);
		return response.data;
	},

	readByAuthor: async (id) => {
		const response = await axios.get(BASE_URL + '/author/' + id);
		return response.data;
	},


	create: async (newKnowledge) => {
		const response = await axios.post(BASE_URL, newKnowledge,
			{
				headers:{
					'Content-Type': 'application/json;charset=utf-8'
				}
			})
			.then(res=>{
				console.log("Data: ",res.data)
				console.log("success")
			})
	}
}

export default knowledgesAPI;